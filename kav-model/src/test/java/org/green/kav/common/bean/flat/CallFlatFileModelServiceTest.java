package org.green.kav.common.bean.flat;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;


class CallFlatFileModelServiceTest {

    @Test
    void getDateDiffToNewDate() throws Exception {
        CallFlatFileModelService callFlatFileModelService = new CallFlatFileModelService();
        callFlatFileModelService.addData("97455|850490917989565|640293246761021|463104007339093|Ooredoo@\t\t|20201026093154|1000|1|1|751116|1|1|1|0||2|219-2-143-675534||mcng|");
        callFlatFileModelService.sortByDate();
        LocalDateTime localDate = LocalDateTime.of(2020, 11, 10, 00, 00,00);
        long diff = callFlatFileModelService.getDateDiffToNewDate(localDate);
        System.err.println(diff);
    }
}
