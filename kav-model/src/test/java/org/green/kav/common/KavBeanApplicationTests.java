package org.green.kav.common;

import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.bean.flat.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;


@Slf4j
@SpringBootTest
class KavBeanApplicationTests {

    @Test
    void contextLoads() {
        String a = "a";
        String b = "a";

        log.info("{}", isStartCgi1(a, b));
        log.info("{}", isStartCgi2(a, b));
    }

    private boolean isStartCgi1(String a, String b) {
        if (isCellInvalid(a)) {
            if (a.equals(b)) return true;
        }
        return false;
    }

    private boolean isStartCgi2(String a, String b) {
        if (isCellInvalid(b)) {
            if (a == b) return true;
        }
        return false;
    }

    private boolean isCellInvalid(String abc) {
        return abc != null;
    }

    @Test
    public void testInnerLoopBreak() {
        List<String> actors = new ArrayList<>();
        actors.add("a");
        actors.add("b");
        actors.add("c");

        Set<String> msisdns = new HashSet<>();
        msisdns.add("a1");
        msisdns.add("a2");
        msisdns.add("a3");

        actors.stream().forEach(actor -> {
            log.info("1 {}", actor);
            Iterator<String> msisdnIt = msisdns.iterator();
            while (msisdnIt.hasNext()) {
                String msisdn = msisdnIt.next();
                if (msisdn.equals("a2")) {
                    log.info("2 {}", msisdn);
                    break;
                }
                log.info("3 {}", msisdn);
            }
        });
    }

    @Test
    public void testAddNanos(){
        DateTimeFormatter mcngDateTimeFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")
                                                                .withZone(ZoneOffset.UTC);

        LocalDateTime localDateTime = LocalDateTime.now();
        System.err.println(mcngDateTimeFormat.format(localDateTime));
    }

    @Test
    public void testFlatFiles(){
        CallFlatFileModelService callFlatFileModelService = new CallFlatFileModelService();
        LocationFlatFileModelService locationFlatFileModelService = new LocationFlatFileModelService();
        SmsFlatFileModelService smsFlatFileModelService = new SmsFlatFileModelService();
        MsisdnFlatFileModelService msisdnFlatFileModelService = new MsisdnFlatFileModelService();
        TargetFlatFileModelService targetFlatFileModelService = new TargetFlatFileModelService();
        log.info("1: {}", callFlatFileModelService.getHeaderSingleLine());
        log.info("2: {}", locationFlatFileModelService.getHeaderSingleLine());
        log.info("3: {}", smsFlatFileModelService.getHeaderSingleLine());
        log.info("4: {}", targetFlatFileModelService.getHeaderSingleLine());
        log.info("5: {}", msisdnFlatFileModelService.getHeaderSingleLine());
    }
}
