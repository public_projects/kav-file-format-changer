package org.green.kav.common.bean.flat;

import org.green.kav.common.bean.flat.model.RootFlatFileModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


public class TargetFlatFileModelService extends FlatFileModelService {
    public static final String DATE_TIME_COLUMN_HEADER_NAME = "DATEID";
    public TargetFlatFileModelService() {
        this.headers.add("MCTARGETID");
        this.headers.add("NAME");
        this.headers.add("DATEID");
        this.headers.add("SYSTEMID");
        this.headers.add("AGENCYID");
    }

    public static final String FILE_NAME = "P_TARGET_FLAT.csv";

    @Override
    public String getFileName() {
        return TargetFlatFileModelService.FILE_NAME;
    }

    @Override
    public String getDateColumnValue(RootFlatFileModel rootFlatFileModel) {
        return null;
    }

    @Override
    public List<RootFlatFileModel> sortByDate() throws Exception {
        return null;
    }

    @Override
    public long getDateDiffToNewDate(LocalDateTime expectedDate) throws Exception {
        return 0;
    }

    @Override
    public void addDaysToCurrentDates(long days) {
    }

    @Override
    public String getDataCsv(RootFlatFileModel rootFlatFileModel) {
        return null;
    }
}
