package org.green.kav.common.bean.flat.model;

/**
 * e.g:
 * {"instance":"mcng","agencyId":3,"agencyName":"AG03","interceptId":0}
 * {"system":"mcng","properties":{"instance":"mcng","agency-id":"3","agency-name":"AG03"}}
 */
public class SourceModel {
    /**
     * e.g: 3
     */
    private String agentId;
    /**
     * e.g: mcng
     */
    private String systemId;
    /**
     * e.g: 3
     */
    private String interceptId;
    /**
     * e.g: AG03
     */
    private String agencyName;

    public SourceModel() {}

    public SourceModel(String agentId, String systemId) {
        this.agentId = agentId;
        this.systemId = systemId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getInterceptId() {
        return interceptId;
    }

    public void setInterceptId(String interceptId) {
        this.interceptId = interceptId;
    }
}
