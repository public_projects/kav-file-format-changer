package org.green.kav.common.model.response;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.io.Serializable;
public class CdrGenerateRequest implements Serializable {
    @JsonAlias({"provider", "PROVIDER"})
    private String provider;
    @JsonAlias({"eventType", "EVENTTYPE"})
    private String eventType;
    @JsonAlias({"start", "START"})
    private String start;
    @JsonAlias({"duration", "DURATION"})
    private String duration;
    @JsonAlias({"status", "STATUS"})
    private String status;
    @JsonAlias({"cgiStartA", "CGISTARTA"})
    private String cgiStartA;
    @JsonAlias({"cgiStartB", "CGISTARTB"})
    private String cgiStartB;
    @JsonAlias({"latStartA", "LATSTARTA"})
    private String latStartA;
    @JsonAlias({"latStartB", "LATSTARTB"})
    private String latStartB;
    @JsonAlias({"lonStartA", "LONSTARTA"})
    private String lonStartA;
    @JsonAlias({"lonStartB", "LONSTARTB"})
    private String lonStartB;
    @JsonAlias({"cgiEndA", "CGIENDA"})
    private String cgiEndA;
    @JsonAlias({"cgiEndB", "CGIENDB"})
    private String cgiEndB;
    @JsonAlias({"latEndA", "LATENDA"})
    private String latEndA;
    @JsonAlias({"latEndB", "LATENDB"})
    private String latEndB;
    @JsonAlias({"lonEndA", "LONENDA"})
    private String lonEndA;
    @JsonAlias({"lonEndB", "LONENDB"})
    private String lonEndB;
    @JsonAlias({"msisdnA", "MSISDNA"})
    private String msisdnA;
    @JsonAlias({"msisdnB", "MSISDNB"})
    private String msisdnB;
    @JsonAlias({"msisdnC", "MSISDNC"})
    private String msisdnC;
    @JsonAlias({"imeiA", "IMEIA"})
    private String imeiA;
    @JsonAlias({"imeiB", "IMEIB"})
    private String imeiB;
    @JsonAlias({"imsiA", "IMSIA"})
    private String imsiA;
    @JsonAlias({"imsiB", "IMSIB"})
    private String imsiB;

    public CdrGenerateRequest() {
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCgiStartA() {
        return cgiStartA;
    }

    public void setCgiStartA(String cgiStartA) {
        this.cgiStartA = cgiStartA;
    }

    public String getCgiStartB() {
        return cgiStartB;
    }

    public void setCgiStartB(String cgiStartB) {
        this.cgiStartB = cgiStartB;
    }

    public String getLatStartA() {
        return latStartA;
    }

    public void setLatStartA(String latStartA) {
        this.latStartA = latStartA;
    }

    public String getLatStartB() {
        return latStartB;
    }

    public void setLatStartB(String latStartB) {
        this.latStartB = latStartB;
    }

    public String getLonStartA() {
        return lonStartA;
    }

    public void setLonStartA(String lonStartA) {
        this.lonStartA = lonStartA;
    }

    public String getLonStartB() {
        return lonStartB;
    }

    public void setLonStartB(String lonStartB) {
        this.lonStartB = lonStartB;
    }

    public String getCgiEndA() {
        return cgiEndA;
    }

    public void setCgiEndA(String cgiEndA) {
        this.cgiEndA = cgiEndA;
    }

    public String getCgiEndB() {
        return cgiEndB;
    }

    public void setCgiEndB(String cgiEndB) {
        this.cgiEndB = cgiEndB;
    }

    public String getLatEndA() {
        return latEndA;
    }

    public void setLatEndA(String latEndA) {
        this.latEndA = latEndA;
    }

    public String getLatEndB() {
        return latEndB;
    }

    public void setLatEndB(String latEndB) {
        this.latEndB = latEndB;
    }

    public String getLonEndA() {
        return lonEndA;
    }

    public void setLonEndA(String lonEndA) {
        this.lonEndA = lonEndA;
    }

    public String getLonEndB() {
        return lonEndB;
    }

    public void setLonEndB(String lonEndB) {
        this.lonEndB = lonEndB;
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(String msisdnA) {
        this.msisdnA = msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(String msisdnB) {
        this.msisdnB = msisdnB;
    }

    public String getMsisdnC() {
        return msisdnC;
    }

    public void setMsisdnC(String msisdnC) {
        this.msisdnC = msisdnC;
    }

    public String getImeiA() {
        return imeiA;
    }

    public void setImeiA(String imeiA) {
        this.imeiA = imeiA;
    }

    public String getImeiB() {
        return imeiB;
    }

    public void setImeiB(String imeiB) {
        this.imeiB = imeiB;
    }

    public String getImsiA() {
        return imsiA;
    }

    public void setImsiA(String imsiA) {
        this.imsiA = imsiA;
    }

    public String getImsiB() {
        return imsiB;
    }

    public void setImsiB(String imsiB) {
        this.imsiB = imsiB;
    }
}
