package org.green.kav.common.bean;

public enum CallType {
    VOICE,
    SMS,
    DATA,
    UNKNOWN;

    private CallType() {
    }

    public static CallType of(String value) {
        if (value == null) {
            return UNKNOWN;
        } else {
            byte var2 = -1;
            switch(value.hashCode()) {
                case 68:
                    if (value.equals("D")) {
                        var2 = 6;
                    }
                    break;
                case 83:
                    if (value.equals("S")) {
                        var2 = 3;
                    }
                    break;
                case 86:
                    if (value.equals("V")) {
                        var2 = 0;
                    }
                    break;
                case 100:
                    if (value.equals("d")) {
                        var2 = 7;
                    }
                    break;
                case 115:
                    if (value.equals("s")) {
                        var2 = 4;
                    }
                    break;
                case 118:
                    if (value.equals("v")) {
                        var2 = 1;
                    }
                    break;
                case 82233:
                    if (value.equals("SMS")) {
                        var2 = 5;
                    }
                    break;
                case 2090922:
                    if (value.equals("DATA")) {
                        var2 = 8;
                    }
                    break;
                case 81848594:
                    if (value.equals("VOICE")) {
                        var2 = 2;
                    }
            }

            switch(var2) {
                case 0:
                case 1:
                case 2:
                    return VOICE;
                case 3:
                case 4:
                case 5:
                    return SMS;
                case 6:
                case 7:
                case 8:
                    return DATA;
                default:
                    return UNKNOWN;
            }
        }
    }
}