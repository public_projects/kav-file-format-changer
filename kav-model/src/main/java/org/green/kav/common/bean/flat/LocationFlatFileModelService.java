package org.green.kav.common.bean.flat;

import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.bean.flat.model.RootFlatFileModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class LocationFlatFileModelService extends FlatFileModelService {
    public static final String FILE_NAME = "P_LOC_FLAT.csv";
    public static final String DATE_TIME_COLUMN_HEADER_NAME = "SES4LOC_TIMESTAMP";

    public LocationFlatFileModelService(){
        this.getHeaders().add("SES4LOC_PHONE_MCINTERCEPTID");
        this.getHeaders().add("SES4LOC_PHONE_MCAGENCYID");
        this.getHeaders().add("SES4LOC_PHONE_IDENTITY_VALUE");
        this.getHeaders().add("SES4LOC_IMSI_IDENTITY_VALUE");
        this.getHeaders().add("SES4LOC_IMEI_IDENTITY_VALUE");
        this.getHeaders().add("SES4LOC_GEOCOR_SPATIAL_X");
        this.getHeaders().add("SES4LOC_GEOCOR_SPATIAL_Y");
        this.getHeaders().add("SES4LOC_GEOCOR_GEOACCURACY");
        this.getHeaders().add("SES4LOC_CELL_CELLID_CGI");
        this.getHeaders().add("SES4LOC_TIMESTAMP");
        this.getHeaders().add("SYSTEMID");
    }

    @Override
    public String getDataCsv(RootFlatFileModel model) {
        StringBuilder sb = new StringBuilder();
        sb.append(System.lineSeparator());
        sb.append(model.getSES4LOC_PHONE_MCINTERCEPTID());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_PHONE_MCAGENCYID());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_PHONE_IDENTITY_VALUE());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_IMSI_IDENTITY_VALUE());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_IMEI_IDENTITY_VALUE());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_GEOCOR_SPATIAL_X());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_GEOCOR_SPATIAL_Y());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_GEOCOR_GEOACCURACY());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_CELL_CELLID_CGI());
        sb.append(delimeterForData);
        sb.append(model.getSES4LOC_TIMESTAMP());
        sb.append(delimeterForData);
        sb.append(model.getSYSTEMID());
        return sb.toString();
    }

    @Override
    public String getFileName() {
        return LocationFlatFileModelService.FILE_NAME;
    }

    @Override
    public String getDateColumnValue(RootFlatFileModel rootFlatFileModel) {
        return rootFlatFileModel.getSES4LOC_TIMESTAMP();
    }

    @Override
    public List<RootFlatFileModel> sortByDate() throws Exception {
        RootFlatFileModel[] rootRecords = getDataContainer();
        List<RootFlatFileModel> rootFlatFileModelList = Arrays.asList(rootRecords);

        rootFlatFileModelList.sort((o1, o2) -> {
            long date1 = getEpochSeconds(o1.getSES4LOC_TIMESTAMP());
            long date2 = getEpochSeconds(o2.getSES4LOC_TIMESTAMP());
            return date1 < date2 ? -1 : date1 > date2? 1 : 0;
        });

        rootFlatFileModelList.stream().map(rootFlatFileModel -> rootFlatFileModel.getSES4LOC_TIMESTAMP()).forEach(s -> {
//            log.info("Sorted: {}", s);
        });

        this.rootFlatFileModels = rootFlatFileModelList;
        log.info("{}, size: {} or {}", LocationFlatFileModelService.FILE_NAME, this.rootFlatFileModels.size(), rootFlatFileModelList.size());
        return rootFlatFileModelList;
    }

    public long getDateDiffToNewDate(LocalDateTime expectedDate) throws Exception {
        List<RootFlatFileModel> rootFlatFileModelList = this.rootFlatFileModels;
        RootFlatFileModel latestRecord = rootFlatFileModelList.get(rootFlatFileModelList.size() - 1);
        log.info("Current record date: {}", latestRecord.getSES4LOC_TIMESTAMP());
        LocalDateTime localDateTime = getConvertedLocalDateTime(latestRecord.getSES4LOC_TIMESTAMP());
        log.info("expected date: {}, the most latest record date: {}", expectedDate, localDateTime);
        return ChronoUnit.DAYS.between(expectedDate, localDateTime);
    }

    public void addDaysToCurrentDates(long days){
        this.rootFlatFileModels.forEach(rootFlatFileModel -> {
            LocalDateTime localDateTime = getConvertedLocalDateTime(rootFlatFileModel.getSES4LOC_TIMESTAMP());
            localDateTime = localDateTime.plusDays(days);
            rootFlatFileModel.setSES_PHONE_DIR1_STARTDATE(localDateTime.format(flatFileDateFormatter2));
        });
    }
}
