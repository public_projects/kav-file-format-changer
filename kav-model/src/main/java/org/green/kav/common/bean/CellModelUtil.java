package org.green.kav.common.bean;

import java.math.BigInteger;


public class CellModelUtil {
    public CellModelUtil() {
    }

    public static Cell of(CellGlobalIdentity cellGlobalIdentity, Location location) {
        if (cellGlobalIdentity != null && location != null) {
            String cgi = cellGlobalIdentity.toMcngEncodedString();
            if (!isValidCgi(cgi)) {
                throw new IllegalArgumentException();
            } else if (isEcgi(cgi)) {
                LteCell lteCell = new LteCell(cgi, location.getLatitude(), location.getLongitude());
                return lteCell;
            } else {
                PreLteCell lteCell = new PreLteCell(cgi, location.getLatitude(), location.getLongitude());
                return lteCell;
            }
        } else {
            return null;
        }
    }

    public static Cell of(String cgi, Location location, CellMetadata cellMetadata) {
        if (cgi != null && location != null) {
            if (!isValidCgi(cgi)) {
                throw new IllegalArgumentException();
            } else if (isEcgi(cgi)) {
                LteCell lteCell = new LteCell(cgi, location.getLatitude(), location.getLongitude());
                lteCell.setCellMetadata(cellMetadata);
                return lteCell;
            } else {
                PreLteCell lteCell = new PreLteCell(cgi, location.getLatitude(), location.getLongitude());
                lteCell.setCellMetadata(cellMetadata);
                return lteCell;
            }
        } else {
            return null;
        }
    }

    public static boolean isEcgi(String cgiString) {
        if (cgiString == null) {
            return false;
        } else {
            int nSegments = cgiString.split("-").length;
            return nSegments == 3;
        }
    }

    public static boolean isValidCgi(String cgiString) {
        if (cgiString != null && !cgiString.equals("0-0-0-0") && !cgiString.equals("0-0-0")) {
            int nSegments = cgiString.split("-").length;
            boolean hasCorrectSegmentCount = nSegments == 3 || nSegments == 4;
            boolean nonNullOrEmptySegments = nonNullOrEmptySegments(cgiString);
            return hasCorrectSegmentCount && nonNullOrEmptySegments;
        } else {
            return false;
        }
    }

    private static boolean nonNullOrEmptySegments(String cgiString) {
        String[] segments = cgiString.split("-");
        boolean isValid = true;
        String[] var3 = segments;
        int var4 = segments.length;

        for(int var5 = 0; var5 < var4; ++var5) {
            String segment = var3[var5];
            if (segment == null || segment.isEmpty()) {
                isValid = false;
            }

            try {
                new BigInteger(segment.trim());
            } catch (NumberFormatException var8) {
                isValid = false;
            }

            if (!isValid) {
                return false;
            }
        }

        return isValid;
    }
}
