package org.green.kav.common.bean;

public enum CallDirection {
    UNKNOWN,
    IN,
    OUT;

    private CallDirection() {
    }

    public static CallDirection of(int value) {
        switch(value) {
            case 1:
                return IN;
            case 2:
                return OUT;
            default:
                return UNKNOWN;
        }
    }

    public static CallDirection of(String value) {
        byte var2 = -1;
        switch(value.hashCode()) {
            case 2341:
                if (value.equals("IN")) {
                    var2 = 0;
                }
                break;
            case 78638:
                if (value.equals("OUT")) {
                    var2 = 1;
                }
        }

        switch(var2) {
            case 0:
                return IN;
            case 1:
                return OUT;
            default:
                return UNKNOWN;
        }
    }
}