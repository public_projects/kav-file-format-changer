package org.green.kav.common.bean;

import java.util.LinkedHashMap;
import java.util.Map;


public class CDRProperties extends LinkedHashMap<String, String> {
    public CDRProperties() {
    }

    public CDRProperties(Map<String, String> properties) {
        properties.forEach(this::put);
    }
}
