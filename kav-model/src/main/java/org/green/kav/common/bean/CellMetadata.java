package org.green.kav.common.bean;

import java.io.Serializable;


public class CellMetadata implements Serializable {
    private Double rightLatitude;
    private Double rightLongitude;
    private Double leftLatitude;
    private Double leftLongitude;
    private Double midLatitude;
    private Double midLongitude;
    private Double azimuth;
    private Double height;
    private Double coverageRadius;
    private Double mechanicalDowntilt;
    private Double btsTransmitterPower;
    private String towerName;
    private String cellName;
    private Double latitude;
    private Double longitude;
    private Long insertTimestamp;
    private String actualCgi;

    public CellMetadata() {
    }

    public CellMetadata(String actualCgi, Double latitude, Double longitude, Double rightLatitude, Double rightLongitude, Double leftLatitude, Double leftLongitude, Double midLatitude, Double midLongitude, Double azimuth, Double height, Double coverageRadius, Double mechanicalDowntilt, Double btsTransmitterPower, String towerName, String cellName, Long insertTimestamp) {
        this.actualCgi = actualCgi;
        this.latitude = latitude;
        this.longitude = longitude;
        this.rightLatitude = rightLatitude;
        this.rightLongitude = rightLongitude;
        this.leftLatitude = leftLatitude;
        this.leftLongitude = leftLongitude;
        this.midLatitude = midLatitude;
        this.midLongitude = midLongitude;
        this.azimuth = azimuth;
        this.height = height;
        this.coverageRadius = coverageRadius;
        this.mechanicalDowntilt = mechanicalDowntilt;
        this.btsTransmitterPower = btsTransmitterPower;
        this.towerName = towerName;
        this.cellName = cellName;
        this.insertTimestamp = insertTimestamp;
    }

    public String getActualCgi() {
        return this.actualCgi;
    }

    public void setActualCgi(String actualCgi) {
        this.actualCgi = actualCgi;
    }

    public Double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getRightLatitude() {
        return this.rightLatitude;
    }

    public void setRightLatitude(Double rightLatitude) {
        this.rightLatitude = rightLatitude;
    }

    public Double getRightLongitude() {
        return this.rightLongitude;
    }

    public void setRightLongitude(Double rightLongitude) {
        this.rightLongitude = rightLongitude;
    }

    public Double getLeftLatitude() {
        return this.leftLatitude;
    }

    public void setLeftLatitude(Double leftLatitude) {
        this.leftLatitude = leftLatitude;
    }

    public Double getLeftLongitude() {
        return this.leftLongitude;
    }

    public void setLeftLongitude(Double leftLongitude) {
        this.leftLongitude = leftLongitude;
    }

    public Double getMidLatitude() {
        return this.midLatitude;
    }

    public void setMidLatitude(Double midLatitude) {
        this.midLatitude = midLatitude;
    }

    public Double getMidLongitude() {
        return this.midLongitude;
    }

    public void setMidLongitude(Double midLongitude) {
        this.midLongitude = midLongitude;
    }

    public Double getAzimuth() {
        return this.azimuth;
    }

    public void setAzimuth(Double azimuth) {
        this.azimuth = azimuth;
    }

    public Double getHeight() {
        return this.height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getCoverageRadius() {
        return this.coverageRadius;
    }

    public void setCoverageRadius(Double coverageRadius) {
        this.coverageRadius = coverageRadius;
    }

    public Double getMechanicalDowntilt() {
        return this.mechanicalDowntilt;
    }

    public void setMechanicalDowntilt(Double mechanicalDowntilt) {
        this.mechanicalDowntilt = mechanicalDowntilt;
    }

    public Double getBtsTransmitterPower() {
        return this.btsTransmitterPower;
    }

    public void setBtsTransmitterPower(Double btsTransmitterPower) {
        this.btsTransmitterPower = btsTransmitterPower;
    }

    public String getTowerName() {
        return this.towerName;
    }

    public void setTowerName(String towerName) {
        this.towerName = towerName;
    }

    public String getCellName() {
        return this.cellName;
    }

    public void setCellName(String cellName) {
        this.cellName = cellName;
    }

    public Long getInsertTimestamp() {
        return this.insertTimestamp;
    }

    public void setInsertTimestamp(Long insertTimestamp) {
        this.insertTimestamp = insertTimestamp;
    }

    public Location getLocation() {
        return Location.of(this.latitude, this.longitude);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            CellMetadata that = (CellMetadata)o;
            if (this.getRightLatitude() != null) {
                if (!this.getRightLatitude().equals(that.getRightLatitude())) {
                    return false;
                }
            } else if (that.getRightLatitude() != null) {
                return false;
            }

            label190: {
                if (this.getRightLongitude() != null) {
                    if (this.getRightLongitude().equals(that.getRightLongitude())) {
                        break label190;
                    }
                } else if (that.getRightLongitude() == null) {
                    break label190;
                }

                return false;
            }

            label183: {
                if (this.getLeftLatitude() != null) {
                    if (this.getLeftLatitude().equals(that.getLeftLatitude())) {
                        break label183;
                    }
                } else if (that.getLeftLatitude() == null) {
                    break label183;
                }

                return false;
            }

            if (this.getLeftLongitude() != null) {
                if (!this.getLeftLongitude().equals(that.getLeftLongitude())) {
                    return false;
                }
            } else if (that.getLeftLongitude() != null) {
                return false;
            }

            label169: {
                if (this.getMidLatitude() != null) {
                    if (this.getMidLatitude().equals(that.getMidLatitude())) {
                        break label169;
                    }
                } else if (that.getMidLatitude() == null) {
                    break label169;
                }

                return false;
            }

            if (this.getMidLongitude() != null) {
                if (!this.getMidLongitude().equals(that.getMidLongitude())) {
                    return false;
                }
            } else if (that.getMidLongitude() != null) {
                return false;
            }

            label155: {
                if (this.getAzimuth() != null) {
                    if (this.getAzimuth().equals(that.getAzimuth())) {
                        break label155;
                    }
                } else if (that.getAzimuth() == null) {
                    break label155;
                }

                return false;
            }

            if (this.getHeight() != null) {
                if (!this.getHeight().equals(that.getHeight())) {
                    return false;
                }
            } else if (that.getHeight() != null) {
                return false;
            }

            if (this.getCoverageRadius() != null) {
                if (!this.getCoverageRadius().equals(that.getCoverageRadius())) {
                    return false;
                }
            } else if (that.getCoverageRadius() != null) {
                return false;
            }

            label134: {
                if (this.getMechanicalDowntilt() != null) {
                    if (this.getMechanicalDowntilt().equals(that.getMechanicalDowntilt())) {
                        break label134;
                    }
                } else if (that.getMechanicalDowntilt() == null) {
                    break label134;
                }

                return false;
            }

            label127: {
                if (this.getBtsTransmitterPower() != null) {
                    if (this.getBtsTransmitterPower().equals(that.getBtsTransmitterPower())) {
                        break label127;
                    }
                } else if (that.getBtsTransmitterPower() == null) {
                    break label127;
                }

                return false;
            }

            if (this.getTowerName() != null) {
                if (!this.getTowerName().equals(that.getTowerName())) {
                    return false;
                }
            } else if (that.getTowerName() != null) {
                return false;
            }

            if (this.getCellName() != null) {
                if (!this.getCellName().equals(that.getCellName())) {
                    return false;
                }
            } else if (that.getCellName() != null) {
                return false;
            }

            label106: {
                if (this.getLatitude() != null) {
                    if (this.getLatitude().equals(that.getLatitude())) {
                        break label106;
                    }
                } else if (that.getLatitude() == null) {
                    break label106;
                }

                return false;
            }

            if (this.getLongitude() != null) {
                if (!this.getLongitude().equals(that.getLongitude())) {
                    return false;
                }
            } else if (that.getLongitude() != null) {
                return false;
            }

            return this.getInsertTimestamp() != null ? this.getInsertTimestamp().equals(that.getInsertTimestamp()) : that.getInsertTimestamp() == null;
        } else {
            return false;
        }
    }

    public int hashCode() {
        int result = this.getRightLatitude() != null ? this.getRightLatitude().hashCode() : 0;
        result = 31 * result + (this.getRightLongitude() != null ? this.getRightLongitude().hashCode() : 0);
        result = 31 * result + (this.getLeftLatitude() != null ? this.getLeftLatitude().hashCode() : 0);
        result = 31 * result + (this.getLeftLongitude() != null ? this.getLeftLongitude().hashCode() : 0);
        result = 31 * result + (this.getMidLatitude() != null ? this.getMidLatitude().hashCode() : 0);
        result = 31 * result + (this.getMidLongitude() != null ? this.getMidLongitude().hashCode() : 0);
        result = 31 * result + (this.getAzimuth() != null ? this.getAzimuth().hashCode() : 0);
        result = 31 * result + (this.getHeight() != null ? this.getHeight().hashCode() : 0);
        result = 31 * result + (this.getCoverageRadius() != null ? this.getCoverageRadius().hashCode() : 0);
        result = 31 * result + (this.getMechanicalDowntilt() != null ? this.getMechanicalDowntilt().hashCode() : 0);
        result = 31 * result + (this.getBtsTransmitterPower() != null ? this.getBtsTransmitterPower().hashCode() : 0);
        result = 31 * result + (this.getTowerName() != null ? this.getTowerName().hashCode() : 0);
        result = 31 * result + (this.getCellName() != null ? this.getCellName().hashCode() : 0);
        result = 31 * result + (this.getLatitude() != null ? this.getLatitude().hashCode() : 0);
        result = 31 * result + (this.getLongitude() != null ? this.getLongitude().hashCode() : 0);
        result = 31 * result + (this.getInsertTimestamp() != null ? this.getInsertTimestamp().hashCode() : 0);
        return result;
    }

    public String toString() {
        return "CellMetadata{rightLatitude=" + this.rightLatitude + ", rightLongitude=" + this.rightLongitude + ", leftLatitude=" + this.leftLatitude + ", leftLongitude=" + this.leftLongitude + ", midLatitude=" + this.midLatitude + ", midLongitude=" + this.midLongitude + ", azimuth=" + this.azimuth + ", height=" + this.height + ", coverageRadius=" + this.coverageRadius + ", mechanicalDowntilt=" + this.mechanicalDowntilt + ", btsTransmitterPower=" + this.btsTransmitterPower + ", towerName='" + this.towerName + '\'' + ", cellName='" + this.cellName + '\'' + ", latitude=" + this.latitude + ", longitude=" + this.longitude + ", insertTimestamp=" + this.insertTimestamp + '}';
    }
}
