package org.green.kav.common.bean.flat.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RootFlatFileModel {
    @JsonProperty("SES4LOC_PHONE_MCINTERCEPTID")
    private String sES4LOC_PHONE_MCINTERCEPTID;
    @JsonProperty("SES4LOC_PHONE_MCAGENCYID")
    private String sES4LOC_PHONE_MCAGENCYID;
    @JsonProperty("SES4LOC_PHONE_IDENTITY_VALUE")
    private String sES4LOC_PHONE_IDENTITY_VALUE;
    @JsonProperty("SES4LOC_IMSI_IDENTITY_VALUE")
    private String sES4LOC_IMSI_IDENTITY_VALUE;
    @JsonProperty("SES4LOC_IMEI_IDENTITY_VALUE")
    private String sES4LOC_IMEI_IDENTITY_VALUE;
    @JsonProperty("SES4LOC_GEOCOR_SPATIAL_X")
    private String sES4LOC_GEOCOR_SPATIAL_X;
    @JsonProperty("SES4LOC_GEOCOR_SPATIAL_Y")
    private String sES4LOC_GEOCOR_SPATIAL_Y;
    @JsonProperty("SES4LOC_GEOCOR_GEOACCURACY")
    private String sES4LOC_GEOCOR_GEOACCURACY;
    @JsonProperty("SES4LOC_CELL_CELLID_CGI")
    private String sES4LOC_CELL_CELLID_CGI;
    @JsonProperty("SES4LOC_TIMESTAMP")
    private String sES4LOC_TIMESTAMP;
    @JsonProperty("SESSIONLOG_MCINTERCEPTID")
    private Integer sESSIONLOG_MCINTERCEPTID;
    @JsonProperty("SES_PHONE_DIR1_IDENTITY_VALUE")
    private Long sES_PHONE_DIR1_IDENTITY_VALUE;
    @JsonProperty("SES_IMSI_IDENTITY_VALUE")
    private Long sES_IMSI_IDENTITY_VALUE;
    @JsonProperty("SES_IMEI_IDENTITY_VALUE")
    private Long sES_IMEI_IDENTITY_VALUE;
    @JsonProperty("SES_PHONE_DIR2_IDENTITY_VALUE")
    private String sES_PHONE_DIR2_IDENTITY_VALUE;
    @JsonProperty("SES_PHONE_DIR1_STARTDATE")
    private String sES_PHONE_DIR1_STARTDATE;
    @JsonProperty("SES_PHONE_DIR1_DURATION")
    private Integer sES_PHONE_DIR1_DURATION;
    @JsonProperty("SES_PHONE_DIR1_SESSIONTYPE")
    private Integer sES_PHONE_DIR1_SESSIONTYPE;
    @JsonProperty("SES_PHONE_DIR1_INID_DIRECTION")
    private Integer sES_PHONE_DIR1_INID_DIRECTION;
    @JsonProperty("SES_PHONE_DIR1_TRIG_TARGETID")
    private Integer sES_PHONE_DIR1_TRIG_TARGETID;
    @JsonProperty("SES_PHONE_DIR1_INID_MARKEDTYPE")
    private Integer sES_PHONE_DIR1_INID_MARKEDTYPE;
    @JsonProperty("SES_IMSI_INID_MARKEDTYPE")
    private Integer sES_IMSI_INID_MARKEDTYPE;
    @JsonProperty("SES_IMEI_INID_MARKEDTYPE")
    private Integer sES_IMEI_INID_MARKEDTYPE;
    @JsonProperty("SES_PHONE_DIR2_INID_MARKEDTYPE")
    private Integer sES_PHONE_DIR2_INID_MARKEDTYPE;
    @JsonProperty("SES_PHONE_DIR1_TRIG_TRIGGERID")
    private String sES_PHONE_DIR1_TRIG_TRIGGERID;
    @JsonProperty("SES_PHONE_DIR1_INID_MCAGENCYID")
    private Integer sES_PHONE_DIR1_INID_MCAGENCYID;
    @JsonProperty("SES_PHONE_DIR1_CGI")
    private String sES_PHONE_DIR1_CGI;
    @JsonProperty("SES_PHONE_DIR1_CELL_NETWORK")
    private String sES_PHONE_DIR1_CELL_NETWORK;
    @JsonProperty("SYSTEMID")
    private String sYSTEMID;
    @JsonProperty("CONTENT_LOCATORS")
    private String cONTENT_LOCATORS;
    @JsonProperty("QUICKINFO")
    private String qUICKINFO;
    @JsonProperty("APNNAME")
    private String aPNNAME;
    @JsonProperty("SIP_CALLID")
    private String sIP_CALLID;
    @JsonProperty("SIP_EVENT")
    private String sIP_EVENT;
    @JsonProperty("USERAGENT")
    private String uSERAGENT;
    @JsonProperty("PROVIDERINFO")
    private String pROVIDERINFO;


    public String getSES4LOC_PHONE_MCINTERCEPTID() {
        return sES4LOC_PHONE_MCINTERCEPTID;
    }

    public void setSES4LOC_PHONE_MCINTERCEPTID(String sES4LOC_PHONE_MCINTERCEPTID) {
        this.sES4LOC_PHONE_MCINTERCEPTID = sES4LOC_PHONE_MCINTERCEPTID;
    }

    public String getSES4LOC_PHONE_MCAGENCYID() {
        return sES4LOC_PHONE_MCAGENCYID;
    }

    public void setSES4LOC_PHONE_MCAGENCYID(String sES4LOC_PHONE_MCAGENCYID) {
        this.sES4LOC_PHONE_MCAGENCYID = sES4LOC_PHONE_MCAGENCYID;
    }

    public String getSES4LOC_PHONE_IDENTITY_VALUE() {
        return sES4LOC_PHONE_IDENTITY_VALUE;
    }

    public void setSES4LOC_PHONE_IDENTITY_VALUE(String sES4LOC_PHONE_IDENTITY_VALUE) {
        this.sES4LOC_PHONE_IDENTITY_VALUE = sES4LOC_PHONE_IDENTITY_VALUE;
    }

    public String getSES4LOC_IMSI_IDENTITY_VALUE() {
        return sES4LOC_IMSI_IDENTITY_VALUE;
    }

    public void setSES4LOC_IMSI_IDENTITY_VALUE(String sES4LOC_IMSI_IDENTITY_VALUE) {
        this.sES4LOC_IMSI_IDENTITY_VALUE = sES4LOC_IMSI_IDENTITY_VALUE;
    }

    public String getSES4LOC_IMEI_IDENTITY_VALUE() {
        return sES4LOC_IMEI_IDENTITY_VALUE;
    }

    public void setSES4LOC_IMEI_IDENTITY_VALUE(String sES4LOC_IMEI_IDENTITY_VALUE) {
        this.sES4LOC_IMEI_IDENTITY_VALUE = sES4LOC_IMEI_IDENTITY_VALUE;
    }

    public String getSES4LOC_GEOCOR_SPATIAL_X() {
        return sES4LOC_GEOCOR_SPATIAL_X;
    }

    public void setSES4LOC_GEOCOR_SPATIAL_X(String sES4LOC_GEOCOR_SPATIAL_X) {
        this.sES4LOC_GEOCOR_SPATIAL_X = sES4LOC_GEOCOR_SPATIAL_X;
    }

    public String getSES4LOC_GEOCOR_SPATIAL_Y() {
        return sES4LOC_GEOCOR_SPATIAL_Y;
    }

    public void setSES4LOC_GEOCOR_SPATIAL_Y(String sES4LOC_GEOCOR_SPATIAL_Y) {
        this.sES4LOC_GEOCOR_SPATIAL_Y = sES4LOC_GEOCOR_SPATIAL_Y;
    }

    public String getSES4LOC_GEOCOR_GEOACCURACY() {
        return sES4LOC_GEOCOR_GEOACCURACY;
    }

    public void setSES4LOC_GEOCOR_GEOACCURACY(String sES4LOC_GEOCOR_GEOACCURACY) {
        this.sES4LOC_GEOCOR_GEOACCURACY = sES4LOC_GEOCOR_GEOACCURACY;
    }

    public String getSES4LOC_CELL_CELLID_CGI() {
        return sES4LOC_CELL_CELLID_CGI;
    }

    public void setSES4LOC_CELL_CELLID_CGI(String sES4LOC_CELL_CELLID_CGI) {
        this.sES4LOC_CELL_CELLID_CGI = sES4LOC_CELL_CELLID_CGI;
    }

    public String getSES4LOC_TIMESTAMP() {
        return sES4LOC_TIMESTAMP;
    }

    public void setSES4LOC_TIMESTAMP(String sES4LOC_TIMESTAMP) {
        this.sES4LOC_TIMESTAMP = sES4LOC_TIMESTAMP;
    }

    public Integer getSESSIONLOG_MCINTERCEPTID() {
        return sESSIONLOG_MCINTERCEPTID;
    }

    public void setSESSIONLOG_MCINTERCEPTID(Integer sESSIONLOG_MCINTERCEPTID) {
        this.sESSIONLOG_MCINTERCEPTID = sESSIONLOG_MCINTERCEPTID;
    }

    public Long getSES_PHONE_DIR1_IDENTITY_VALUE() {
        return sES_PHONE_DIR1_IDENTITY_VALUE;
    }

    public void setSES_PHONE_DIR1_IDENTITY_VALUE(Long sES_PHONE_DIR1_IDENTITY_VALUE) {
        this.sES_PHONE_DIR1_IDENTITY_VALUE = sES_PHONE_DIR1_IDENTITY_VALUE;
    }

    public Long getSES_IMSI_IDENTITY_VALUE() {
        return sES_IMSI_IDENTITY_VALUE;
    }

    public void setSES_IMSI_IDENTITY_VALUE(Long sES_IMSI_IDENTITY_VALUE) {
        this.sES_IMSI_IDENTITY_VALUE = sES_IMSI_IDENTITY_VALUE;
    }

    public Long getSES_IMEI_IDENTITY_VALUE() {
        return sES_IMEI_IDENTITY_VALUE;
    }

    public void setSES_IMEI_IDENTITY_VALUE(Long sES_IMEI_IDENTITY_VALUE) {
        this.sES_IMEI_IDENTITY_VALUE = sES_IMEI_IDENTITY_VALUE;
    }

    public String getSES_PHONE_DIR2_IDENTITY_VALUE() {
        return sES_PHONE_DIR2_IDENTITY_VALUE;
    }

    public void setSES_PHONE_DIR2_IDENTITY_VALUE(String sES_PHONE_DIR2_IDENTITY_VALUE) {
        this.sES_PHONE_DIR2_IDENTITY_VALUE = sES_PHONE_DIR2_IDENTITY_VALUE;
    }

    public String getSES_PHONE_DIR1_STARTDATE() {
        return sES_PHONE_DIR1_STARTDATE;
    }

    public void setSES_PHONE_DIR1_STARTDATE(String sES_PHONE_DIR1_STARTDATE) {
        this.sES_PHONE_DIR1_STARTDATE = sES_PHONE_DIR1_STARTDATE;
    }

    public Integer getSES_PHONE_DIR1_DURATION() {
        return sES_PHONE_DIR1_DURATION;
    }

    public void setSES_PHONE_DIR1_DURATION(Integer sES_PHONE_DIR1_DURATION) {
        this.sES_PHONE_DIR1_DURATION = sES_PHONE_DIR1_DURATION;
    }

    public Integer getSES_PHONE_DIR1_SESSIONTYPE() {
        return sES_PHONE_DIR1_SESSIONTYPE;
    }

    public void setSES_PHONE_DIR1_SESSIONTYPE(Integer sES_PHONE_DIR1_SESSIONTYPE) {
        this.sES_PHONE_DIR1_SESSIONTYPE = sES_PHONE_DIR1_SESSIONTYPE;
    }

    public Integer getSES_PHONE_DIR1_INID_DIRECTION() {
        return sES_PHONE_DIR1_INID_DIRECTION;
    }

    public void setSES_PHONE_DIR1_INID_DIRECTION(Integer sES_PHONE_DIR1_INID_DIRECTION) {
        this.sES_PHONE_DIR1_INID_DIRECTION = sES_PHONE_DIR1_INID_DIRECTION;
    }

    public Integer getSES_PHONE_DIR1_TRIG_TARGETID() {
        return sES_PHONE_DIR1_TRIG_TARGETID;
    }

    public void setSES_PHONE_DIR1_TRIG_TARGETID(Integer sES_PHONE_DIR1_TRIG_TARGETID) {
        this.sES_PHONE_DIR1_TRIG_TARGETID = sES_PHONE_DIR1_TRIG_TARGETID;
    }

    public Integer getSES_PHONE_DIR1_INID_MARKEDTYPE() {
        return sES_PHONE_DIR1_INID_MARKEDTYPE;
    }

    public void setSES_PHONE_DIR1_INID_MARKEDTYPE(Integer sES_PHONE_DIR1_INID_MARKEDTYPE) {
        this.sES_PHONE_DIR1_INID_MARKEDTYPE = sES_PHONE_DIR1_INID_MARKEDTYPE;
    }

    public Integer getSES_IMSI_INID_MARKEDTYPE() {
        return sES_IMSI_INID_MARKEDTYPE;
    }

    public void setSES_IMSI_INID_MARKEDTYPE(Integer sES_IMSI_INID_MARKEDTYPE) {
        this.sES_IMSI_INID_MARKEDTYPE = sES_IMSI_INID_MARKEDTYPE;
    }

    public Integer getSES_IMEI_INID_MARKEDTYPE() {
        return sES_IMEI_INID_MARKEDTYPE;
    }

    public void setSES_IMEI_INID_MARKEDTYPE(Integer sES_IMEI_INID_MARKEDTYPE) {
        this.sES_IMEI_INID_MARKEDTYPE = sES_IMEI_INID_MARKEDTYPE;
    }

    public Integer getSES_PHONE_DIR2_INID_MARKEDTYPE() {
        return sES_PHONE_DIR2_INID_MARKEDTYPE;
    }

    public void setSES_PHONE_DIR2_INID_MARKEDTYPE(Integer sES_PHONE_DIR2_INID_MARKEDTYPE) {
        this.sES_PHONE_DIR2_INID_MARKEDTYPE = sES_PHONE_DIR2_INID_MARKEDTYPE;
    }

    public String getSES_PHONE_DIR1_TRIG_TRIGGERID() {
        return sES_PHONE_DIR1_TRIG_TRIGGERID;
    }

    public void setSES_PHONE_DIR1_TRIG_TRIGGERID(String sES_PHONE_DIR1_TRIG_TRIGGERID) {
        this.sES_PHONE_DIR1_TRIG_TRIGGERID = sES_PHONE_DIR1_TRIG_TRIGGERID;
    }

    public Integer getSES_PHONE_DIR1_INID_MCAGENCYID() {
        return sES_PHONE_DIR1_INID_MCAGENCYID;
    }

    public void setSES_PHONE_DIR1_INID_MCAGENCYID(Integer sES_PHONE_DIR1_INID_MCAGENCYID) {
        this.sES_PHONE_DIR1_INID_MCAGENCYID = sES_PHONE_DIR1_INID_MCAGENCYID;
    }

    public String getSES_PHONE_DIR1_CGI() {
        return sES_PHONE_DIR1_CGI;
    }

    public void setSES_PHONE_DIR1_CGI(String sES_PHONE_DIR1_CGI) {
        this.sES_PHONE_DIR1_CGI = sES_PHONE_DIR1_CGI;
    }

    public String getSES_PHONE_DIR1_CELL_NETWORK() {
        return sES_PHONE_DIR1_CELL_NETWORK;
    }

    public void setES_PHONE_DIR1_CELL_NETWORK(String sES_PHONE_DIR1_CELL_NETWORK) {
        this.sES_PHONE_DIR1_CELL_NETWORK = sES_PHONE_DIR1_CELL_NETWORK;
    }

    public String getSYSTEMID() {
        return sYSTEMID;
    }

    public void setSYSTEMID(String sYSTEMID) {
        this.sYSTEMID = sYSTEMID;
    }

    public String getCONTENT_LOCATORS() {
        return cONTENT_LOCATORS;
    }

    public void setCONTENT_LOCATORS(String cONTENT_LOCATORS) {
        this.cONTENT_LOCATORS = cONTENT_LOCATORS;
    }

    public String getQUICKINFO() {
        return qUICKINFO;
    }

    public void setQUICKINFO(String qUICKINFO) {
        this.qUICKINFO = qUICKINFO;
    }

    public String getAPNNAME() {
        return aPNNAME;
    }

    public void setAPNNAME(String aPNNAME) {
        this.aPNNAME = aPNNAME;
    }

    public String getSIP_CALLID() {
        return sIP_CALLID;
    }

    public void setSIP_CALLID(String sIP_CALLID) {
        this.sIP_CALLID = sIP_CALLID;
    }

    public String getSIP_EVENT() {
        return sIP_EVENT;
    }

    public void setSIP_EVENT(String sIP_EVENT) {
        this.sIP_EVENT = sIP_EVENT;
    }

    public String getUSERAGENT() {
        return uSERAGENT;
    }

    public void setUSERAGENT(String uSERAGENT) {
        this.uSERAGENT = uSERAGENT;
    }

    public String getPROVIDERINFO() {
        return pROVIDERINFO;
    }

    public void setPROVIDERINFO(String pROVIDERINFO) {
        this.pROVIDERINFO = pROVIDERINFO;
    }
}
