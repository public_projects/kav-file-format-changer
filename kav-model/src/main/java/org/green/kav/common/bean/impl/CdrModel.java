package org.green.kav.common.bean.impl;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.bean.ContentType;

import java.util.ArrayList;
import java.util.List;


@Slf4j
public class CdrModel implements ContentType {
    private String provider;
    private String eventtype;
    private String start;
    private String duration;
    private String status;
    private String cgistarta;
    private String cgistartb;
    private String latstarta;
    private String latstartb;
    private String lonstarta;
    private String lonstartb;
    private String cgienda;
    private String cgiendb;
    private String latenda;
    private String latendb;
    private String lonenda;
    private String lonendb;
    private String msisdna;
    private String msisdnb;
    private String msisdnc;
    private String imeia;
    private String imeib;
    private String imsia;
    private String imsib;

    @Override
    public String toString() {
        return "CdrModel{" + "provider='" + provider + '\'' + ", eventtype='" + eventtype + '\'' + ", start='" + start + '\'' + ", duration='" +
               duration + '\'' + ", status='" + status + '\'' + ", cgistarta='" + cgistarta + '\'' + ", cgistartb='" + cgistartb + '\'' +
               ", latstarta='" + latstarta + '\'' + ", latstartb='" + latstartb + '\'' + ", lonstarta='" + lonstarta + '\'' +
               ", lonstartb='" + lonstartb + '\'' + ", cgienda='" + cgienda + '\'' + ", cgiendb='" + cgiendb + '\'' + ", latenda='" +
               latenda + '\'' + ", latendb='" + latendb + '\'' + ", lonenda='" + lonenda + '\'' + ", lonendb='" + lonendb + '\'' +
               ", msisdna='" + msisdna + '\'' + ", msisdnb='" + msisdnb + '\'' + ", msisdnc='" + msisdnc + '\'' + ", imeia='" + imeia +
               '\'' + ", imeib='" + imeib + '\'' + ", imsia='" + imsia + '\'' + ", imsib='" + imsib + '\'' + '}';
    }

    @Override
    public List<ContentType> mapToPojo(List<List<String>> dataLs) {
        StringBuilder simpleCsv = new StringBuilder();
        simpleCsv.append("start,duration,msisdna,cgistarta,cgistartb,msisdnb").append(System.lineSeparator());

        List<ContentType> cdrModelLS = new ArrayList<>();
        dataLs.forEach(record -> {
            CdrModel cdrModel = new CdrModel();
            cdrModel.setProvider(record.get(0));
            cdrModel.setEventtype(record.get(1));
            cdrModel.setStart(record.get(2));
            cdrModel.setDuration(record.get(3));
            cdrModel.setStatus(record.get(4));
            cdrModel.setCgistarta(record.get(5));
            cdrModel.setCgistartb(record.get(6));
            cdrModel.setLatstarta(record.get(7));
            cdrModel.setLatstartb(record.get(8));
            cdrModel.setLonstarta(record.get(9));
            cdrModel.setLonstartb(record.get(10));
            cdrModel.setCgienda(record.get(11));
            cdrModel.setCgiendb(record.get(12));
            cdrModel.setLatenda(record.get(13));
            cdrModel.setLatendb(record.get(14));
            cdrModel.setLonenda(record.get(15));
            cdrModel.setLonendb(record.get(16));
            cdrModel.setMsisdna(record.get(17));
            cdrModel.setMsisdnb(record.get(18));
            cdrModel.setMsisdnc(record.get(19));
            cdrModel.setImeia(record.get(20));
            cdrModel.setImeib(record.get(21));
            cdrModel.setImsia(record.get(22));
            cdrModel.setImsib(record.get(23));
            simpleCsv.append(cdrModel.getStart()).append(",");
            simpleCsv.append(cdrModel.getDuration()).append(",");
            simpleCsv.append(cdrModel.getMsisdna()).append(",");
            simpleCsv.append(cdrModel.getCgistarta()).append(",");
            simpleCsv.append(cdrModel.getCgistartb()).append(",");
            simpleCsv.append(cdrModel.getMsisdnb()).append(System.lineSeparator());
            cdrModelLS.add(cdrModel);
        });
        log.info("Simple csv: {}", simpleCsv.toString());
        return cdrModelLS;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEventtype() {
        return eventtype;
    }

    public void setEventtype(String eventtype) {
        this.eventtype = eventtype;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCgistarta() {
        return cgistarta;
    }

    public void setCgistarta(String cgistarta) {
        this.cgistarta = cgistarta;
    }

    public String getCgistartb() {
        return cgistartb;
    }

    public void setCgistartb(String cgistartb) {
        this.cgistartb = cgistartb;
    }

    public String getLatstarta() {
        return latstarta;
    }

    public void setLatstarta(String latstarta) {
        this.latstarta = latstarta;
    }

    public String getLatstartb() {
        return latstartb;
    }

    public void setLatstartb(String latstartb) {
        this.latstartb = latstartb;
    }

    public String getLonstarta() {
        return lonstarta;
    }

    public void setLonstarta(String lonstarta) {
        this.lonstarta = lonstarta;
    }

    public String getLonstartb() {
        return lonstartb;
    }

    public void setLonstartb(String lonstartb) {
        this.lonstartb = lonstartb;
    }

    public String getCgienda() {
        return cgienda;
    }

    public void setCgienda(String cgienda) {
        this.cgienda = cgienda;
    }

    public String getCgiendb() {
        return cgiendb;
    }

    public void setCgiendb(String cgiendb) {
        this.cgiendb = cgiendb;
    }

    public String getLatenda() {
        return latenda;
    }

    public void setLatenda(String latenda) {
        this.latenda = latenda;
    }

    public String getLatendb() {
        return latendb;
    }

    public void setLatendb(String latendb) {
        this.latendb = latendb;
    }

    public String getLonenda() {
        return lonenda;
    }

    public void setLonenda(String lonenda) {
        this.lonenda = lonenda;
    }

    public String getLonendb() {
        return lonendb;
    }

    public void setLonendb(String lonendb) {
        this.lonendb = lonendb;
    }

    public String getMsisdna() {
        return msisdna;
    }

    public void setMsisdna(String msisdna) {
        this.msisdna = msisdna;
    }

    public String getMsisdnb() {
        return msisdnb;
    }

    public void setMsisdnb(String msisdnb) {
        this.msisdnb = msisdnb;
    }

    public String getMsisdnc() {
        return msisdnc;
    }

    public void setMsisdnc(String msisdnc) {
        this.msisdnc = msisdnc;
    }

    public String getImeia() {
        return imeia;
    }

    public void setImeia(String imeia) {
        this.imeia = imeia;
    }

    public String getImeib() {
        return imeib;
    }

    public void setImeib(String imeib) {
        this.imeib = imeib;
    }

    public String getImsia() {
        return imsia;
    }

    public void setImsia(String imsia) {
        this.imsia = imsia;
    }

    public String getImsib() {
        return imsib;
    }

    public void setImsib(String imsib) {
        this.imsib = imsib;
    }
}
