package org.green.kav.common.bean;

/**
 * Created by ys03 on 3/27/2018.
 */
public interface Constants {

    String CGI_SEPARATOR = "-";

    interface CELL {

        /** Cell CSV header constants */
        String CGI = "cgi";
        String LATITUDE = "latitude";
        String LONGITUDE = "longitude";
        String MCC = "mcc";
        String MNC = "mnc";
        String LAC = "lac";
        String CELLID = "cellId";
        String ECELLID = "eCellId";
        String RIGHT_LATITUDE = "right_latitude";
        String RIGHT_LONGITUDE = "right_longitude";
        String LEFT_LATITUDE = "left_latitude";
        String LEFT_LONGITUDE = "left_longitude";
        String MID_LATITUDE = "mid_latitude";
        String MID_LONGITUDE = "mid_longitude";
        String AZIMUTH = "azimuth";
        String HEIGHT = "height";
        String COVERAGE_RADIUS = "coverage_radius";
        String MECHANICAL_DOWNTILT = "mechanical_downtilt";
        String BTS_TRANSMITTER_POWER = "bts_transmitter_power";
        String TOWER_NAME = "tower_name";
        String CELL_NAME = "cell_name";
    }

}
