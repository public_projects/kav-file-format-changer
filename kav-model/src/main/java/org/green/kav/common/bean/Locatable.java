/*
 * Copyright (c) 2016, trovicor GmbH. All rights reserved. COMPANY CONFIDENTIAL.
 */
package org.green.kav.common.bean;

/**
 * Object that can be located on the Earth's surface.
 */
public interface Locatable {

    /**
     * Returns the location of this object.
     *
     * @return the location of this object
     */
    Location getLocation();
}
