package org.green.kav.common.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


public class CDR implements Comparable<CDR>, Serializable {
    public static final String StartFormatPattern = "yyyyMMddHHmmss";
    public static final DateTimeFormatter StartFormatter;
    private UUID uuid;
    private String provider;
    private CallType callType;
    private long start;
    private int duration;
    private int direction;
    private String msisdnA;
    private String msisdnB;
    private String msisdnC;
    private String imsiA;
    private String imsiB;
    private String imeiA;
    private String imeiB;
    private String status;
    private List<Cell> cellsA;
    private List<Cell> cellsB;
    private List<Location> locationsA;
    private List<Location> locationsB;
    private String countryA;
    private String countryB;
    private String smsContent;
    private List<String> voiceContent;
    private Source source;
    private CDRProperties properties;

    public CDR() {
    }

    private CDR(UUID uuid,
                String provider,
                CallType callType,
                long start,
                int duration,
                CallDirection direction,
                String msisdnA,
                String msisdnB) {
        this.uuid = uuid;
        this.provider = provider;
        this.callType = callType;
        this.start = start;
        this.duration = duration;
        this.direction = direction.ordinal();
        this.msisdnA = msisdnA;
        this.msisdnB = msisdnB;
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public String getProvider() {
        return this.provider;
    }

    public CallType getCallType() {
        return this.callType;
    }

    public long getStart() {
        return this.start;
    }

    public int getDuration() {
        return this.duration;
    }

    public int getDirection() {
        return this.direction;
    }

    public CallDirection getDirectionEnum() {
        return CallDirection.of(this.direction);
    }

    public String getMsisdnA() {
        return this.msisdnA;
    }

    public String getMsisdnB() {
        return this.msisdnB;
    }

    public String getMsisdnC() {
        return this.msisdnC;
    }

    public String getImsiA() {
        return this.imsiA;
    }

    public String getImsiB() {
        return this.imsiB;
    }

    public String getImeiA() {
        return this.imeiA;
    }

    public String getImeiB() {
        return this.imeiB;
    }

    public String getStatus() {
        return this.status;
    }

    public List<Cell> getCellsA() {
        return this.cellsA;
    }

    public List<Cell> getCellsB() {
        return this.cellsB;
    }

    public List<Location> getLocationsA() {
        return this.locationsA;
    }

    public List<Location> getLocationsB() {
        return this.locationsB;
    }

    public String getCountryA() {
        return this.countryA;
    }

    public String getCountryB() {
        return this.countryB;
    }

    public String getSmsContent() {
        return this.smsContent;
    }

    public List<String> getVoiceContent() {
        return this.voiceContent;
    }

    public Optional<Source> getSource() {
        return Optional.ofNullable(this.source);
    }

    public CDRProperties getProperties() {
        return this.properties;
    }

    public long getTimestamp() {
        return this.start;
    }

    public String getStartFormatted() {
        return this.getStartAsDateTime().format(StartFormatter);
    }

    public static long formattedStartToTimestamp(String start) {
        return ZonedDateTime.parse(start, StartFormatter).toInstant().toEpochMilli();
    }

    public ZonedDateTime getStartAsDateTime() {
        return Instant.ofEpochMilli(this.start).atZone(ZoneOffset.UTC);
    }

    public List<String> getCellsBCGIs() {
        return this.getCellsCGIs(this.cellsB);
    }

    public List<String> getCellsACGIs() {
        return this.getCellsCGIs(this.cellsA);
    }

    public String getCGIStartA() {
        List<String> cgis = this.getCellsACGIs();
        return cgis.isEmpty() ? null : (String) cgis.get(0);
    }

    public String getCGIEndA() {
        List<String> cgis = this.getCellsACGIs();
        return cgis.isEmpty() ? null : (String) cgis.get(cgis.size() - 1);
    }

    public String getCGIStartB() {
        List<String> cgis = this.getCellsBCGIs();
        return cgis.isEmpty() ? null : (String) cgis.get(0);
    }

    public String getCGIEndB() {
        List<String> cgis = this.getCellsBCGIs();
        return cgis.isEmpty() ? null : (String) cgis.get(cgis.size() - 1);
    }

    private List<String> getCellsCGIs(List<Cell> cells) {
        return cells == null ? Collections.emptyList() : (List) cells.stream().filter(Objects::nonNull).map(Cell::getCgi).collect(Collectors
                                                                                                                                          .toList());
    }

    public CDR invert() {
        CDR.Builder builder = new CDR.Builder(this.uuid,
                                              this.provider,
                                              this.callType,
                                              this.start,
                                              this.duration,
                                              this.getDirectionEnum() == CallDirection.IN ? CallDirection.OUT : CallDirection.IN,
                                              this.msisdnB,
                                              this.msisdnA);
        return builder.setMsisdnC(this.msisdnC)
                      .setImsiA("0")
                      .setImsiB(this.imsiA)
                      .setImeiA("0")
                      .setImeiB(this.imeiA)
                      .setStatus(this.status)
                      .setCellsA(this.cellsB)
                      .setCellsB(this.cellsA)
                      .setLocationsA(this.locationsB)
                      .setLocationsB(this.locationsA)
                      .setSmsContent(this.smsContent)
                      .setVoiceContent(this.voiceContent)
                      .setSource(this.source)
                      .setProperties(this.properties)
                      .build();
    }

    public int compareTo(CDR other) {
        return Long.compare(other.start, this.start);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        else if (o != null && this.getClass() == o.getClass()) {
            CDR cdr = (CDR) o;
            return this.start == cdr.start && this.duration == cdr.duration && this.direction == cdr.direction &&
                   this.callType == cdr.callType && Objects.equals(this.uuid, cdr.uuid) && Objects.equals(this.provider, cdr.provider) &&
                   Objects.equals(this.msisdnA, cdr.msisdnA) && Objects.equals(this.msisdnB, cdr.msisdnB) && Objects.equals(this.msisdnC,
                                                                                                                            cdr.msisdnC) &&
                   Objects.equals(this.imsiA, cdr.imsiA) && Objects.equals(this.imsiB, cdr.imsiB) && Objects.equals(this.imeiA,
                                                                                                                    cdr.imeiA) &&
                   Objects.equals(this.imeiB, cdr.imeiB) && Objects.equals(this.status, cdr.status) && Objects.equals(this.cellsA,
                                                                                                                      cdr.cellsA) &&
                   Objects.equals(this.cellsB, cdr.cellsB) && Objects.equals(this.locationsA, cdr.locationsA) &&
                   Objects.equals(this.locationsB, cdr.locationsB) && Objects.equals(this.countryA, cdr.countryA) &&
                   Objects.equals(this.countryB, cdr.countryB) && Objects.equals(this.smsContent, cdr.smsContent) &&
                   Objects.equals(this.voiceContent, cdr.voiceContent) && Objects.equals(this.source, cdr.source) &&
                   Objects.equals(this.properties, cdr.properties);
        }
        else {
            return false;
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.uuid,
                                         this.provider,
                                         this.callType,
                                         this.start,
                                         this.duration,
                                         this.direction,
                                         this.msisdnA,
                                         this.msisdnB,
                                         this.msisdnC,
                                         this.imsiA,
                                         this.imsiB,
                                         this.imeiA,
                                         this.imeiB,
                                         this.status,
                                         this.cellsA,
                                         this.cellsB,
                                         this.locationsA,
                                         this.locationsB,
                                         this.countryA,
                                         this.countryB,
                                         this.smsContent,
                                         this.voiceContent,
                                         this.source,
                                         this.properties});
    }

    public String toString() {
        return "CDR{uuid=" + this.uuid + ", provider='" + this.provider + '\'' + ", callType=" + this.callType + ", start=" + this.start +
               ", duration=" + this.duration + ", direction=" + this.direction + ", msisdnA='" + this.msisdnA + '\'' + ", msisdnB='" +
               this.msisdnB + '\'' + ", msisdnC='" + this.msisdnC + '\'' + ", imsiA='" + this.imsiA + '\'' + ", imsiB='" + this.imsiB +
               '\'' + ", imeiA='" + this.imeiA + '\'' + ", imeiB='" + this.imeiB + '\'' + ", status='" + this.status + '\'' + ", cellsA=" +
               this.cellsA + ", cellsB=" + this.cellsB + ", locationsA=" + this.locationsA + ", locationsB=" + this.locationsB +
               ", countryA='" + this.countryA + '\'' + ", countryB='" + this.countryB + '\'' + ", smsContent='" + this.smsContent + '\'' +
               ", voiceContent=" + this.voiceContent + ", source=" + this.source + ", properties=" + this.properties + '}';
    }

    static {
        StartFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withZone(ZoneOffset.UTC);
    }

    public static class Builder {
        private final Logger logger;
        private final UUID uuid;
        private String provider;
        private CallType callType;
        private long start;
        private int duration;
        private CallDirection direction;
        private String msisdnA;
        private String msisdnB;
        private String msisdnC;
        private String imsiA;
        private String imsiB;
        private String imeiA;
        private String imeiB;
        private String status;
        private List<Cell> cellsA;
        private List<Cell> cellsB;
        private List<Location> locationsA;
        private List<Location> locationsB;
        private String countryA;
        private String countryB;
        private String smsContent;
        private List<String> voiceContent;
        private Source source;
        private CDRProperties properties;

        public Builder() {
            this(UUID.randomUUID());
        }

        public Builder(UUID uuid) {
            this(uuid, (String) null, CallType.UNKNOWN, 0L, 0, CallDirection.OUT, (String) null, (String) null);
        }

        public Builder(UUID uuid,
                       String provider,
                       CallType callType,
                       long start,
                       int duration,
                       CallDirection direction,
                       String msisdnA,
                       String msisdnB) {
            this.logger = LoggerFactory.getLogger(CDR.Builder.class);
            this.uuid = uuid;
            this.provider = provider;
            this.callType = callType;
            this.start = start;
            this.duration = duration;
            this.direction = direction;
            this.msisdnA = msisdnA;
            this.msisdnB = msisdnB;
        }

        public Builder(UUID uuid,
                       String provider,
                       CallType callType,
                       long start,
                       int duration,
                       int direction,
                       String msisdnA,
                       String msisdnB) {
            this(uuid, provider, callType, start, duration, CallDirection.of(direction), msisdnA, msisdnB);
        }

        public Builder(UUID uuid,
                       String provider,
                       String callType,
                       long start,
                       int duration,
                       int direction,
                       String msisdnA,
                       String msisdnB) {
            this(uuid, provider, CallType.of(callType), start, duration, direction, msisdnA, msisdnB);
        }

        public Builder(String provider, String callType, long start, int duration, int direction, String msisdnA, String msisdnB) {
            this(UUID.randomUUID(), provider, callType, start, duration, direction, msisdnA, msisdnB);
        }

        public Builder(CDR cdr) {
            this(cdr.uuid, cdr.provider, cdr.callType, cdr.start, cdr.duration, cdr.direction, cdr.msisdnA, cdr.msisdnB);
            this.setMsisdnC(cdr.msisdnC)
                .setImsiA(cdr.imsiA)
                .setImsiB(cdr.imsiB)
                .setImeiA(cdr.imeiA)
                .setImeiB(cdr.imeiB)
                .setStatus(cdr.status)
                .setCellsA(cdr.cellsA)
                .setCellsB(cdr.cellsB)
                .setLocationsA(cdr.locationsA)
                .setLocationsB(cdr.locationsB)
                .setCountryA(cdr.countryA)
                .setCountryB(cdr.countryB)
                .setSmsContent(cdr.smsContent)
                .setVoiceContent(cdr.voiceContent)
                .setSource(cdr.source)
                .setProperties(cdr.properties);
        }

        public CDR.Builder setProvider(String provider) {
            this.provider = provider;
            return this;
        }

        public CDR.Builder setCallType(CallType callType) {
            this.callType = callType;
            return this;
        }

        public CDR.Builder setStart(long start) {
            this.start = start;
            return this;
        }

        public CDR.Builder setDuration(int duration) {
            this.duration = duration;
            return this;
        }

        public CDR.Builder setDirection(CallDirection direction) {
            this.direction = direction;
            return this;
        }

        public CDR.Builder setDirection(int direction) {
            this.direction = CallDirection.of(direction);
            return this;
        }

        public CDR.Builder setMsisdnA(String msisdnA) {
            this.msisdnA = msisdnA;
            return this;
        }

        public CDR.Builder setMsisdnB(String msisdnB) {
            this.msisdnB = msisdnB;
            return this;
        }

        public CDR.Builder setMsisdnC(String msisdnC) {
            this.msisdnC = msisdnC;
            return this;
        }

        public CDR.Builder setImsiA(String imsiA) {
            this.imsiA = imsiA;
            return this;
        }

        public CDR.Builder setImsiB(String imsiB) {
            this.imsiB = imsiB;
            return this;
        }

        public CDR.Builder setImeiA(String imeiA) {
            this.imeiA = imeiA;
            return this;
        }

        public CDR.Builder setImeiB(String imeiB) {
            this.imeiB = imeiB;
            return this;
        }

        public CDR.Builder setStatus(String status) {
            this.status = status;
            return this;
        }

        public CDR.Builder setCellsA(List<Cell> cellsA) {
            this.cellsA = cellsA;
            return this;
        }

        public CDR.Builder setCellsB(List<Cell> cellsB) {
            this.cellsB = cellsB;
            return this;
        }

        public CDR.Builder setLocationsA(List<Location> locationsA) {
            this.locationsA = locationsA;
            return this;
        }

        public CDR.Builder setLocationsB(List<Location> locationsB) {
            this.locationsB = locationsB;
            return this;
        }

        public CDR.Builder setCountryA(String countryA) {
            this.countryA = countryA;
            return this;
        }

        public CDR.Builder setCountryB(String countryB) {
            this.countryB = countryB;
            return this;
        }

        public CDR.Builder setSmsContent(String smsContent) {
            this.smsContent = smsContent;
            return this;
        }

        public CDR.Builder setVoiceContent(List<String> voiceContent) {
            this.voiceContent = voiceContent;
            return this;
        }

        public CDR.Builder setSource(Source source) {
            this.source = source;
            return this;
        }

        public CDR.Builder setProperties(CDRProperties properties) {
            this.properties = properties;
            return this;
        }

        public CDR.Builder addCellsA(List<Cell> cellsA) {
            this.cellsA.addAll(cellsA);
            return this;
        }

        public CDR.Builder addCellsB(List<Cell> cellsB) {
            this.cellsB.addAll(cellsB);
            return this;
        }

        public CDR.Builder setCellsAFromCGIs(List<String> cgiList) {
            this.cellsA = this.getCellsFromCGIs(cgiList);
            return this;
        }

        public CDR.Builder setCellsBFromCGIs(List<String> cgiList) {
            this.cellsB = this.getCellsFromCGIs(cgiList);
            return this;
        }

        private List<Cell> getCellsFromCGIs(List<String> cgiList) {
            return (List) cgiList.stream().map((cgi) -> {
                return CellModelUtil.of(cgi, Location.ZERO, (CellMetadata) null);
            }).collect(Collectors.toList());
        }

        public CDR.Builder addCellAFromCGI(String cgi) {
            this.cellsA = this.addCellFromCGI(this.cellsA, cgi);
            return this;
        }

        public CDR.Builder addCellBFromCGI(String cgi) {
            this.cellsB = this.addCellFromCGI(this.cellsB, cgi);
            return this;
        }

        private List<Cell> addCellFromCGI(List<Cell> cells, String cgi) {
            if (cells == null) {
                cells = new ArrayList<>();
            }

            ((List) cells).add(CellModelUtil.of(cgi, Location.ZERO, (CellMetadata) null));
            return (List) cells;
        }

        public CDR build() {
            if (this.uuid == null || this.provider == null || this.callType == null || this.msisdnA == null || this.msisdnB == null) {
                this.logger.error(
                        "Building Invalid CDR! uuid({}), provider({}), callType({}), MSISDN A({}) and MSISDN B({}) must be assigned!",
                        new Object[]{this.uuid, this.provider, this.callType, this.msisdnA, this.msisdnB});
            }

            CDR cdr = new CDR(this.uuid,
                              this.provider,
                              this.callType,
                              this.start,
                              this.duration,
                              this.direction,
                              this.msisdnA,
                              this.msisdnB);
            cdr.msisdnC = this.msisdnC;
            cdr.imsiA = this.imsiA;
            cdr.imsiB = this.imsiB;
            cdr.imeiA = this.imeiA;
            cdr.imeiB = this.imeiB;
            cdr.status = this.status;
            cdr.cellsA = this.cellsA;
            cdr.cellsB = this.cellsB;
            cdr.locationsA = this.locationsA;
            cdr.locationsB = this.locationsB;
            cdr.countryA = this.countryA;
            cdr.countryB = this.countryB;
            cdr.smsContent = this.smsContent;
            cdr.voiceContent = this.voiceContent;
            cdr.source = this.source;
            cdr.properties = this.properties;
            return cdr;
        }
    }
}
