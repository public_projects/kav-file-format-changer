package org.green.kav.common.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryCode {
    Map<String, CountryDetail> countries = new HashMap<>();

    public Map<String, CountryDetail> getCountries() {
        return countries;
    }

    public void setCountries(Map<String, CountryDetail> countries) {
        this.countries = countries;
    }
}
