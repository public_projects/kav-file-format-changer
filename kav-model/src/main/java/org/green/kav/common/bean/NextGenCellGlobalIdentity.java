package org.green.kav.common.bean;

import java.io.Serializable;


/**
 * Created by ys03 on 4/19/2018.
 */
public class NextGenCellGlobalIdentity extends CellGlobalIdentity implements Serializable {

    protected NextGenCellGlobalIdentity(Integer mcc, Integer mnc) {
        super(mcc, mnc);
    }

    @Override
    public Long getCellId() {
        // TODO support 5g ncgi
        return null;
    }

    @Override
    public String toMcngEncodedString() {
        // TODO support 5g ncgi
        return null;
    }

    @Override
    public String toMcngEncodedStringLeftPadded() {
        // TODO support 5g ncgi
        return null;
    }
}
