package org.green.kav.common.model.request;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


public final class McngBuildRequest implements Serializable {
    private List<String> targets;
    private String outputDirectory;
    private File fileDirectory;

    /**
     * fasterxml.jackson needs it to deserialize REST request.
     */
    public McngBuildRequest(){
    }

    public McngBuildRequest(List<String> targets, String outputDirectory) {
        this.targets = targets;
        this.outputDirectory = outputDirectory;
        String folderName = LocalDateTime.now()
                                         .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        this.fileDirectory = new File(this.outputDirectory + File.separator + folderName);
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
        String folderName = LocalDateTime.now()
                                         .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        this.fileDirectory = new File(this.outputDirectory + File.separator + folderName);
    }

    public List<String> getTargets() {
        return targets;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public File getFileDirectory() {
        return fileDirectory;
    }
}
