package org.green.kav.common.bean.flat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.bean.flat.model.RootFlatFileModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;


public abstract class FlatFileModelService {
    protected  Logger log = LoggerFactory.getLogger(this.getClass());
    public static final DateTimeFormatter flatFileDateFormatter1 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static final DateTimeFormatter flatFileDateFormatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final String delimeterForHeader = "\\|";
    public static final String delimeterForData = "|";
    protected List<List<String>> dataLs = new ArrayList<>();
    protected List<RootFlatFileModel> rootFlatFileModels;
    protected List<String> headers = new ArrayList<>();
    private String dateHeaderName;
    private String[] dataHeaders;
    protected ObjectMapper mapper = new ObjectMapper();
    private ArrayNode flatFileObjectNode = mapper.createObjectNode().arrayNode();

    public abstract String getFileName();

    public List<RootFlatFileModel> getRootFlatFileModels() {
        return rootFlatFileModels;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public ArrayNode getFlatFileObjectNode() {
        return flatFileObjectNode;
    }

    public String getHeaderSingleLine() {
        StringBuilder sb = new StringBuilder();
        this.headers.stream()
                    .forEach(s -> sb.append("\"")
                                    .append(s)
                                    .append("\"")
                                    .append("|"));
        String headerStr = sb.toString();
        return headerStr.substring(0, headerStr.length() - 1);
    }

    public abstract String getDateColumnValue(RootFlatFileModel rootFlatFileModel);

    public void addDataHeader(String header) {
        this.dataHeaders = header.split(this.delimeterForHeader);
        log.info("Header {}", dataHeaders);
    }

    public void addData(String data) {
        String[] dataArray = data.split(delimeterForHeader);
        ObjectNode node = mapper.createObjectNode();

        for (int i = 0; i < headers.size(); i++) {
            try {
                node.put(headers.get(i), dataArray[i]);
            } catch(ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
                //igonore
                arrayIndexOutOfBoundsException.printStackTrace();
                node.put(headers.get(i), "");
            }
        }

        flatFileObjectNode.add(node);
    }

    public String getJsonData() throws Exception {
        return mapper.writeValueAsString(this.getFlatFileObjectNode());
    }

    public abstract List<RootFlatFileModel> sortByDate() throws Exception;

    protected long getEpochSeconds(String strDateTime) {
        LocalDateTime localDateTime = null;
        try {
            localDateTime = LocalDateTime.parse(strDateTime, flatFileDateFormatter1);
        } catch (DateTimeParseException dateTimeParseException) {
            localDateTime = LocalDateTime.parse(strDateTime, flatFileDateFormatter2);
        }

        return localDateTime.toEpochSecond(ZoneOffset.UTC);
    }

    protected RootFlatFileModel[] getDataContainer() throws Exception{
        return mapper.readValue(this.getJsonData(), RootFlatFileModel[].class);
    }

    protected LocalDateTime getConvertedLocalDateTime(String strDateTime) {
        LocalDateTime localDateTime;
        try {
            localDateTime = LocalDateTime.parse(strDateTime, flatFileDateFormatter1);
        } catch (DateTimeParseException dateTimeParseException) {
            localDateTime = LocalDateTime.parse(strDateTime, flatFileDateFormatter2);
        }
        return localDateTime;
    }

    public abstract long getDateDiffToNewDate(LocalDateTime expectedDate) throws Exception;
    public abstract void addDaysToCurrentDates(long days);
    public abstract String getDataCsv(RootFlatFileModel rootFlatFileModel);
}
