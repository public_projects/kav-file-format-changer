package org.green.kav.common.bean.mfi;

import lombok.Getter;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public abstract class MfiModelService {
    public static final DateTimeFormatter flatFileDateFormatter1 = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static final DateTimeFormatter flatFileDateFormatter2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final String delimeterForHeader = ";";
    public static final String delimeterForData = ";";
    protected List<String> headers = new ArrayList<>();

    public List<String> getHeaders() {
        return headers;
    }
}
