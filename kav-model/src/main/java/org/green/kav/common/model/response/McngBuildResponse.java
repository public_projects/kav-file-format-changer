package org.green.kav.common.model.response;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.model.TargetDetailsModel;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
public final class McngBuildResponse implements Serializable {
    private List<TargetDetailsModel> result;
    private String outputDirectory;

    public McngBuildResponse(List<TargetDetailsModel> result, String outputDirectory) {
        this.result = result;
        this.outputDirectory = outputDirectory;
    }

    public List<TargetDetailsModel> getResult() {
        return result;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }
}
