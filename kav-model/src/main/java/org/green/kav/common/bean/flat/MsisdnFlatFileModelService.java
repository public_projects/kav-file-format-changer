package org.green.kav.common.bean.flat;

import org.green.kav.common.bean.flat.model.RootFlatFileModel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


public class MsisdnFlatFileModelService extends FlatFileModelService {
    public MsisdnFlatFileModelService(){
         this.getHeaders().add("MCTARGETID");
         this.getHeaders().add("MSISDNID");
         this.getHeaders().add("CREATED");
         this.getHeaders().add("ISASSIGNEDBYANALYSTS");
         this.getHeaders().add("SYSTEMID");
         this.getHeaders().add("MCAGENCYID");
    }

    @Override
    public String getFileName() {
        return MsisdnFlatFileModelService.FILE_NAME;
    }

    public static final String FILE_NAME = "P_TARGET_MSISDN_FLAT.csv";
    public static final String DATE_TIME_COLUMN_HEADER_NAME = "CREATED";

    @Override
    public String getDateColumnValue(RootFlatFileModel rootFlatFileModel) {
        return null;
    }

    @Override
    public List<RootFlatFileModel> sortByDate() throws Exception {
        return null;
    }

    @Override
    public long getDateDiffToNewDate(LocalDateTime expectedDate) throws Exception {
        return 0;
    }

    @Override
    public void addDaysToCurrentDates(long days) {

    }

    @Override
    public String getDataCsv(RootFlatFileModel rootFlatFileModel) {
        return null;
    }
}
