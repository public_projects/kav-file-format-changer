package org.green.kav.common.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import static org.green.kav.common.bean.Constants.CELL.CGI;
import static org.green.kav.common.bean.Constants.CELL.LATITUDE;
import static org.green.kav.common.bean.Constants.CELL.LONGITUDE;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "cellGlobalId", "mcc", "mnc", "eCellId",
        "location", "latitude", "longitude",
        "rightLatitude", "rightLongitude", "leftLatitude", "leftLongitude",
        "azimuth", "height", "coverageRadius", "mechanicalDowntilt", "btsTransmitterPower", "towerName", "cellName"
})
public class LteCell extends Cell<EvolvedCellGlobalIdentity> {

    public LteCell() {
    }

    public LteCell(EvolvedCellGlobalIdentity cgi, Location location) {
        super(cgi, location);
    }

    @JsonCreator
    public LteCell(@JsonProperty(CGI)            String cgi,
                   @JsonProperty(LATITUDE)       Double latitude,
                   @JsonProperty(LONGITUDE)      Double longitude) {
        super(EvolvedCellGlobalIdentity.createFromMcngEncodedString(cgi), Location.of(latitude, longitude));
    }

    public Long geteCellId() {
        return getCellGlobalId().geteCellId();
    }

    @Override
    public Long getCellId() {
        if (getCellGlobalId() == null || !(getCellGlobalId() instanceof EvolvedCellGlobalIdentity)) return Long.valueOf(0);
        return getCellGlobalId().getCellId();
    }

}
