package org.green.kav.common.bean;

import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;


public class EvolvedCellGlobalIdentity extends CellGlobalIdentity implements Serializable {
    public static final EvolvedCellGlobalIdentity UNKNOWN = new EvolvedCellGlobalIdentity(0, 0, 0L);
    private  Long eCellId;

    public static EvolvedCellGlobalIdentity createFromMcngEncodedString(String cgi) {
        String[] parts = cgi.split("-");
        if (parts.length != 3) {
            throw new IllegalArgumentException("Expected 3 parts from the CGI string");
        } else {
            try {
                return new EvolvedCellGlobalIdentity(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Long.parseLong(parts[2]));
            } catch (NumberFormatException var3) {
                throw new IllegalArgumentException(var3);
            }
        }
    }

    public static List<CellGlobalIdentity> createFromMcngEncodedStrings(List<String> cgiStrings) {
        if (cgiStrings.isEmpty()) {
            return Collections.emptyList();
        } else {
            List<CellGlobalIdentity> cgis = new ArrayList(cgiStrings.size());
            Iterator var2 = cgiStrings.iterator();

            while(var2.hasNext()) {
                String cgiString = (String)var2.next();

                try {
                    cgis.add(createFromMcngEncodedString(cgiString));
                } catch (IllegalArgumentException var5) {
                }
            }

            return cgis;
        }
    }

    public static CellGlobalIdentity of(Integer mcc, Integer mnc, Long eCgi) {
        Preconditions.checkArgument(mcc > 0, "Mobile Country Code must be strictly positive");
        Preconditions.checkArgument(mnc >= 0, "Mobile Network Code must be positive");
        Preconditions.checkArgument(eCgi >= 0L, "Cell Identity must be positive");
        return new EvolvedCellGlobalIdentity(mcc, mnc, eCgi);
    }

    public EvolvedCellGlobalIdentity(Integer mcc, Integer mnc, Long eCellId) {
        super(mcc, mnc);
        this.eCellId = eCellId;
    }

    public Long geteCellId() {
        return this.eCellId;
    }

    public Integer getEnb() {
        if (this.eCellId == null) {
            return null;
        } else {
            String leftPaddedeCellId = String.format("%09d", this.eCellId);
            return Integer.parseInt(leftPaddedeCellId.substring(0, 6));
        }
    }

    public Long getCellId() {
        if (this.eCellId == null) {
            return null;
        } else {
            String leftPaddedeCellId = String.format("%09d", this.eCellId);
            return Long.valueOf(leftPaddedeCellId.substring(6));
        }
    }

    public String toMcngEncodedString() {
        return this.getMcc() + "-" + this.getMnc() + "-" + this.eCellId;
    }

    public String toMcngEncodedStringLeftPadded() {
        String mncPadded = String.format("%03d", this.getMnc());
        String eCellIdPadded = String.format("%09d", this.eCellId);
        return this.getMcc() + "-" + mncPadded + "-" + eCellIdPadded;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            EvolvedCellGlobalIdentity that;
            label41: {
                that = (EvolvedCellGlobalIdentity)o;
                if (this.getMcc() != null) {
                    if (this.getMcc().equals(that.getMcc())) {
                        break label41;
                    }
                } else if (that.getMcc() == null) {
                    break label41;
                }

                return false;
            }

            if (this.getMnc() != null) {
                if (this.getMnc().equals(that.getMnc())) {
                    return this.geteCellId() != null ? this.geteCellId().equals(that.geteCellId()) : that.geteCellId() == null;
                }
            } else if (that.getMnc() == null) {
                return this.geteCellId() != null ? this.geteCellId().equals(that.geteCellId()) : that.geteCellId() == null;
            }

            return false;
        } else {
            return false;
        }
    }

    public int hashCode() {
        int result = this.getMcc() != null ? this.getMcc().hashCode() : 0;
        result = 31 * result + (this.getMnc() != null ? this.getMnc().hashCode() : 0);
        result = 31 * result + (this.geteCellId() != null ? this.geteCellId().hashCode() : 0);
        return result;
    }

    public String toString() {
        return "EvolvedCellGlobalIdentity{mcc=" + this.getMcc() + ", mnc=" + this.getMnc() + ", eCellId=" + this.eCellId + '}';
    }
}
