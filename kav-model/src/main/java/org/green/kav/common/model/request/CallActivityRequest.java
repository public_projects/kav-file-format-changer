package org.green.kav.common.model.request;

import org.green.kav.common.model.CallActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CallActivityRequest implements Serializable {
    private List<CallActivity> callActivities = new ArrayList<>();
    private long maxRecordInfile;
    private String fileOutputDir;

    public List<CallActivity> getCallActivities() {
        return callActivities;
    }

    public void setCallActivities(List<CallActivity> callActivities) {
        this.callActivities = callActivities;
    }

    public long getMaxRecordInfile() {
        return maxRecordInfile;
    }

    public void setMaxRecordInfile(long maxRecordInfile) {
        this.maxRecordInfile = maxRecordInfile;
    }

    public String getFileOutputDir() {
        return fileOutputDir;
    }

    public void setFileOutputDir(String fileOutputDir) {
        this.fileOutputDir = fileOutputDir;
    }
}
