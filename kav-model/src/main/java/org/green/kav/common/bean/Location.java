package org.green.kav.common.bean;

import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.Objects;

public final class Location implements Locatable, Serializable {

    public static final Location ZERO = new Location(0.0, 0.0);

    /** Earth diameter specified in meters. */
    private static final double EARTH_DIAMETER = 6371.01 * 1000;

    /**
     * Creates a new location object.
     *
     * @param latitude The latitude of the location. May range from -90.0 to 90.0.
     * @param longitude The longitude of the location. May range from -180.0 to 180.0.
     * @throws IllegalArgumentException thrown when invalid longitude and latitude are given
     */
    public static Location of(Double latitude, Double longitude) {
        return new Location(latitude, longitude);
    }

    private final Double latitude;
    private final Double longitude;

    /**
     * Creates a new location object.
     *
     * @param latitude The latitude of the location. May range from -90.0 to 90.0.
     * @param longitude The longitude of the location. May range from -180.0 to 180.0.
     * @throws IllegalArgumentException thrown when invalid longitude and latitude are given
     */
    private Location(Double latitude, Double longitude) {
        Preconditions.checkArgument(latitude >= -90.0 && latitude <= 90.0, "Latitude may range from -90.0 to 90.0");
        Preconditions.checkArgument(longitude >= -180.0 && longitude <= 180.0, "Longitude may range from -180.0 to 180.0");
        this.latitude  = latitude;
        this.longitude = longitude;
    }

    /**
     * Returns the latitude
     *
     * Latitude is a geographic coordinate that specifies the north-south
     * position of a point on the Earth's surface
     *
     * @return The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Returns the longitude.
     *
     * Longitude is a geographic coordinate that specifies the east-west
     * position of a point on the Earth's surface
     *
     * @return The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    @Override
    public Location getLocation() {
        return this;
    }

    /**
     * Returns the distance in meters between this and the specified location.
     *
     * @param other The other location
     * @return The distance in meters between this and the specified location
     */
    public double distanceFrom(Location other) {
        double diffLongitudes = Math.toRadians(Math.abs(other.getLongitude() - this.getLongitude()));
        double slat = Math.toRadians(this.getLatitude());
        double flat = Math.toRadians(other.getLatitude());

        // spherical law of cosines
        double c = Math.acos((Math.sin(slat) * Math.sin(flat)) +
                             (Math.cos(slat) * Math.cos(flat) * Math.cos(diffLongitudes)));

        return EARTH_DIAMETER * c;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(longitude, location.longitude) &&
               Objects.equals(latitude, location.latitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(longitude, latitude);
    }

    @Override
    public String toString() {
        return "Location{" +
               "longitude=" + longitude +
               ", latitude=" + latitude +
               '}';
    }
}