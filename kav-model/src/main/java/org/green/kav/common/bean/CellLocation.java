/*
 * Copyright (c) 2015, trovicor GmbH. All rights reserved. COMPANY CONFIDENTIAL.
 */
package org.green.kav.common.bean;

import java.io.Serializable;
import java.util.Objects;


/**
 * The cell location
 */
public final class CellLocation implements Locatable, Serializable {

    private Double latitude;

    private Double longitude;

    /** Must have for Cassandra */
    @SuppressWarnings("unused")
    public CellLocation() {}

    /**
     * Creates an object representing the location of a cell
     *
     * @param latitude The latitude
     * @param longitude The longitude
     */
    public CellLocation(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Creates an object representing the location of a cell using the
     * coordinates from the supplied {@link Locatable} instance
     *
     * @param locatable Object from which to extract the coordinates
     */
    public CellLocation(Locatable locatable) {
        this(locatable.getLocation().getLatitude(), locatable.getLocation().getLongitude());
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    @Override
    public Location getLocation() {
        return Location.of(latitude, longitude);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CellLocation location = (CellLocation) o;
        return Objects.equals(latitude, location.latitude) &&
                Objects.equals(longitude, location.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public String toString() {
        return "CellLocation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
