package org.green.kav.common.bean.mfi;

import java.util.ArrayList;
import java.util.List;

public class CanonicalCdrService extends MfiModelService {
    private static List<String> headers = new ArrayList<>();
    private static List<String> givenFileHeaders = new ArrayList<>();
    private String givenFile;

    static {
        headers.add("PROVIDER");
        headers.add("EVENTTYPE");
        headers.add("START");
        headers.add("DURATION");
        headers.add("STATUS");
        headers.add("CGISTARTA");
        headers.add("CGISTARTB");
        headers.add("LATSTARTA");
        headers.add("LATSTARTB");
        headers.add("LONSTARTA");
        headers.add("LONSTARTB");
        headers.add("CGIENDA");
        headers.add("CGIENDB");
        headers.add("LATENDA");
        headers.add("LATENDB");
        headers.add("LONENDA");
        headers.add("LONENDB");
        headers.add("MSISDNA");
        headers.add("MSISDNB");
        headers.add("MSISDNC");
        headers.add("IMEIA");
        headers.add("IMEIB");
        headers.add("IMSIA");
        headers.add("IMSIB");
    }

    public CanonicalCdrService(String fileName){
    }

    public boolean isMatch(){
        return true;
    }
}
