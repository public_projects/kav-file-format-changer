package org.green.kav.common.model;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class TargetDetailsModel {
    private final static Logger log = LoggerFactory.getLogger(TargetDetailsModel.class);

    private String name;
    private String mcngId;
    private List<String> msisdn;
    private List<String> imsi;
    private List<String> imei;
    public TargetDetailsModel(String mcngId, String targetName, Map<String, Object> rawData) {
        log.info("data {}", rawData);
        Map<String, Object> extractedData = (Map<String, Object>) rawData.get("message");
        log.info("extracted data: {}", extractedData);
//        this.msisdn = getIdentityByKey("MSISDN", extractedData);
//        this.imsi = getIdentityByKey("IMSI", extractedData);
//        this.imei = getIdentityByKey("IMEI", extractedData);

        this.msisdn = (List<String>) extractedData.get("MSISDN");
        this.imsi = (List<String>) extractedData.get("IMSI");
        this.imei = (List<String>) extractedData.get("IMEI");
        this.mcngId = mcngId;
        this.name = targetName;
    }

    public TargetDetailsModel(String mcngId, String targetName, List<String> msisdns, List<String> imsis, List<String> imeis) {
        this.mcngId = mcngId;
        this.name = targetName;
        this.msisdn = msisdns;
        this.imsi = imsis;
        this.imei = imeis;
    }
    public TargetDetailsModel() {}

    public String getIdentityByKey(String key, Map<String, Object> extractedData) {
        List<String> data = (List<String>) extractedData.get(key);
        return data.get(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TargetDetailsModel)) return false;
        TargetDetailsModel that = (TargetDetailsModel) o;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getMcngId(), that.getMcngId()) && Objects.equals(getMsisdn(),
                                                                                                                            that.getMsisdn()) &&
               Objects.equals(getImsi(), that.getImsi()) && Objects.equals(getImei(), that.getImei());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMcngId() {
        return mcngId;
    }

    public void setMcngId(String mcngId) {
        this.mcngId = mcngId;
    }

    public List<String> getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(List<String> msisdn) {
        this.msisdn = msisdn;
    }

    public List<String> getImsi() {
        return imsi;
    }

    public void setImsi(List<String> imsi) {
        this.imsi = imsi;
    }

    public List<String> getImei() {
        return imei;
    }

    public void setImei(List<String> imei) {
        this.imei = imei;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getMcngId(), getMsisdn(), getImsi(), getImei());
    }
}
