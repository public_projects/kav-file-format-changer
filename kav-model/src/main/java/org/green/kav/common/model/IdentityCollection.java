package org.green.kav.common.model;

import java.util.*;


public class IdentityCollection {
    private List imsi = new ArrayList();
    private List imei = new ArrayList();
    private List msisdn = new ArrayList();
    private Map<String, Set<String>> others = new HashMap<>();
    private String targetName;
    private String mcngId;
    private Map<String, Object> collection = new HashMap<>();

    public IdentityCollection(List imsi, List imei, List msisdn, Map<String, Set<String>> others, String targetName, String mcngId, Map<String, Object> collection) {
        this.imsi = imsi;
        this.imei = imei;
        this.msisdn = msisdn;
        this.others = others;
        this.targetName = targetName;
        this.mcngId = mcngId;
        this.collection = collection;
    }

    private IdentityCollection() {
    }

    public static IdentityCollection of(String targetName, String msisdn, String imsi, String imei, String mcngId) {
        IdentityCollection identityCollection = new IdentityCollection();
        identityCollection.msisdn.add(msisdn);
        identityCollection.imsi.add(imsi);
        identityCollection.imei.add(imei);
        identityCollection.targetName = targetName;
        identityCollection.mcngId = mcngId;
        return identityCollection;
    }

    public static IdentityCollection of(String targetName, List<String> msisdn, List<String> imsi, List<String> imei, String mcngId) {
        IdentityCollection identityCollection = new IdentityCollection();
        identityCollection.msisdn.addAll(msisdn);
        identityCollection.imsi.addAll(imsi);
        identityCollection.imei.addAll(imei);
        identityCollection.targetName = targetName;
        identityCollection.mcngId = mcngId;
        return identityCollection;
    }

    public Map<String, Object> getTargetContainer() {
        Map<String, Object> content = new HashMap<>();
        Map<String, Object> identitiesMap = new HashMap<>();
        identitiesMap.put("IMSI", imsi);
        identitiesMap.put("IMEI", imei);
        identitiesMap.put("MSISDN", msisdn);
        identitiesMap.putAll(others);
        identitiesMap.put("mcng-etl-id", mcngId);
        content.put(targetName, identitiesMap);
        return content;
    }

    public List getImsi() {
        return imsi;
    }

    public List getImei() {
        return imei;
    }

    public List getMsisdn() {
        return msisdn;
    }

    public Map<String, Set<String>> getOthers() {
        return others;
    }

    public String getTargetName() {
        return targetName;
    }

    public String getMcngId() {
        return mcngId;
    }

    public Map<String, Object> getCollection() {
        return collection;
    }

    @Override
    public String toString() {
        return "IdentityCollection{" + "imsi=" + imsi + ", imei=" + imei + ", msisdn=" + msisdn + ", targetName='" + targetName + '\'' +
               ", mcngId='" + mcngId + '\'' + ", collection=" + collection + '}';
    }
}
