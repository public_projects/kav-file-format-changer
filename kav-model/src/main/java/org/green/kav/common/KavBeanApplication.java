package org.green.kav.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KavBeanApplication {
    public static void main(String[] args) {
        SpringApplication.run(KavBeanApplication.class, args);
    }
}
