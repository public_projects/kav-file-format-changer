package org.green.kav.common.bean;

import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;
import static org.green.kav.common.bean.Constants.CGI_SEPARATOR;


public class ConventionalCellGlobalIdentity extends CellGlobalIdentity implements Serializable {

    /**
     * Represents an unknown Cell Global Identity.
     */
    public static final ConventionalCellGlobalIdentity UNKNOWN = new ConventionalCellGlobalIdentity(0, 0, 0, 0L);

    /**
     * Creates a {@code CellGlobalIdentity} instance from the encoded string
     * (MCC-MNC-LAC-CellId) provided by MCng exports
     *
     * @param cgi cell global identity string (MCC-MNC-LAC-CellId)
     * @return {@code CellGlobalIdentity} instance
     * @throws IllegalArgumentException thrown if the provided CGI string is invalid
     */
    public static ConventionalCellGlobalIdentity createFromMcngEncodedString(String cgi) {
        String[] parts = cgi.split(CGI_SEPARATOR);
        if (parts.length != 4) throw new IllegalArgumentException("Expected 4 parts from the CGI string");

        try {
            return new ConventionalCellGlobalIdentity(
                    Integer.parseInt(parts[0]),
                    Integer.parseInt(parts[1]),
                    Integer.parseInt(parts[2]),
                    Long.parseLong(parts[3])
            );
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Creates a list of {@code CellGlobalIdentity} instances from the encoded strings
     * (MCC-MNC-LAC-CellId) provided by MCng exports
     *
     * @param cgiStrings cell global identity strings (MCC-MNC-LAC-CellId)
     * @return list of {@code CellGlobalIdentity} instances
     */
    public static List<CellGlobalIdentity> createFromMcngEncodedStrings(List<String> cgiStrings) {
        if (cgiStrings.isEmpty()) return Collections.emptyList();
        List<CellGlobalIdentity> cgis = new ArrayList<>(cgiStrings.size());
        for (String cgiString : cgiStrings) {
            try {
                cgis.add(ConventionalCellGlobalIdentity.createFromMcngEncodedString(cgiString));
            } catch (IllegalArgumentException ignore) {}
        }

        return cgis;
    }

    /**
     * Creates a non LTE Cell Global Identity.
     *
     * @param mcc mobile country code
     * @param mnc mobile network code
     * @param lac location area code
     * @param cellId cell identity
     * @throws IllegalArgumentException thrown if any of the arguments is not strictly positive
     */
    public static ConventionalCellGlobalIdentity of(Integer mcc, Integer mnc, Integer lac, Long cellId) {
        checkArgument(mcc > 0, "Mobile Country Code must be strictly positive");
        checkArgument(mnc >= 0, "Mobile Network Code must be positive");
        checkArgument(lac >= 0, "Location Area Code must be positive");
        checkArgument(cellId >= 0, "Cell Identity must be positive");

        return new ConventionalCellGlobalIdentity(mcc, mnc, lac, cellId);
    }

    /** Location Area Code */
    private final Integer lac;

    /** Cell Identity */
    private final Long cellId;

    /**
     * Creates a Cell Global Identity.
     *
     * @param mcc mobile country code
     * @param mnc mobile network code
     * @param lac location area code
     * @param cellId cell identity
     *
     */
    public ConventionalCellGlobalIdentity(Integer mcc, Integer mnc, Integer lac, Long cellId) {
        super(mcc, mnc);
        this.lac = lac;
        this.cellId = cellId;
    }

    /**
     * Returns the cell's location area code.
     *
     * @return the cell's location area code
     */
    public Integer getLac() {
        return lac;
    }

    /**
     * Returns the cell's identity.
     *
     * @return the cell's identity
     */
    @Override
    public Long getCellId() {
        return cellId;
    }

    /**
     * Returns the MCng Cell Global Identity encoded string.
     *
     * @return the MCng Cell Global Identity encoded string
     */
    @Override
    public String toMcngEncodedString() {
        return getMcc() + CGI_SEPARATOR + getMnc() + CGI_SEPARATOR + this.lac + CGI_SEPARATOR + this.cellId;
    }

    /**
     * Returns the MCng Cell Global Identity encoded string with left padded 0.
     *
     * @return the MCng Cell Global Identity encoded string
     */
    @Override
    public String toMcngEncodedStringLeftPadded() {
        String mncPadded = String.format("%03d", getMnc());
        String lacPadded = String.format("%05d", lac);
        String cellIdPadded = String.format("%05d", cellId);

        return getMcc() + CGI_SEPARATOR + mncPadded + CGI_SEPARATOR + lacPadded + CGI_SEPARATOR + cellIdPadded;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConventionalCellGlobalIdentity that = (ConventionalCellGlobalIdentity) o;
        return Objects.equals(getMcc(), that.getMcc()) &&
               Objects.equals(getMnc(), that.getMnc()) &&
               Objects.equals(lac, that.lac) &&
               Objects.equals(cellId, that.cellId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMcc(), getMnc(), lac, cellId);
    }

    @Override
    public String toString() {
        return "CellGlobalIdentity{" +
               "mcc=" + getMcc() +
               ", mnc=" + getMnc() +
               ", lac=" + lac +
               ", cellId=" + cellId +
               '}';
    }
}
