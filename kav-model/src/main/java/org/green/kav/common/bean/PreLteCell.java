package org.green.kav.common.bean;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import static org.green.kav.common.bean.Constants.CELL.CGI;
import static org.green.kav.common.bean.Constants.CELL.LATITUDE;
import static org.green.kav.common.bean.Constants.CELL.LONGITUDE;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "cellGlobalId", "mcc", "mnc", "lac", "cellId",
        "location", "latitude", "longitude",
        "rightLatitude", "rightLongitude", "leftLatitude", "leftLongitude",
        "azimuth", "height", "coverageRadius", "mechanicalDowntilt", "btsTransmitterPower", "towerName", "cellName"
})
public class PreLteCell extends Cell<ConventionalCellGlobalIdentity> {

    public PreLteCell(ConventionalCellGlobalIdentity cgi, Location location) {
        super(cgi, location);
    }

    @JsonCreator
    public PreLteCell(@JsonProperty(CGI)            String cgi,
                      @JsonProperty(LATITUDE)       Double latitude,
                      @JsonProperty(LONGITUDE)      Double longitude) {
        super(ConventionalCellGlobalIdentity.createFromMcngEncodedString(cgi), Location.of(latitude, longitude));
    }

    public Integer getLac() {
        return getCellGlobalId().getLac();
    }

    public Long getCellId() {
        return getCellGlobalId().getCellId();
    }

}
