package org.green.kav.common.bean;

import java.util.Map;

public class Source {
    String system;
    Map<String, String> properties;

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }
}
