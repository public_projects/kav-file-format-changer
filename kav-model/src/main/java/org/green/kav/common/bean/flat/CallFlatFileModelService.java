package org.green.kav.common.bean.flat;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.bean.flat.model.RootFlatFileModel;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

@Slf4j
public class CallFlatFileModelService extends FlatFileModelService {
    public static final String DATE_TIME_COLUMN_HEADER_NAME = "SES_PHONE_DIR1_STARTDATE";
    private static CallFlatFileModelService callFlatFileModelService;
    public static final String FILE_NAME = "P_CALL_FLAT.csv";

    public CallFlatFileModelService() {
        this.getHeaders().add("SESSIONLOG_MCINTERCEPTID");
        this.getHeaders().add("SES_PHONE_DIR1_IDENTITY_VALUE");
        this.getHeaders().add("SES_IMSI_IDENTITY_VALUE");
        this.getHeaders().add("SES_IMEI_IDENTITY_VALUE");
        this.getHeaders().add("SES_PHONE_DIR2_IDENTITY_VALUE");
        this.getHeaders().add("SES_PHONE_DIR1_STARTDATE");
        this.getHeaders().add("SES_PHONE_DIR1_DURATION");
        this.getHeaders().add("SES_PHONE_DIR1_SESSIONTYPE");
        this.getHeaders().add("SES_PHONE_DIR1_INID_DIRECTION");
        this.getHeaders().add("SES_PHONE_DIR1_TRIG_TARGETID");
        this.getHeaders().add("SES_PHONE_DIR1_INID_MARKEDTYPE");
        this.getHeaders().add("SES_IMSI_INID_MARKEDTYPE");
        this.getHeaders().add("SES_IMEI_INID_MARKEDTYPE");
        this.getHeaders().add("SES_PHONE_DIR2_INID_MARKEDTYPE");
        this.getHeaders().add("SES_PHONE_DIR1_TRIG_TRIGGERID");
        this.getHeaders().add("SES_PHONE_DIR1_INID_MCAGENCYID");
        this.getHeaders().add("SES_PHONE_DIR1_CGI");
        this.getHeaders().add("SES_PHONE_DIR1_CELL_NETWORK");
        this.getHeaders().add("SYSTEMID");
        this.getHeaders().add("CONTENT_LOCATORS");
    }

    @Override
    public String getDataCsv(RootFlatFileModel model) {
        StringBuilder sb = new StringBuilder();
        sb.append(System.lineSeparator());
        sb.append(model.getSESSIONLOG_MCINTERCEPTID());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_IDENTITY_VALUE());
        sb.append(delimeterForData);
        sb.append(model.getSES_IMSI_IDENTITY_VALUE());
        sb.append(delimeterForData);
        sb.append(model.getSES_IMEI_IDENTITY_VALUE());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR2_IDENTITY_VALUE());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_STARTDATE());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_DURATION());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_SESSIONTYPE());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_INID_DIRECTION());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_TRIG_TARGETID());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_INID_MARKEDTYPE());
        sb.append(delimeterForData);
        sb.append(model.getSES_IMSI_INID_MARKEDTYPE());
        sb.append(delimeterForData);
        sb.append(model.getSES_IMEI_INID_MARKEDTYPE());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR2_INID_MARKEDTYPE());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_TRIG_TRIGGERID());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_INID_MCAGENCYID());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_CGI());
        sb.append(delimeterForData);
        sb.append(model.getSES_PHONE_DIR1_CELL_NETWORK());
        sb.append(delimeterForData);
        sb.append(model.getSYSTEMID());
        sb.append(delimeterForData);
        sb.append(model.getCONTENT_LOCATORS());
        return sb.toString();
    }

    @Override
    public String getFileName() {
        return CallFlatFileModelService.FILE_NAME;
    }

    @Override
    public String getDateColumnValue(RootFlatFileModel rootFlatFileModel) {
        return rootFlatFileModel.getSES_PHONE_DIR1_STARTDATE();
    }

    public List<RootFlatFileModel> sortByDate() throws Exception {
        RootFlatFileModel[] rootRecords = getDataContainer();
        List<RootFlatFileModel> rootFlatFileModelList = Arrays.asList(rootRecords);

        rootFlatFileModelList.sort((o1, o2) -> {
            long date1 = getEpochSeconds(o1.getSES_PHONE_DIR1_STARTDATE());
            long date2 = getEpochSeconds(o2.getSES_PHONE_DIR1_STARTDATE());
            return date1 < date2 ? -1 : date1 > date2 ? 1 : 0;
        });

//        rootFlatFileModelList.stream()
//                             .map(rootFlatFileModel -> rootFlatFileModel.getSES_PHONE_DIR1_STARTDATE())
//                             .forEach(s -> {
//                                 log.info("Sorted: {}", s);
//                             });

        this.rootFlatFileModels = rootFlatFileModelList;
        return rootFlatFileModelList;
    }

    public long getDateDiffToNewDate(LocalDateTime expectedDate) {
        RootFlatFileModel latestRecord = null;
        if (this.rootFlatFileModels.size() == 1) {
            latestRecord =  this.rootFlatFileModels.get(this.rootFlatFileModels.size() - 1);
        } else {
            latestRecord = this.rootFlatFileModels.get(this.rootFlatFileModels.size() - 1);
        }

//        log.info("Current record's date: {}", latestRecord.getSES_PHONE_DIR1_STARTDATE());
        LocalDateTime localDateTime = getConvertedLocalDateTime(latestRecord.getSES_PHONE_DIR1_STARTDATE());
        long dateDiff = ChronoUnit.DAYS.between(localDateTime.toLocalDate(), expectedDate.toLocalDate());
//        log.info("expected date: {}, the most latest record date: {}, Date differents are: {}", expectedDate, localDateTime, dateDiff);
        return dateDiff;
    }

    public void addDaysToCurrentDates(long days){
        this.rootFlatFileModels.forEach(rootFlatFileModel -> {
            LocalDateTime localDateTime = getConvertedLocalDateTime(rootFlatFileModel.getSES_PHONE_DIR1_STARTDATE());
            String originalDate = localDateTime.format(flatFileDateFormatter2);
            localDateTime = localDateTime.plusDays(days);
//            log.info("Original date: {}, new date: {}", originalDate, localDateTime.format(flatFileDateFormatter2));
            rootFlatFileModel.setSES_PHONE_DIR1_STARTDATE(localDateTime.format(flatFileDateFormatter2));
        });
    }
}
