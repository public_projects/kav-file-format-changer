package org.green.kav.common.model;

import org.green.kav.common.bean.CallType;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
public class CallActivity {
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private TargetDetailsModel partyA;
    private TargetDetailsModel partyB;
    private LocalDateTime startTime;
    private long duration;
    private CallType CallType;
    private LocationDetail partyAStartArea;
    private LocationDetail partyAEndArea;
    private LocationDetail partyBEndArea;
    private LocationDetail partyBStartArea;
    private String mcInterceptId;
    private String agencyId;
    private String systemId;
    private String contentLocator;

    public DateTimeFormatter getDateTimeFormatter() {
        return dateTimeFormatter;
    }

    public void setDateTimeFormatter(DateTimeFormatter dateTimeFormatter) {
        this.dateTimeFormatter = dateTimeFormatter;
    }

    public TargetDetailsModel getPartyA() {
        return partyA;
    }

    public void setPartyA(TargetDetailsModel partyA) {
        this.partyA = partyA;
    }

    public TargetDetailsModel getPartyB() {
        return partyB;
    }

    public void setPartyB(TargetDetailsModel partyB) {
        this.partyB = partyB;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public org.green.kav.common.bean.CallType getCallType() {
        return CallType;
    }

    public void setCallType(org.green.kav.common.bean.CallType callType) {
        CallType = callType;
    }

    public LocationDetail getPartyAStartArea() {
        return partyAStartArea;
    }

    public void setPartyAStartArea(LocationDetail partyAStartArea) {
        this.partyAStartArea = partyAStartArea;
    }

    public LocationDetail getPartyAEndArea() {
        return partyAEndArea;
    }

    public void setPartyAEndArea(LocationDetail partyAEndArea) {
        this.partyAEndArea = partyAEndArea;
    }

    public LocationDetail getPartyBEndArea() {
        return partyBEndArea;
    }

    public void setPartyBEndArea(LocationDetail partyBEndArea) {
        this.partyBEndArea = partyBEndArea;
    }

    public LocationDetail getPartyBStartArea() {
        return partyBStartArea;
    }

    public void setPartyBStartArea(LocationDetail partyBStartArea) {
        this.partyBStartArea = partyBStartArea;
    }

    public String getMcInterceptId() {
        return mcInterceptId;
    }

    public void setMcInterceptId(String mcInterceptId) {
        this.mcInterceptId = mcInterceptId;
    }

    public String getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(String agencyId) {
        this.agencyId = agencyId;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getContentLocator() {
        return contentLocator;
    }

    public void setContentLocator(String contentLocator) {
        this.contentLocator = contentLocator;
    }
}
