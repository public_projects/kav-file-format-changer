package org.green.kav.common.bean.impl;

import org.green.kav.common.bean.ContentType;

import java.util.ArrayList;
import java.util.List;

public class CellModel implements ContentType {
    private String mechanical_downtilt;
    private String cgi;
    private String longitude;
    private String latitude;
    private String azimuth;
    private String height;
    private String coverage_radius;
    private String bts_transmitter_power;
    private String tower_name;
    private String cell_name;

    @Override
    public List<ContentType> mapToPojo(List<List<String>> dataLs) {
        List<ContentType> cellModels = new ArrayList<>();
        dataLs.forEach(record -> {
            CellModel cdrModel = new CellModel();
            cdrModel.setCgi(record.get(0));
            cdrModel.setLongitude(record.get(1));
            cdrModel.setLatitude(record.get(2));
            cdrModel.setAzimuth(record.get(3));
            cdrModel.setHeight(record.get(4));
            cdrModel.setCell_name(record.get(5));
            cdrModel.setMechanical_downtilt(record.get(6));
            cdrModel.setBts_transmitter_power(record.get(7));
            cdrModel.setTower_name(record.get(8));
            cdrModel.setCell_name(record.get(9));

            cellModels.add(cdrModel);
        });

        return cellModels;
    }

    public String getMechanical_downtilt() {
        return mechanical_downtilt;
    }

    public void setMechanical_downtilt(String mechanical_downtilt) {
        this.mechanical_downtilt = mechanical_downtilt;
    }

    public String getCgi() {
        return cgi;
    }

    public void setCgi(String cgi) {
        this.cgi = cgi;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getAzimuth() {
        return azimuth;
    }

    public void setAzimuth(String azimuth) {
        this.azimuth = azimuth;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getCoverage_radius() {
        return coverage_radius;
    }

    public void setCoverage_radius(String coverage_radius) {
        this.coverage_radius = coverage_radius;
    }

    public String getBts_transmitter_power() {
        return bts_transmitter_power;
    }

    public void setBts_transmitter_power(String bts_transmitter_power) {
        this.bts_transmitter_power = bts_transmitter_power;
    }

    public String getTower_name() {
        return tower_name;
    }

    public void setTower_name(String tower_name) {
        this.tower_name = tower_name;
    }

    public String getCell_name() {
        return cell_name;
    }

    public void setCell_name(String cell_name) {
        this.cell_name = cell_name;
    }
}
