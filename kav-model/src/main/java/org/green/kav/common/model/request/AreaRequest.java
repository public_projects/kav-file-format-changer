package org.green.kav.common.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.awt.geom.Area;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AreaRequest {
    private String name;
    @JsonProperty("long")
    private Double longitude;
    private Double lat;
    private String cgi;
    private String details;

    public AreaRequest(){
    }

    public AreaRequest(String name, Double longitude, Double lat, String cgi, String details) {
        this.name = name;
        this.longitude = longitude;
        this.lat = lat;
        this.cgi = cgi;
        this.details = details;
    }

    public Map<String, Object> getDataByName() {
        Map<String, Object> args = new HashMap<>(4);
        args.put("cgi", getCgi());
        args.put("longitude", getLongitude());
        args.put("latitude", getLat());
        if (details != null && !details.isEmpty()) {
            args.put("details", getDetails());
        }
        return args;
    }

    public Map<String, Object> getDataByCgi() {
        Map<String, Object> args = new HashMap<>(4);
        args.put("name", getName());
        args.put("longitude", getLongitude());
        args.put("latitude", getLat());
        if (details != null && !details.isEmpty()) {
            args.put("details", getDetails());
        }
        return args;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public String getCgi() {
        return cgi;
    }

    public void setCgi(String cgi) {
        this.cgi = cgi;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getLongitude(), getLat(), getCgi());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AreaRequest)) return false;
        AreaRequest that = (AreaRequest) o;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getLongitude(), that.getLongitude()) && Objects.equals(
                getLat(),
                that.getLat()) && Objects.equals(getCgi(), that.getCgi()) && Objects.equals(getDetails(), that.getDetails());
    }
}
