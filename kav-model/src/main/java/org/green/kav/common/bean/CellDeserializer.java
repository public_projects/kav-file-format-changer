package org.green.kav.common.bean;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.NullNode;
import com.google.common.base.CaseFormat;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import static org.green.kav.common.bean.Constants.CELL.*;


/**
 * Custom deserializer to let mapper know how to determine which subclass to instantiate for {@code Cell}
 */
public class CellDeserializer extends StdDeserializer<Cell> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CellDeserializer.class);

    protected CellDeserializer() {
        super(Cell.class);
    }

    /**
     * Deserialize {@code Cell} from csv/json
     * Logic:
     * 1. IF field "cgi" is found, this is the desh delimited cgi string consists of 3 or 4 (ecgi) segments,
     *    this string will be used to construct the {@see Cell} object
     * 1. IF field "cgi" is not found, then it will first use field "eCellId" to check whether this is ecgi,
     *    if field "eCellId" is not found, then this will be treated as normal non LTE CGI which consists of LAC, CellID
     */
    @Override
    public Cell deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
        JsonNode treeNode = jp.readValueAsTree();
        if (treeNode instanceof NullNode) {
            return null;
        }
        Cell result = null;
        try {
            String cgiString = treeNode.get(CGI) != null ? treeNode.get(CGI).asText() : null;
            if (!Strings.isNullOrEmpty(cgiString)) {
                result = CellModelUtil.of(cgiString, extractLocation(treeNode), extractCellMetadata(treeNode));
            } else {
                result = deserializeByDeterminingeCellIdExists(treeNode, extractLocation(treeNode), extractCellMetadata(treeNode));
            }
        } catch (Exception ex) {
            LOGGER.warn("Error while reading json cell data with cgi {}, skipping", treeNode.toString());
        }
        return result;
    }

    private Cell deserializeByDeterminingeCellIdExists(JsonNode jsonNode, Location location, CellMetadata cellMetadata) {
        Cell cell;
        Integer mcc = jsonNode.get(MCC)!= null ? jsonNode.get(MCC).asInt() : null;
        Integer mnc = jsonNode.get(MNC) != null ? jsonNode.get(MNC).asInt() : null;
        if (mcc == null || mnc == null) throw new IllegalArgumentException("Invalid mcc or mnc value is null");
        Integer lac = jsonNode.get(LAC)!= null ? jsonNode.get(LAC).asInt() : null;
        Long cellId = jsonNode.get(CELLID) != null ? jsonNode.get(CELLID).asLong() : null;
        Long eCellId = jsonNode.get(ECELLID) != null ? jsonNode.get(ECELLID).asLong() : null;
        if (eCellId != null) {
            cell = CellModelUtil.of(CellGlobalIdentity.of(mcc, mnc, eCellId), location);
        } else if (lac != null && cellId != null) {
            cell = CellModelUtil.of(CellGlobalIdentity.of(mcc, mnc, lac, cellId), location);
        } else {
            throw new IllegalArgumentException("Invalid cell global identity");
        }
        cell.setCellMetadata(cellMetadata);
        return cell;
    }

    private CellMetadata extractCellMetadata(JsonNode jsonNode) {
        JsonNode cellMetadataJsonNode = jsonNode.get("cellMetadata");
        if (cellMetadataJsonNode == null) return extractCellMetadataFromCsv(jsonNode);
        return extractCellMetadataFromWebJson(cellMetadataJsonNode);
    }

    private CellMetadata extractCellMetadataFromCsv(JsonNode jsonNode) {
        Location location = extractLocation(jsonNode);
        String actualCgi = jsonNode.get(CGI) != null ? jsonNode.get(CGI).asText() : null;
        Double rightLatitude = jsonNode.get(RIGHT_LATITUDE) != null ? jsonNode.get(RIGHT_LATITUDE).asDouble() : null;
        Double rightLongitude = jsonNode.get(RIGHT_LONGITUDE) != null ? jsonNode.get(RIGHT_LONGITUDE).asDouble() : null;
        Double leftLatitude = jsonNode.get(LEFT_LATITUDE) != null ? jsonNode.get(LEFT_LATITUDE).asDouble() : null;
        Double leftLongitude = jsonNode.get(LEFT_LONGITUDE) != null ? jsonNode.get(LEFT_LONGITUDE).asDouble() : null;
        Double midLatitude = jsonNode.get(MID_LATITUDE) != null ? jsonNode.get(MID_LATITUDE).asDouble() : null;
        Double midLongitude = jsonNode.get(MID_LONGITUDE) != null ? jsonNode.get(MID_LONGITUDE).asDouble() : null;
        Double azimuth = jsonNode.get(AZIMUTH) != null ? jsonNode.get(AZIMUTH).asDouble() : null;
        Double height = jsonNode.get(HEIGHT) != null ? jsonNode.get(HEIGHT).asDouble() : null;
        Double coverageRadius = jsonNode.get(COVERAGE_RADIUS) != null ? jsonNode.get(COVERAGE_RADIUS).asDouble() : null;
        Double mechanicalDowntilt = jsonNode.get(MECHANICAL_DOWNTILT) != null ? jsonNode.get(MECHANICAL_DOWNTILT).asDouble() : null;
        Double btsTransmitterPower = jsonNode.get(BTS_TRANSMITTER_POWER) != null ? jsonNode.get(BTS_TRANSMITTER_POWER).asDouble() : null;
        String towerName = jsonNode.get(TOWER_NAME) != null ? jsonNode.get(TOWER_NAME).asText() : null;
        String cellName = jsonNode.get(CELL_NAME) != null ? jsonNode.get(CELL_NAME).asText() : null;
        return new CellMetadata(actualCgi,
                location.getLatitude(),
                location.getLongitude(),
                rightLatitude,
                rightLongitude,
                leftLatitude,
                leftLongitude,
                midLatitude,
                midLongitude,
                azimuth,
                height,
                coverageRadius,
                mechanicalDowntilt,
                btsTransmitterPower,
                towerName,
                cellName,
                null);
    }

    private CellMetadata extractCellMetadataFromWebJson(JsonNode jsonNode) {
        Location location = extractLocation(jsonNode);
        String actualCgi = jsonNode.get(toCamelCase(CGI)) != null ? jsonNode.get(toCamelCase(CGI)).asText() : null;
        Double rightLatitude = jsonNode.get(toCamelCase(RIGHT_LATITUDE)) != null ? jsonNode.get(toCamelCase(RIGHT_LATITUDE)).asDouble() : null;
        Double rightLongitude = jsonNode.get(toCamelCase(RIGHT_LONGITUDE)) != null ? jsonNode.get(toCamelCase(RIGHT_LONGITUDE)).asDouble() : null;
        Double leftLatitude = jsonNode.get(toCamelCase(LEFT_LATITUDE)) != null ? jsonNode.get(toCamelCase(LEFT_LATITUDE)).asDouble() : null;
        Double leftLongitude = jsonNode.get(toCamelCase(LEFT_LONGITUDE)) != null ? jsonNode.get(toCamelCase(LEFT_LONGITUDE)).asDouble() : null;
        Double midLatitude = jsonNode.get(toCamelCase(MID_LATITUDE)) != null ? jsonNode.get(toCamelCase(MID_LATITUDE)).asDouble() : null;
        Double midLongitude = jsonNode.get(toCamelCase(MID_LONGITUDE)) != null ? jsonNode.get(toCamelCase(MID_LONGITUDE)).asDouble() : null;
        Double azimuth = jsonNode.get(toCamelCase(AZIMUTH)) != null ? jsonNode.get(toCamelCase(AZIMUTH)).asDouble() : null;
        Double height = jsonNode.get(toCamelCase(HEIGHT)) != null ? jsonNode.get(toCamelCase(HEIGHT)).asDouble() : null;
        Double coverageRadius = jsonNode.get(toCamelCase(COVERAGE_RADIUS)) != null ? jsonNode.get(toCamelCase(COVERAGE_RADIUS)).asDouble() : null;
        Double mechanicalDowntilt = jsonNode.get(toCamelCase(MECHANICAL_DOWNTILT)) != null ? jsonNode.get(toCamelCase(MECHANICAL_DOWNTILT)).asDouble() : null;
        Double btsTransmitterPower = jsonNode.get(toCamelCase(BTS_TRANSMITTER_POWER)) != null ? jsonNode.get(toCamelCase(BTS_TRANSMITTER_POWER)).asDouble() : null;
        String towerName = jsonNode.get(toCamelCase(TOWER_NAME)) != null ? jsonNode.get(toCamelCase(TOWER_NAME)).asText() : null;
        String cellName = jsonNode.get(toCamelCase(CELL_NAME)) != null ? jsonNode.get(toCamelCase(CELL_NAME)).asText() : null;
        return new CellMetadata(actualCgi,
                location.getLatitude(),
                location.getLongitude(),
                rightLatitude,
                rightLongitude,
                leftLatitude,
                leftLongitude,
                midLatitude,
                midLongitude,
                azimuth,
                height,
                coverageRadius,
                mechanicalDowntilt,
                btsTransmitterPower,
                towerName,
                cellName,
                null);
    }

    private Location extractLocation(JsonNode jsonNode) {
        Double latitude = jsonNode.get(LATITUDE) != null ? jsonNode.get(LATITUDE).asDouble() : null;
        Double longitude = jsonNode.get(LONGITUDE) != null ? jsonNode.get(LONGITUDE).asDouble() : null;
        return Location.of(latitude, longitude);
    }

    private String toCamelCase(String input) {
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, input);
    }

}
