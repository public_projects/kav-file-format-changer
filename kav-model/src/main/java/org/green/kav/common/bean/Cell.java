package org.green.kav.common.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;


@JsonDeserialize(using = CellDeserializer.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class Cell<T extends CellGlobalIdentity> implements Locatable, Serializable {

    private T cellGlobalId = null;
    private Location location = null;

    private CellMetadata cellMetadata;

    public Cell() {
    }

    /**
     * Creates a cell location specification record.
     *
     * @param cellGlobalId the Cell Global Identity
     * @param location the cell's location
     * @throws NullPointerException
     */
    public Cell(T cellGlobalId, Location location) {
        this.cellGlobalId = checkNotNull(cellGlobalId);
        this.location     = checkNotNull(location);
    }

    /**
     * Returns the cell's mobile country code.
     *
     * @return the cell's mobile country code
     */
    public Integer getMcc() {
        return cellGlobalId.getMcc();
    }

    /**
     * Returns the cell's mobile network code.
     *
     * @return the cell's mobile network code
     */
    public Integer getMnc() {
        return cellGlobalId.getMnc();
    }

    /**
     * Returns the latitude
     *
     * Latitude is a geographic coordinate that specifies the north-south
     * position of a point on the Earth's surface
     *
     * @return The latitude
     */
    public Double getLatitude() {
        return location.getLatitude();
    }

    /**
     * Returns the longitude.
     *
     * Longitude is a geographic coordinate that specifies the east-west
     * position of a point on the Earth's surface
     *
     * @return The longitude
     */
    public Double getLongitude() {
        return location.getLongitude();
    }

    /**
     * Returns the Cell Global Identity.
     *
     * @return the Cell Global Identity
     */
    @JsonIgnore
    public T getCellGlobalId() {
        return cellGlobalId;
    }

    /**
     * Returns the cell's location.
     *
     * @return the cell's location
     */
    @JsonIgnore
    @Override
    public Location getLocation() {
        return location;
    }

    public abstract Long getCellId();

    public String getCgi() {
        return cellGlobalId.toMcngEncodedString();
    }

    public CellMetadata getCellMetadata() {
        return cellMetadata;
    }

    public void setCellMetadata(CellMetadata cellMetadata) {
        this.cellMetadata = cellMetadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Cell<?> cell = (Cell<?>) o;

        if (getCellGlobalId() != null ? !getCellGlobalId().equals(cell.getCellGlobalId()) : cell.getCellGlobalId() != null)
            return false;
        if (getLocation() != null ? !getLocation().equals(cell.getLocation()) : cell.getLocation() != null)
            return false;
        return getCellMetadata() != null ? getCellMetadata().equals(cell.getCellMetadata()) : cell.getCellMetadata() == null;
    }

    @Override
    public int hashCode() {
        int result = getCellGlobalId() != null ? getCellGlobalId().hashCode() : 0;
        result = 31 * result + (getLocation() != null ? getLocation().hashCode() : 0);
        result = 31 * result + (getCellMetadata() != null ? getCellMetadata().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Cell{" +
               "cellGlobalId=" + cellGlobalId +
               ", location=" + location +
               ", cellMetadata=" + cellMetadata +
               '}';
    }
}

