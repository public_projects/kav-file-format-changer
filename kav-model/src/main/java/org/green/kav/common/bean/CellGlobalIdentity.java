package org.green.kav.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.base.Preconditions.checkArgument;

public abstract class CellGlobalIdentity implements Serializable {

    /**
     * TODO how to represent?
     * Represents an unknown Cell Global Identity.
     */
    public static final CellGlobalIdentity UNKNOWN = new ConventionalCellGlobalIdentity(0, 0, 0, 0L);

    /** Mobile Country Code */
    private final Integer mcc;

    /** Mobile Network Code */
    private final Integer mnc;

    protected CellGlobalIdentity(Integer mcc, Integer mnc) {
        this.mcc = mcc;
        this.mnc = mnc;
    }

    public static CellGlobalIdentity of(Integer mcc, Integer mnc, Integer lac, Long cellId) {
        checkArgument(mcc > 0, "Mobile Country Code must be strictly positive");
        checkArgument(mnc >= 0, "Mobile Network Code must be positive");
        checkArgument(lac >= 0, "Location Area Code must be positive");
        checkArgument(cellId >= 0, "Cell Identity must be positive");
        return ConventionalCellGlobalIdentity.of(mcc, mnc, lac, cellId);
    }

    public static CellGlobalIdentity of(Integer mcc, Integer mnc, Long eCgi) {
        checkArgument(mcc > 0, "Mobile Country Code must be strictly positive");
        checkArgument(mnc >= 0, "Mobile Network Code must be positive");
        checkArgument(eCgi >= 0, "Cell Identity must be positive");
        return EvolvedCellGlobalIdentity.of(mcc, mnc, eCgi);
    }

    public static CellGlobalIdentity of(String cgi) {
        return createFromMcngEncodedString(cgi);
    }

    public static CellGlobalIdentity createFromMcngEncodedString(String cgi) {
        if (!CellModelUtil.isValidCgi(cgi)) return UNKNOWN;
        return CellModelUtil.isEcgi(cgi) ? EvolvedCellGlobalIdentity.createFromMcngEncodedString(cgi)
                                         : ConventionalCellGlobalIdentity.createFromMcngEncodedString(cgi);
    }

    public static List<CellGlobalIdentity> createFromMcngEncodedStrings(List<String> cgiStrings) {
        if (cgiStrings.isEmpty()) return Collections.emptyList();
        List<CellGlobalIdentity> cgis = new ArrayList<>(cgiStrings.size());
        for (String cgiString : cgiStrings) {
            try {
                cgis.add(createFromMcngEncodedString(cgiString));
            } catch (IllegalArgumentException ignore) {}
        }
        return cgis;
    }

    public Integer getMcc() {
        return mcc;
    }

    public Integer getMnc() {
        return mnc;
    }

    public abstract Long getCellId();

    public abstract String toMcngEncodedString();

    public abstract String toMcngEncodedStringLeftPadded();

}
