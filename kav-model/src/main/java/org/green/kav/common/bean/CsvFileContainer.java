package org.green.kav.common.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class CsvFileContainer {
    private final static Logger log = LoggerFactory.getLogger(CsvFileContainer.class);
    private List<String> headerStr = new ArrayList<>();
    private List<List<String>> dataLs = new ArrayList<>();
    private String delimeter;
    private boolean sortable = true;
    private boolean renewDates = false;

    public CsvFileContainer(String delimeter, boolean renewDates) {
        this.delimeter = delimeter;
        this.renewDates = renewDates;
    }

    public void sort(int orientation) {
        int indexOfColumnToBeSorted = getIndexOfRecord();

        if (indexOfColumnToBeSorted == -1) {
            sortable = false;
            return;
        }

        Collections.sort(dataLs, new Comparator<List<String>>() {
            @Override
            public int compare(List<String> o1, List<String> o2) {
                Long value1 = Long.parseLong(o1.get(indexOfColumnToBeSorted));
                Long value2 = Long.parseLong(o2.get(indexOfColumnToBeSorted));
                if (orientation == 1) {
                    return value1.compareTo(value2);
                }
                else {
                    return value2.compareTo(value1);
                }
            }
        });
    }

    public List<String> getHeaderStr() {
        return headerStr;
    }

    public void setHeaderStr(List<String> headerStr) {
        this.headerStr = headerStr;
    }

    public List<List<String>> getDataLs() {
        return dataLs;
    }

    public void setDataLs(List<List<String>> dataLs) {
        this.dataLs = dataLs;
    }

    public String getDelimeter() {
        return delimeter;
    }

    public void setDelimeter(String delimeter) {
        this.delimeter = delimeter;
    }

    public boolean isSortable() {
        return sortable;
    }

    public void setSortable(boolean sortable) {
        this.sortable = sortable;
    }

    public boolean isRenewDates() {
        return renewDates;
    }

    public void setRenewDates(boolean renewDates) {
        this.renewDates = renewDates;
    }

    public void sort() {
        this.sort(1);
    }

    public boolean isSortableData() {
        return this.sortable;
    }

    private int getIndexOfRecord() {
        int indexOfRecord = headerStr.indexOf("start");
        if (indexOfRecord == -1) {
            indexOfRecord = headerStr.indexOf("START");
        }

        return indexOfRecord;
    }

    public void renewDates() {
        if(!renewDates){
            return;
        }

        if (!isSortableData()) return;

        final DateTimeFormatter ipfDateTimeFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime yesterday = currentTime.minusDays(1);
        log.info("Yesterday: {}", yesterday.format(ipfDateTimeFormat));

        int dateStringIndex = getIndexOfRecord();
        String latestDateTimeStr = dataLs.get(dataLs.size() - 1).get(dateStringIndex);

        LocalDateTime oldDateTime = LocalDateTime.parse(latestDateTimeStr, ipfDateTimeFormat);
        final long daysToAdd = ChronoUnit.DAYS.between(oldDateTime, yesterday);
        log.info("days to add {}", ChronoUnit.DAYS.between(oldDateTime, yesterday));

        dataLs.forEach(strings -> {
            String startDateString = strings.get(dateStringIndex);
            LocalDateTime tempLocalDateTime = LocalDateTime.parse(startDateString, ipfDateTimeFormat);
            tempLocalDateTime = tempLocalDateTime.plusDays(daysToAdd);
            strings.set(dateStringIndex, tempLocalDateTime.format(ipfDateTimeFormat));
            log.info("From: {}, to: {}",startDateString, strings.get(dateStringIndex));
        });
    }

    public StringBuilder toCSV() {
        StringBuilder header = new StringBuilder();
        for (int i = 0; i < headerStr.size(); i++) {
            header.append(headerStr.get(i).toUpperCase());
            if (i == (headerStr.size() - 1)) {
                header.append(System.getProperty("line.separator"));
            }
            else {
                header.append(this.delimeter);
            }
        }

        String headerString = header.toString().substring(0, header.toString().length() - 1);
        header = new StringBuilder(headerString);

        StringBuilder contents = new StringBuilder();

        for (List<String> dataL : dataLs) {
            for (int i = 0; i < dataL.size(); i++) {
                contents.append(dataL.get(i));
                if (i == (dataL.size() - 1)) {
                    contents.append(System.getProperty("line.separator"));
                }
                else {
                    contents.append(this.delimeter);
                }
            }
        }
        return header.append(contents);
    }
}
