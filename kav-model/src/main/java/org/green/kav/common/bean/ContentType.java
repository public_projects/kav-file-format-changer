package org.green.kav.common.bean;

import java.io.Serializable;
import java.util.List;

public interface ContentType extends Serializable {
    List<ContentType> mapToPojo(List<List<String>> dataLs);
}
