package org.green.kav.common.bean;

import java.util.ArrayList;
import java.util.List;


public class CsvFlatFileContainer {
    private List<String> headerStr = new ArrayList<>();
    private List<List<String>> dataLs = new ArrayList<>();
    private String delimeter;
    private boolean sortable = true;
    private boolean renewDates = false;
}
