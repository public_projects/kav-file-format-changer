package org.green.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.CommandLineRunner;
import com.github.javafaker.Faker;

@Slf4j
@SpringBootApplication
public class KavUtilApplication {

	public static void main(String[] args) {
		SpringApplication.run(KavUtilApplication.class, args);
	}


}


