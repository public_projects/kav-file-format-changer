package org.green.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StopWatch;


@Slf4j
@SpringBootTest
class KavUtilApplicationTests {

	@Test
	void contextLoads() throws Exception {
		StopWatch watch = new StopWatch();
		watch.start("Test task");
		task();
		log.info("1: {}", watch.prettyPrint());
		task();
		log.info("2: {}", watch.currentTaskName());
		task();
		log.info("3: {}", watch.getTotalTimeMillis());
		task();
		watch.stop();
		log.info("4: {}", watch.getTotalTimeSeconds());
	}

	public static void task() throws Exception{
		Thread.sleep(3000l);
	}
}
