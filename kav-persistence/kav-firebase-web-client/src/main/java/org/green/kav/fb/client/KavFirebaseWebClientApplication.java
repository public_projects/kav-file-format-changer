package org.green.kav.fb.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"org.green.kav.fb.client", "org.green.kav.common"})
public class KavFirebaseWebClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(KavFirebaseWebClientApplication.class, args);
	}

}
