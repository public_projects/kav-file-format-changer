package org.green.kav.fb.client.core;

import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.bean.AreaContent;
import org.green.kav.common.model.IdentityCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.*;


@Service
public class IpfFirebaseController {
    private final static Logger log = LoggerFactory.getLogger(IpfFirebaseController.class);

    private final WebClient firebaseIpfWebClient;
    private final WebClient firebaseIpfWebClient2;

    public IpfFirebaseController(WebClient.Builder webClientBuilder) {
        this.firebaseIpfWebClient = webClientBuilder.baseUrl("https://greenhorn-c7a65.firebaseio.com/dev_db_1/")
                                                    .build();

        this.firebaseIpfWebClient2 = webClientBuilder.baseUrl("https://green-db512-default-rtdb.asia-southeast1.firebasedatabase.app/dev_db_2/")
                                                    .build();
    }

    public List<IdentityCollection> getTargetsWithEqualIdentities() {
        Mono<Map> targetMap = this.firebaseIpfWebClient2.get()
                                                        .uri("targets.json")
                                                        .retrieve()
                                                        .bodyToMono(Map.class);

        Map<String, Object> targetMapObject = targetMap.block();
        List<IdentityCollection> identityCollections = new ArrayList<>();

        for (Map.Entry<String, Object> keyValue: targetMapObject.entrySet()){
            Map<String, Object> value = (Map<String, Object>) keyValue.getValue();
            String targetName = keyValue.getKey();
            String mcngEtl = (String) value.get("mcng-etl-id");
            List<String> imeiLs = (List<String>) value.get("IMEI");
            List<String> imsiLs = (List<String>) value.get("IMSI");
            List<String> msidsnLs = (List<String>) value.get("MSISDN");

            if (imeiLs.size() != msidsnLs.size() || msidsnLs.size() != imsiLs.size()) {
                log.warn("Unequal identity number for {}, with {} msisdn, {} imei, {} imsi", targetName, msidsnLs, imsiLs, imeiLs);
                continue;
            }

            IdentityCollection identityCollection = IdentityCollection.of(targetName, msidsnLs, imsiLs, imeiLs, mcngEtl);
            identityCollections.add(identityCollection);
        }

        return identityCollections;
    }

    public List<String> getTargetNamesWithEqualIdentities() {
        Mono<Map> targetMap = this.firebaseIpfWebClient2.get()
                                                        .uri("targets.json")
                                                        .retrieve()
                                                        .bodyToMono(Map.class);

        Map<String, Object> targetMapObject = targetMap.block();
        List<String> targetNames = new ArrayList<>();

        for (Map.Entry<String, Object> keyValue: targetMapObject.entrySet()){
            Map<String, Object> value = (Map<String, Object>) keyValue.getValue();
            String targetName = keyValue.getKey();
            List<String> imeiLs = (List<String>) value.get("IMEI");
            List<String> imsiLs = (List<String>) value.get("IMSI");
            List<String> msidsnLs = (List<String>) value.get("MSISDN");

            if (imeiLs.size() != msidsnLs.size() || msidsnLs.size() != imsiLs.size()) {
                log.warn("Unequal identity number for {}, with {} msisdn, {} imei, {} imsi", targetName, msidsnLs, imsiLs, imeiLs);
                continue;
            }
            targetNames.add(targetName);
        }

        return targetNames;
    }


    public List<IdentityCollection> getAllTargets() {
        Mono<Map> targetMap = this.firebaseIpfWebClient2.get()
                                                       .uri("targets.json")
                                                       .retrieve()
                                                       .bodyToMono(Map.class);

        Map<String, Object> targetMapObject = targetMap.block();
        List<IdentityCollection> identityCollections = new ArrayList<>();

        targetMapObject.forEach((s, o) -> {
            String mcngEtl = (String) ((Map<String, Object>) o).get("mcng-etl-id");
            List<String> imeiLs = (List<String>) ((Map<String, Object>) o).get("IMEI");
            List<String> imsiLs = (List<String>) ((Map<String, Object>) o).get("IMSI");
            List<String> msidsnLs = (List<String>) ((Map<String, Object>) o).get("MSISDN");
            IdentityCollection identityCollection = IdentityCollection.of(s, msidsnLs, imsiLs, imeiLs, mcngEtl);
            identityCollections.add(identityCollection);
        });

        return identityCollections;
    }

    public IdentityCollection getTargetByName(String targetName) throws Exception {
        for (IdentityCollection identity: getAllTargets()) {
            if (targetName.equalsIgnoreCase(identity.getTargetName())) return identity;
        }
        throw new Exception("can't find target for given: "+ targetName);
    }

    @Cacheable("countries")
    public Mono<Map> getCountries() {
        return this.firebaseIpfWebClient.get()
                                        .uri("country_by_code.json")
                                        .retrieve()
                                        .bodyToMono(Map.class);

    }



    @Cacheable("mcng")
    public Mono<String> getMcng(){
        return this.firebaseIpfWebClient.get()
                                        .uri("mcng.json")
                                        .retrieve()
                                        .bodyToMono(String.class);
    }

    @Cacheable("area")
    public Mono<String> getArea(){
        return this.firebaseIpfWebClient.get()
                                        .uri("area.json")
                                        .retrieve()
                                        .bodyToMono(String.class);
    }

    @Cacheable("target")
    public Mono<String> getTarget(){
        return this.firebaseIpfWebClient2.get()
                                        .uri("targets.json")
                                        .retrieve()
                                        .bodyToMono(String.class);
    }

    @Cacheable("identities")
    public Mono<String> getIdentities(){
        return this.firebaseIpfWebClient2.get()
                                        .uri("identities.json")
                                        .retrieve()
                                        .bodyToMono(String.class);
    }

    @Cacheable("country_by_code")
    public Mono<String> getCountryByCode(){
        return this.firebaseIpfWebClient.get()
                                        .uri("country_by_code.json")
                                        .retrieve()
                                        .bodyToMono(String.class);
    }
    @Cacheable("operator_by_country")
    public Mono<String> getOperatorByCountry(){
        return this.firebaseIpfWebClient.get()
                                        .uri("operator_by_country.json")
                                        .retrieve()
                                        .bodyToMono(String.class);
    }

    @Cacheable("areas")
    public Map<String, AreaContent> getAllAreas() {
        Mono<Map> map = this.firebaseIpfWebClient2.get().uri("area.json").retrieve().bodyToMono(Map.class);

        Map<String, Map<String, String>> areaMap = map.block();

        Map<String, AreaContent> areaContents = new HashMap<>();
        areaMap.forEach((s, stringStringMap) -> {
            AreaContent areaContent = new AreaContent();
            areaContent.setName(s);
            areaContent.setCgi(stringStringMap.get("cgi"));
            Object latitudeStr = stringStringMap.get("latitude");

            if (latitudeStr instanceof Double) {
                areaContent.setLat(latitudeStr + "");
            }
            else {
                areaContent.setLat((String) latitudeStr);
            }

            Object longitudeStr = stringStringMap.get("longitude");

            if (longitudeStr instanceof Double) {
                areaContent.setLongitude(longitudeStr + "");
            }
            else {
                areaContent.setLongitude((String) longitudeStr);
            }

            areaContents.put(areaContent.getName(), areaContent);
        });

        return areaContents;
    }

    public void getMcngTarget() {
        Mono<Map> map = this.firebaseIpfWebClient.get()
                                                 .uri("mcng/targetName.json")
                                                 .retrieve()
                                                 .bodyToMono(Map.class);

        log.info("Get mcngTarget: {}", map.block());
    }

//    private Flux<PastDrawDateResult> getDrawDates(String url) {
//        log.info("URL: {}", url);
//        return webClientBuilder.build().get().uri(url)
//                               .retrieve()
//                               .bodyToFlux(PastDrawDateResult.class)
//                               .retryBackoff(5, Duration.ofSeconds(1), Duration.ofSeconds(20))
//                               .doOnError(IOException.class, e -> log.error(e.getMessage()));
//    }

}
