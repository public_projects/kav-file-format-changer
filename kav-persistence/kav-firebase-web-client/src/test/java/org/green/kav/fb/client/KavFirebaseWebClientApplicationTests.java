package org.green.kav.fb.client;

import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.model.IdentityCollection;
import org.green.kav.fb.client.core.IpfFirebaseController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
class KavFirebaseWebClientApplicationTests {

    @Autowired
    private IpfFirebaseController ipfFirebaseController;

    @Test
    void contextLoads() {
        List<IdentityCollection> data = ipfFirebaseController.getAllTargets();
        log.info("{}", data.toString());
        ipfFirebaseController.getMcngTarget();

//		Map countryCodeMap1 = ipfFirebaseController.getCountries().block();
//
//		List<String> codes = new ArrayList<>();
//		countryCodeMap1.keySet().forEach(o -> {codes.add((String) o);});
//
//		countryCodeMap1.keySet().forEach(o -> log.info("test 1: {}", o));
//
//		ipfFirebaseController.getAllAreas();
    }

}
