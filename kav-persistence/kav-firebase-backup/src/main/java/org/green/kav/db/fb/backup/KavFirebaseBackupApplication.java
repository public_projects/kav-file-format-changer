package org.green.kav.db.fb.backup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"org.green.kav.db.fb.backup","org.green.kav.fb.client.core"})
public class KavFirebaseBackupApplication {

    public static void main(String[] args) {
        SpringApplication.run(KavFirebaseBackupApplication.class, args);
    }

}
