package org.green.kav.db.fb.backup.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.green.kav.fb.client.core.IpfFirebaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import reactor.core.publisher.Mono;
import org.apache.commons.io.FileUtils;

@Service
public class DataExtraction implements CommandLineRunner {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private IpfFirebaseController ipfFirebaseController;

    public void writeDataOut(Mono<String> data, String fileName) {
        PrintWriter out = null;
        try {
            Object json = mapper.readValue(data.block(), Object.class);
            String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
            File file = new File(fileName);
            System.err.println(file.getAbsolutePath());
            FileUtils.writeStringToFile(file, indented, "UTF-8");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    @Override
    public void run(String... args) throws Exception {
        writeDataOut(ipfFirebaseController.getMcng(), "mcng.json");
        writeDataOut(ipfFirebaseController.getTarget(), "target.json");
        writeDataOut(ipfFirebaseController.getIdentities(), "identities.json");
        writeDataOut(ipfFirebaseController.getArea(), "area.json");
        writeDataOut(ipfFirebaseController.getCountryByCode(), "countryByCode.json");
        writeDataOut(ipfFirebaseController.getOperatorByCountry(), "operatorByCountry.json");
    }
}
