package org.green.kav.db.fb;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@Slf4j
@SpringBootApplication
@EnableCaching
@EnableAsync
@ComponentScan({"org.green.kav.db.fb",
				"org.green.kav.fb.client"})
public class KavFirebaseConnectionApplication {
	public static void main(String[] args) {
		SpringApplication.run(KavFirebaseConnectionApplication.class, args);
	}

//	@Bean
//	public ObjectMapper getObjectMapper() {
//		JavaTimeModule module = new JavaTimeModule();
//		LocalDateTimeDeserializer localDateTimeDeserializer =
//				new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
//		module.addDeserializer(LocalDateTime.class, localDateTimeDeserializer);
//		ObjectMapper objectMapperObj = Jackson2ObjectMapperBuilder.json()
//																  .modules(module)
//																  .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
//																  .build();
//		return objectMapperObj;
//	}
}
