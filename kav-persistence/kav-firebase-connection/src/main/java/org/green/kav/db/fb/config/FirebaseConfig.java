package org.green.kav.db.fb.config;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.CollectionReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.io.FileInputStream;
import java.io.IOException;

@Slf4j
@Configuration
@EnableAsync
public class FirebaseConfig {

    @Bean(name = "firebaseAppDs1")
    public FirebaseApp getFirebaseAppDs1() throws IOException {
        FileInputStream
                serviceAccount = new FileInputStream("D:\\software\\googleCredentials\\greenhorn-c7a65-firebase-adminsdk-x802d-9a5cc4a133.json");

        //Initialize the app with a service account, granting admin privileges
        FirebaseOptions options = FirebaseOptions.builder()
                                          .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                                          .setDatabaseUrl("https://greenhorn-c7a65.firebaseio.com")
                                          .build();
        return FirebaseApp.initializeApp(options, "firebaseAppDs1");
    }

    @Bean(name = "firebaseAppDs2")
    public FirebaseApp getFirebaseAppDs2() throws IOException {
        FileInputStream
                serviceAccount = new FileInputStream("D:\\software\\googleCredentials\\green-db512-firebase-adminsdk-vl8eu-f63f5da293.json");

        //Initialize the app with a service account, granting admin privileges
        FirebaseOptions options = FirebaseOptions.builder()
                                                 .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                                                 .setDatabaseUrl("https://green-db512-default-rtdb.asia-southeast1.firebasedatabase.app/")
                                                 .build();
        return FirebaseApp.initializeApp(options, "firebaseAppDs2");
    }

    @Bean(name = "firebaseDatabase2")
    public FirebaseDatabase getFirebaseDatabase2(FirebaseApp firebaseAppDs2) {
        return FirebaseDatabase.getInstance(firebaseAppDs2);
    }

    @Bean(name = "targetDbReference2")
    public DatabaseReference getTargetDbReference2(FirebaseDatabase firebaseDatabase2){
        DatabaseReference db = firebaseDatabase2.getReference("dev_db_2");
        return db.child("targets");
    }

    @Bean(name = "areaDbReference2")
    public DatabaseReference getAreaDbReference2(FirebaseDatabase firebaseDatabase2){
        DatabaseReference db = firebaseDatabase2.getReference("dev_db_2");
        return db.child("area");
    }

    @Bean(name = "cgiDbReference2")
    public DatabaseReference getCgiDbReference2(FirebaseDatabase firebaseDatabase2){
        DatabaseReference db = firebaseDatabase2.getReference("dev_db_2");
        return db.child("cgi");
    }

    @Bean(name = "identityDbReference2")
    public DatabaseReference getIdentityDbReference2(FirebaseDatabase firebaseDatabase2){
        DatabaseReference db = firebaseDatabase2.getReference("dev_db_2");
        return db.child("identities");
    }

    @Bean(name = "mcngDbReference2")
    public DatabaseReference getMcngDbReference2(FirebaseDatabase firebaseDatabase2){
        DatabaseReference db = firebaseDatabase2.getReference("dev_db_2");
        return db.child("mcng");
    }

    /**
     * This is from
     * @return
     * @throws IOException
     */
    @Bean
    public Firestore getFirestore() throws IOException {
        FileInputStream serviceAccount =
                new FileInputStream("D:\\software\\googleCredentials\\green-db512-firebase-adminsdk-vl8eu-f63f5da293.json");

        FirebaseOptions options = FirebaseOptions.builder()
                .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                .build();

        FirebaseApp firestoreDs1 = FirebaseApp.initializeApp(options, "firestoreDs1");
        return FirestoreClient.getFirestore(firestoreDs1);
    }

    @Bean(name = "targetsCollectionRef")
    public CollectionReference getTargetsCollectionRef(Firestore firestore) {
        return firestore.collection("targets");
    }

    @Bean(name = "mcngCollectionRef")
    public CollectionReference getMcngCollectionRef(Firestore firestore) {
        return firestore.collection("mcngId");
    }

    @Bean(name = "firebaseDatabase")
    public FirebaseDatabase getFirebaseDatabase(FirebaseApp firebaseAppDs1) {
        return FirebaseDatabase.getInstance(firebaseAppDs1);
    }

    @Bean(name = "rootDbReference")
    public DatabaseReference getRootDbReference(FirebaseDatabase firebaseDatabase){
        DatabaseReference db = firebaseDatabase.getReference("dev_db_1");
        return firebaseDatabase.getReference("dev_db_1");
    }

    @Autowired
    @Bean(name = "countryCodeDbReference")
    public DatabaseReference getCountryCodeDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("country_by_code");
    }

    @Autowired
    @Bean(name = "targetDbReference")
    public DatabaseReference getTargetDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("targets");
    }

    @Autowired
    @Bean(name = "identityDbReference")
    public DatabaseReference getIdentityDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("identities");
    }

    @Autowired
    @Bean(name = "mcngDbReference")
    public DatabaseReference getMcngDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("mcng");
    }

    @Autowired
    @Bean(name = "areaDbReference")
    public DatabaseReference getAreaDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("area");
    }

    @Autowired
    @Bean(name = "cgiDbReference")
    public DatabaseReference getCgiDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("cgi");
    }

    @Autowired
    @Bean(name = "imeiTypeAllocationCodeDbReference")
    public DatabaseReference getImeiTypeAllocationCodeDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("ac_imei_type_allocation_code");
    }

//    @Autowired
//    @Bean(name = "mcngIdsDbReference")
//    public DatabaseReference getMcngIdDbReference(DatabaseReference mcngDbReference){
//        return mcngDbReference.child("id");
//    }
//
//    @Autowired
//    @Bean(name = "mcngTargetNameDbReference")
//    public DatabaseReference getMcngTargetNameDbReference(DatabaseReference mcngDbReference){
//        return mcngDbReference.child("targetName");
//    }

    @Autowired
    @Bean(name = "msisdnToTargetDbReference")
    public DatabaseReference getMsisdnDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("msisdnToTarget");
    }
}
