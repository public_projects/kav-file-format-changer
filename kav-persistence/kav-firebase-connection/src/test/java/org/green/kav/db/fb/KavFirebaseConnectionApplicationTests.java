package org.green.kav.db.fb;

import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Slf4j
@SpringBootTest
class KavFirebaseConnectionApplicationTests {

    @Test
    void contextLoads() throws Exception {
        String msisdn = " ";

        String newMsisdn = ((msisdn == null || msisdn.trim()
                                                     .isEmpty()) ?
                            null :
                            msisdn.trim()
                                  .toLowerCase());
        Assertions.assertNull(newMsisdn);

        msisdn = "";
        newMsisdn = ((msisdn == null || msisdn.trim()
                                              .isEmpty()) ?
                     null :
                     msisdn.trim()
                           .toLowerCase());
        Assertions.assertNull(newMsisdn);
    }

}
