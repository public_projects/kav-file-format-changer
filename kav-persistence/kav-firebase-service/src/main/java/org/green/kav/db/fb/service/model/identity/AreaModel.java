package org.green.kav.db.fb.service.model.identity;

import java.util.Objects;

public class AreaModel {
    private String name;
    private String cgi;
    private double latitude;
    private double longitude;

    public AreaModel(String name, String cgi, double latitude, double longitude) {
        this.name = name;
        this.cgi = cgi;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCgi() {
        return cgi;
    }

    public void setCgi(String cgi) {
        this.cgi = cgi;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AreaModel)) return false;
        AreaModel areaModel = (AreaModel) o;
        return Double.compare(areaModel.getLatitude(), getLatitude()) == 0 && Double.compare(areaModel.getLongitude(), getLongitude()) ==
                                                                              0 && Objects.equals(getName(), areaModel.getName()) &&
               Objects.equals(getCgi(), areaModel.getCgi());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getCgi(), getLatitude(), getLongitude());
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", cgi='" + cgi + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
