package org.green.kav.db.fb.service.core.mcng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.common.model.TargetDetailsModel;

import java.time.LocalDateTime;
import java.util.LinkedList;



public class TargetImsiFlatFileService extends FlatFileService {
    public TargetImsiFlatFileService() {
        super("P_TARGET_IMSI_FLAT.csv");
    }
    @Override
    protected String toCsvString(TargetDetailsModel model) {
        StringBuilder flatFileStringData = new StringBuilder();

        for(String imsi: model.getImsi()) {
            LocalDateTime dateTime = LocalDateTime.now();
            LinkedList<String> csvRowCells = new LinkedList<>();
            csvRowCells.add(model.getMcngId());
            csvRowCells.add(imsi);
            csvRowCells.add(dateTime.format(getMcngDateTimeFormat()));
            csvRowCells.add("1");
            csvRowCells.add("mcng");
            csvRowCells.add("3");

            flatFileStringData.append(String.join(this.getDelimeter(), csvRowCells)
                                            .concat(NEWLINE));
        }

        return flatFileStringData.toString();
    }

    @Override
    protected String getHeaders() {
        return "\"MCTARGETID\"|\"IMSIID\"|\"CREATED\"|\"ISASSIGNEDBYANALYSTS\"|\"SYSTEMID\"|\"MCAGENCYID\"";
    }
}
