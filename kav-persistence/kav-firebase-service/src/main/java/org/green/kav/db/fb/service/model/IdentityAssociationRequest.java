package org.green.kav.db.fb.service.model;

public class IdentityAssociationRequest {
    private String identityType;
    private String identityValue;

    private String targetName;

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityValue() {
        return identityValue;
    }

    public void setIdentityValue(String identityValue) {
        this.identityValue = identityValue;
    }
}
