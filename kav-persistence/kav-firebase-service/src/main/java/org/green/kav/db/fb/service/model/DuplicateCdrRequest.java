package org.green.kav.db.fb.service.model;

import java.util.List;

public class DuplicateCdrRequest {
    private String cdrFileToDuplicate;
    private String newCdrFileOutputDirectory;
    private String outputFilePostFix;
    private int numberOfRecordForEachTarget;
    private List<String> targetNames;

    public List<String> getTargetNames() {
        return targetNames;
    }

    public void setTargetNames(List<String> targetNames) {
        this.targetNames = targetNames;
    }



    public String getCdrFileToDuplicate() {
        return cdrFileToDuplicate;
    }

    public void setCdrFileToDuplicate(String cdrFileToDuplicate) {
        this.cdrFileToDuplicate = cdrFileToDuplicate;
    }

    public String getNewCdrFileOutputDirectory() {
        return newCdrFileOutputDirectory;
    }

    public void setNewCdrFileOutputDirectory(String newCdrFileOutputDirectory) {
        this.newCdrFileOutputDirectory = newCdrFileOutputDirectory;
    }

    public String getOutputFilePostFix() {
        return outputFilePostFix;
    }

    public void setOutputFilePostFix(String outputFilePostFix) {
        this.outputFilePostFix = outputFilePostFix;
    }

    public int getNumberOfRecordForEachTarget() {
        return numberOfRecordForEachTarget;
    }

    public void setNumberOfRecordForEachTarget(int numberOfRecordForEachTarget) {
        this.numberOfRecordForEachTarget = numberOfRecordForEachTarget;
    }
}
