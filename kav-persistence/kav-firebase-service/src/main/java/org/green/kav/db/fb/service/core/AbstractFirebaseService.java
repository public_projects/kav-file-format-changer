package org.green.kav.db.fb.service.core;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import org.green.kav.db.fb.service.listener.CompletionListenerImpl;
import org.green.kav.db.fb.service.listener.ValueEventListenerImpl;
import org.green.kav.db.fb.service.model.ProgressStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public abstract class AbstractFirebaseService {
    protected Logger log = LoggerFactory.getLogger(this.getClass());
    private final DatabaseReference dbReference;
    private static Random random = new Random();

    public AbstractFirebaseService(DatabaseReference dbReference) {
        this.dbReference = dbReference;
    }

    public static String getRandomNumber(int length){
        String temp = "";
        for (int i = 0; i < length; i++) {
            temp += random.nextInt(9)+"";
        }
        return temp;
    }

    public Map<String, Object> get() {
        return get(getDbReference());
    }

    public DatabaseReference getDbReference() {
        return dbReference;
    }

    public Map<String, Object> get(DatabaseReference dbReference, String id) {
        return get(dbReference.child(id));
    }

    public Map<String, Object> get(String id) {
        return get(this.dbReference.child(id));
    }

    public Map<String, Object> removeByDbRef(DatabaseReference databaseReference){
        ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.DELETING);
        CompletionListenerImpl completionListener = new CompletionListenerImpl(progressStatus);
        databaseReference.removeValue(completionListener);

        waitTillFinish(progressStatus, 200l);

        if (progressStatus.isFailed()) {
            return getFailureResult("Failed in firebase request!");
        }

        log.info("{}, in {} ms", progressStatus.getStatus(), progressStatus.getTimeTookInMs());
        return getSuccessResult(progressStatus.getValue());
    }

    public Map<String, Object> remove(String id) {
        ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.DELETING);
        CompletionListenerImpl completionListener = new CompletionListenerImpl(progressStatus);
        dbReference.child(id).removeValue(completionListener);

        waitTillFinish(progressStatus, 200l);

        if (progressStatus.isFailed()) {
            return getFailureResult("Failed in firebase request!");
        }

        log.info("{}, in {} ms", progressStatus.getStatus(), progressStatus.getTimeTookInMs());
        return getSuccessResult(progressStatus.getValue());
    }

    public Map<String, Object> get(DatabaseReference dbReference) {
        ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.FETCHING);
        ValueEventListener eventlistener = new ValueEventListenerImpl(progressStatus);
        dbReference.addListenerForSingleValueEvent(new ValueEventListenerImpl(progressStatus));

        waitTillFinish(progressStatus, 1000l);

        if (progressStatus.isFailed()) {
            return getFailureResult("Failed in firebase request!");
        }

        dbReference.removeEventListener(eventlistener);
        log.info("{}, in {} ms", progressStatus.getStatus(), progressStatus.getTimeTookInMs());
        return getSuccessResult(progressStatus.getValue());
    }

    public Map<String, Object> put(String id, Map<String, Object> targetData) {
        ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.CREATING);
        CompletionListenerImpl completionListener = new CompletionListenerImpl(progressStatus);
        this.dbReference.child(id).setValue(targetData, completionListener);

        waitTillFinish(progressStatus, 200l);

        if (progressStatus.isFailed()) {
            return getFailureResult("Failed in firebase request!");
        }

        log.info("{}, in {} ms", progressStatus.getStatus(), progressStatus.getTimeTookInMs());
        return get(this.dbReference.child(id));
    }

    public Map<String, Object> put(DatabaseReference dbReference, String id, Map<String, Object> targetData) {
        ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.CREATING);
        CompletionListenerImpl completionListener = new CompletionListenerImpl(progressStatus);
        dbReference.child(id).setValue(targetData, completionListener);

        waitTillFinish(progressStatus, 200l);

        if (progressStatus.isFailed()) {
            return getFailureResult("Failed in firebase request!");
        }

        log.info("{}, in {} ms", progressStatus.getStatus(), progressStatus.getTimeTookInMs());
        return get(dbReference.child(id));
    }

    public static void waitTillFinish(ProgressStatus progressStatus, long l) {
        while (progressStatus.isInprogress()) {
            try {
                Thread.sleep(l);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static Map<String, Object> getResultContainer(String code, Object content) {
        Map<String, Object> data = new HashMap<>();
        data.put("code", code);
        data.put("message", content);
        return data;
    }

    public static Map<String, Object> getFailureResult(String message) {
        return getResultContainer("500", message);
    }

    public static Map<String, Object> getSuccessResult(Object content) {
        return getResultContainer("200", content);
    }
}
