package org.green.kav.db.fb.service.core;

import com.google.firebase.database.DatabaseReference;
import org.green.kav.common.model.request.AreaRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CgiService extends AbstractFirebaseService {

    @Autowired
    public CgiService(DatabaseReference cgiDbReference2) {
        super(cgiDbReference2);
    }

    public List<Map<String, Object>> put(List<AreaRequest> areaRequestLs) {
        List<Map<String, Object>> persistedArea = new ArrayList<>(areaRequestLs.size());
        for (AreaRequest areaRequest : areaRequestLs) {
            if (areaRequest.getCgi() == null || areaRequest.getCgi().isEmpty()) continue;;
            Map<String, Object> areaData = new HashMap<>(4);
            areaData = super.put(areaRequest.getCgi(), areaRequest.getDataByCgi());
            areaData.put("cgi", areaRequest.getCgi());
            persistedArea.add(areaData);
        }
        return persistedArea;
    }
}
