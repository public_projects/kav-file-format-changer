package org.green.kav.db.fb.service.listener;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.db.fb.service.model.ProgressStatus;


public class ValueEventListenerImpl implements ValueEventListener {
    private final static Logger log = LoggerFactory.getLogger(ValueEventListenerImpl.class);
    private ProgressStatus progressStatus;

    public ValueEventListenerImpl(ProgressStatus progressStatus){
        this.progressStatus = progressStatus;
    }
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        progressStatus.completed(dataSnapshot.getValue());
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        log.error("cancelled {}, {}", databaseError.getCode(), databaseError.getMessage());
        progressStatus.failed();
    }
}
