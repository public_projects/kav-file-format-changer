package org.green.kav.db.fb.service.core.report;

import java.time.Instant;

public class AreaRecordInfo {
    private final Instant startTime;
    private Instant endTime;

    public AreaRecordInfo(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public Instant getStartTime() {
        return startTime;
    }

    @Override
    public String toString() {
        return "{" +
                "startTime=" + startTime +
                ", endTime=" + endTime +
                "} \n";
    }
}
