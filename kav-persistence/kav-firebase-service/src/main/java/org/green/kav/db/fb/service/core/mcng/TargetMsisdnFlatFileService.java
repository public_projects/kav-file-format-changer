package org.green.kav.db.fb.service.core.mcng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.common.model.TargetDetailsModel;

import java.time.LocalDateTime;
import java.util.LinkedList;


public class TargetMsisdnFlatFileService extends FlatFileService {
    public TargetMsisdnFlatFileService() {
        super("P_TARGET_MSISDN_FLAT.csv");
    }
    @Override
    protected String toCsvString(TargetDetailsModel model) {
        StringBuilder flatFileStringData = new StringBuilder();

        for(String msisdn: model.getMsisdn()) {
            LocalDateTime dateTime = LocalDateTime.now();
            LinkedList<String> csvRowCells = new LinkedList<>();
            csvRowCells.add(model.getMcngId());
            csvRowCells.add(msisdn);
            csvRowCells.add(dateTime.format(getMcngDateTimeFormat()));
            csvRowCells.add("1");
            csvRowCells.add("mcng");
            csvRowCells.add("3");

            flatFileStringData.append(String.join(this.getDelimeter(), csvRowCells)
                                            .concat(NEWLINE));
        }

        return flatFileStringData.toString();
    }

    @Override
    protected String getHeaders() {
        return "\"MCTARGETID\"|\"MSISDNID\"|\"CREATED\"|\"ISASSIGNEDBYANALYSTS\"|\"SYSTEMID\"|\"MCAGENCYID\"";
    }
}
