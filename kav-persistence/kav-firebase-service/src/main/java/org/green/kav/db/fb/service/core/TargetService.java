package org.green.kav.db.fb.service.core;

import com.github.javafaker.Faker;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.common.model.IdentityCollection;
import org.green.kav.common.model.TargetDetailsModel;
import org.green.kav.db.fb.service.listener.CompletionListenerImpl;
import org.green.kav.db.fb.service.listener.ValueEventListenerImpl;
import org.green.kav.db.fb.service.model.ProgressStatus;
import org.green.kav.db.fb.service.model.identity.IdentityModel;
import org.green.kav.db.fb.service.model.identity.TargetModel;
import org.green.kav.fb.client.core.IpfFirebaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;



@Service
public class TargetService extends AbstractFirebaseService {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
    @Autowired
    private CountryService countryService;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private McngService mcngService;
    private Faker faker;
    @Autowired
    private IpfFirebaseController ipfFirebaseController;
    @Autowired
    private ImeiTypeAllocationCodeService imeiTypeAllocationCodeService;

    /**
     *
     * @param targetDbReference2 using database which is in asia pacific region.
     */
    @Autowired
    public TargetService(DatabaseReference targetDbReference2) {
        super(targetDbReference2);
        this.faker = new Faker();
    }

    public Map<String, Object> createByIdentities(String firstName, String lastName, String msisdn, String imei, String imsi) {
        Map<String, Object> targetDetails = this.get(firstName + " " + lastName);
        Map<String, Object> data = (Map<String, Object>) targetDetails.get("message");

        if (data != null) {
            targetDetails.put("message", "Given target already exist!");
            targetDetails.put("data", data);
            return targetDetails;
        }

        String targetName = firstName + " " + lastName;

        List<String> targets = new ArrayList<String>();
        targets.add(targetName);
        Map<String, String> targetsWithKey = mcngService.putTarget(targets);
        Set<String> mcngIdSet = targetsWithKey.keySet();
        List<String> aList = new ArrayList<>(mcngIdSet);
        String mcngId = aList.get(0);
        log.info("generated mcngId: {}", mcngId);

        IdentityCollection identityCollection = IdentityCollection.of(targetName, msisdn, imsi, imei, mcngId);
        identityService.putMSISDN(msisdn, targetName, mcngId);
        identityService.putIMEI(imei, targetName, mcngId);
        identityService.putIMSI(imsi, targetName, mcngId);
        identityService.refreshCache();
        Map<String, Object> targetContainer = identityCollection.getTargetContainer();
        this.putTarget(targetContainer);

        return targetContainer;
    }

    public TargetModel getTargetByName(String targetName) {
        Map<String, Object> targetDetails = this.get(targetName);
        Map<String, Object> data = (Map<String, Object>) targetDetails.get("message");

        if (data == null) {
            log.info("Target with given name: {} doesn't exist", targetName);
            return null;
        }

        List<String> msisdns = (List<String>) data.get("MSISDN");
        List<String> imeis = (List<String>) data.get("IMEI");
        List<String> imsis = (List<String>) data.get("IMSI");
        String mcngEtl = (String) data.get("mcng-etl-id");
        TargetModel targetModel = new TargetModel(targetName, mcngEtl, msisdns, imeis, imsis);
        for (String key : data.keySet()) {
            if (!key.equalsIgnoreCase("MSISDN") &&
                    !key.equalsIgnoreCase("IMEI") &&
                    !key.equalsIgnoreCase("IMSI") &&
                    !key.equalsIgnoreCase("mcng-etl-id")) {

                Object otherIdentities = data.get(key);

                if (otherIdentities != null) {
                    targetModel.addOtherIdentities(key, (List<String>) otherIdentities);
                }
            }
        }

        return new TargetModel(targetName, mcngEtl, msisdns, imeis, imsis);
    }

    public Map<String, Object> deleteTarget(String fullName) {
        Map<String, Object> targetDetails = this.get(fullName);
        Map<String, Object> data = (Map<String, Object>) targetDetails.get("message");

        if (data == null) {
            targetDetails.put("message", "Target with given name doesn't exist");
            return targetDetails;
        }

        List<String> imeiList = (List<String>) data.get("IMEI");
        List<String> msisdnList = (List<String>) data.get("MSISDN");
        List<String> imsiList = (List<String>) data.get("IMSI");
        imeiList.forEach(s -> identityService.remove("IMEI", s));
        msisdnList.forEach(s -> identityService.remove("MSISDN", s));
        imsiList.forEach(s -> identityService.remove("IMSI", s));

        return this.remove(fullName);
    }

    /**
     *
     * @param numberOfRandomTargets
     * @param msisdnCount
     * @param imeiCount
     * @param imsiCount
     * @param save
     * @return
     */
    public List<TargetDetailsModel> generateRandomTargets(int numberOfRandomTargets,
                                                     int msisdnCount,
                                                     int imeiCount,
                                                     int imsiCount,
                                                     boolean save) {

        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        List<TargetDetailsModel> createdTargets = new ArrayList<>();
        for (int i = 0; i < numberOfRandomTargets; i++) {
            createdTargets.add(createTarget(threadLocalRandom, msisdnCount, imeiCount, imsiCount, save));
        }

        return createdTargets;
    }

    public List<TargetModel> getTargetModels() {
        List<TargetModel> targetModels = new ArrayList<>();
        AtomicInteger targetCount = new AtomicInteger();
        this.get()
            .forEach((s, o) -> {
                if (o instanceof Map) {
                    ((Map) o).forEach((name, o2) -> {
                        List<String> msisdns = (List<String>) ((Map) o2).get("MSISDN");
                        List<String> imeis = (List<String>) ((Map) o2).get("IMEI");
                        List<String> imsis = (List<String>) ((Map) o2).get("IMSI");
                        String mcngEtl = (String) ((Map) o2).get("mcng-etl-id");
//                        log.debug("{}) Target: {}, mcng-etl: {}", targetCount.incrementAndGet(), name, mcngEtl);
//                        log.debug("MSISDN: {}", msisdns);
//                        log.debug("IMEI: {}", imeis);
//                        log.debug("IMSI: {}", imsis);
                        targetModels.add(new TargetModel((String) name, mcngEtl, msisdns, imeis, imsis));
                    });
                }
            });
        return targetModels;
    }

    public TargetModel associateIdentity(String targetName, String identityId, String identityType) {
        TargetModel targetModel = this.getTargetByName(targetName);

        if (targetModel == null) {
            return targetModel;
        }

        switch(identityType) {
            case "MSISDN":
                identityService.putMSISDN(identityId, targetModel.getName(), targetModel.getMcngEtl());
                targetModel.addMsisdn(identityId);
                break;
            case "IMSI":
                identityService.putIMSI(identityId, targetModel.getName(), targetModel.getMcngEtl());
                targetModel.addImsi(identityId);
                break;
            case "IMEI":
                identityService.putIMEI(identityId, targetModel.getName(), targetModel.getMcngEtl());
                targetModel.addImei(identityId);
                break;
            default:
                identityService.putIdentity(identityType, identityId, targetModel.getName(), targetModel.getMcngEtl());
                targetModel.addOtherIdentities(identityType, Arrays.asList(identityId));
        }
        Map<String, Map<String, Object>> existingData = targetModel.getDataAsInsertModel();
        putTarget((Map) existingData);
        return null;
    }

    public TargetDetailsModel createTarget(ThreadLocalRandom threadLocalRandom,
                                            int msisdnCount,
                                            int imeiCount,
                                            int imsiCount,
                                            boolean save) {
        Map codeMap = ipfFirebaseController.getCountries()
                                           .block();
        List<String> codeKey = new ArrayList();
        codeMap.keySet().forEach(o -> {codeKey.add((String) o);});
        LocalDateTime localDateTime = LocalDateTime.now();

        Map<String, List<String>> msisdnImsiList = generateMsisdnImsi(codeMap, codeKey, msisdnCount, imsiCount, threadLocalRandom);

        List<String> msisdns = msisdnImsiList.get("MSISDN");
        List<String> imsis = msisdnImsiList.get("IMSI");
        List<String> imeis = generateIMEI(imeiCount);
        String targetName = generateNewTargetName(null, null);

        List<String> targets = new ArrayList();
        targets.add(targetName);
        String mcngId = null;
        if (save) {
            Map<String, String> targetsWithKey = mcngService.putTarget(targets);
            Set<String> mcngIds = targetsWithKey.keySet();
            List<String> aList = new ArrayList<String>(mcngIds);
            mcngId = aList.get(0);
        }
        else {
            localDateTime = localDateTime.plusSeconds(1);
            mcngId = localDateTime.format(dateTimeFormatter);
        }

        log.info("generated mcngId: {}", mcngId);

        TargetDetailsModel targetModel = new TargetDetailsModel(mcngId, targetName, msisdns, imsis, imeis);
        if (save) {
            IdentityCollection identityCollection = IdentityCollection.of(targetName, msisdns, imsis, imeis, mcngId);
            Map<String, Object> targetContainer = identityCollection.getTargetContainer();
            identityService.putMSISDN(msisdns, targetName, mcngId);
            identityService.putIMEI(imeis, targetName, mcngId);
            identityService.putIMSI(imsis, targetName, mcngId);
            identityService.refreshCache();
            this.putTarget(targetContainer);
        }

        return targetModel;
    }

    public Map<String, Object> generateRandomTargets(int numberOfRandomTargets) {
        Map codes = ipfFirebaseController.getCountries()
                                         .block();
        List<String> codeLs = new ArrayList();
        codes.keySet()
             .forEach(o -> {codeLs.add((String) o);});

        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        Map<String, Object> createdTargets = new HashMap();
        Map<String, Object> validCountry = new HashMap<>();
        for (int i = 0; i < numberOfRandomTargets; i++) {
            validCountry = getValidCountry(codes, codeLs, threadLocalRandom);
            createdTargets.putAll(createTarget(validCountry));
        }

        return createdTargets;
    }

    public Map<String, Object> getValidCountry(Map codes, List<String> codeLs, ThreadLocalRandom threadLocalRandom) {
        int randomNumber = Math.abs((threadLocalRandom.nextInt(codeLs.size()) - 1));
        String code = codeLs.get(randomNumber);
        Map<String, Object> data = (Map<String, Object>) codes.get(code);
        Map<String, Object> telcoOperatorCodes = (Map<String, Object>) data.get("telcoOperatorCode");

        if (telcoOperatorCodes == null) {
            data = getValidCountry(codes, codeLs, threadLocalRandom);
        }

        return data;
    }

    public Map<String, List<String>> generateMsisdnImsi(Map codeMap,
                                                        List<String> codeKey,
                                                        int msisdnCount,
                                                        int imsiCount,
                                                        ThreadLocalRandom threadLocalRandom) {
        List<String> msisdnList = new ArrayList<>();
        List<String> imsiList = new ArrayList<>();
        Map<String, Object> validCountry;
        List<String> callingCodes;
        Map<String, List<String>> identities = new HashMap<>();

        int count = Math.max(msisdnCount,imsiCount);

        for (int i = 0; i < count; i++) {
            validCountry = getValidCountry(codeMap, codeKey, threadLocalRandom);

            if (i <= msisdnCount){
                callingCodes = (List<String>) validCountry.get("callingCodes");
                String code = callingCodes.get(0);
                msisdnList.add(generateNewMSISDN(code));
                identities.put("MSISDN", msisdnList);
                log.info("Generated MSISDN: {}", msisdnList.size());
            }

            if (i <= imsiCount){
                Map<String, String> providerCodeMap = getRandomTelcoOperator(validCountry);
                imsiList.add(generateIMSI(providerCodeMap.get("mcc"), providerCodeMap.get("mnc")));

                log.info("Generated IMSI {}", imsiList.size());
                identities.put("IMSI", imsiList);
            }
        }
        return identities;
    }

    private List<String> generateIMEI(int count) {
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            ids.add(generateIMEI());
        }

        return ids;
    }

    public Map<String, Object> createTarget(Map<String, Object> validCountry) {
        List<String> codes = (List<String>) validCountry.get("callingCodes");
        String callingCode = codes.get(0);

        Map<String, String> providerCodeMap = getRandomTelcoOperator(validCountry);

        String msisdn = generateNewMSISDN(callingCode);
        String imsi = generateIMSI(providerCodeMap.get("mcc"), providerCodeMap.get("mnc"));
        String imei = generateIMEI();
        String targetName = generateNewTargetName(null, null);

        List<String> targets = new ArrayList<String>();
        targets.add(targetName);
        Map<String, String> targetsWithKey = mcngService.putTarget(targets);
        Set<String> mcngIdSet = targetsWithKey.keySet();
        List<String> aList = new ArrayList<>(mcngIdSet);
        String mcngId = aList.get(0);
        log.info("generated mcngId: {}", mcngId);

        IdentityCollection identityCollection = IdentityCollection.of(targetName, msisdn, imsi, imei, mcngId);
        identityService.putMSISDN(msisdn, targetName, mcngId);
        identityService.putIMEI(imei, targetName, mcngId);
        identityService.putIMSI(imsi, targetName, mcngId);
        Map<String, Object> targetContainer = identityCollection.getTargetContainer();
        this.putTarget(targetContainer);

        return targetContainer;
    }

    public Map<String, Object> createTarget(String country_code, String firstName, String lastName) {
        Map<String, Object> result = countryService.get(country_code);

        Map<String, Object> countryData = ((Map<String, Object>) result.get("message"));
        List<String> codes = (List<String>) countryData.get("callingCodes");
        String callingCode = codes.get(0);

        Map<String, String> providerCodeMap = getRandomTelcoOperator(countryData);

        String msisdn = generateNewMSISDN(callingCode);
        String imsi = generateIMSI(providerCodeMap.get("mcc"), providerCodeMap.get("mnc"));
        String imei = generateIMEI();
        String targetName = generateNewTargetName(firstName, lastName);

        List<String> targets = new ArrayList<>();
        targets.add(targetName);
        Map<String, String> targetsWithKey = mcngService.putTarget(targets);
        Set<String> mcngIdSet = targetsWithKey.keySet();
        List<String> aList = new ArrayList<>(mcngIdSet);
        String mcngId = aList.get(0);
        log.info("generated mcngId: {}", mcngId);

        IdentityCollection identityCollection = IdentityCollection.of(targetName, msisdn, imsi, imei, mcngId);
        identityService.putMSISDN(msisdn, targetName, mcngId);
        identityService.putIMEI(imei, targetName, mcngId);
        identityService.putIMSI(imsi, targetName, mcngId);
        identityService.refreshCache();
        Map<String, Object> targetContainer = identityCollection.getTargetContainer();
        this.putTarget(targetContainer);

        return targetContainer;
    }

    public Map<String, Object> putTarget(Map<String, Object> container) {
        Set<String> allKeys = container.keySet();

        log.info("data input: {}", allKeys);

        List<ProgressStatus> processStatuses = new ArrayList<>();

        StopWatch watch = new StopWatch();
        watch.start();

        for (String key : allKeys) {
            TargetModel targetModel = this.getTargetByName(key);
            Map<String, Object> newData = (Map<String, Object>) container.get(key);
            if (targetModel == null){
                ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.CREATING);

                log.info("{}", newData);
                CompletionListenerImpl completionListener = new CompletionListenerImpl(progressStatus);
                this.getDbReference()
                        .child(key)
                        .setValue(newData, completionListener);

                processStatuses.add(completionListener.getProgressStatus());
            } else {
                Map<String, Map<String, Object>> existingData = targetModel.getDataAsInsertModel();
                mergeData(existingData.get(key), newData);

                ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.CREATING);
                CompletionListenerImpl completionListener = new CompletionListenerImpl(progressStatus);
                this.getDbReference()
                        .child(key)
                        .setValue(existingData.get(key), completionListener);

                processStatuses.add(completionListener.getProgressStatus());
            }
        }

        for (ProgressStatus processStatus : processStatuses) {
            waitTillFinish(processStatus, 200l);
            if (processStatus.isFailed()) {
                Map<String, Object> failureResult = getFailureResult("Failed in firebase request!");
                log.error("Failed: [{}]", failureResult);
            }
        }

        watch.stop();

        log.info("Completed {} ms", watch.getTotalTimeMillis());
        return get(this.getDbReference());
    }
    private void mergeData(Map<String, Object> existingData, Map<String, Object> newData) {
        List<String> identites;
        for (String key : newData.keySet()) {
            switch (key) {
                case "mcng-etl-id":
                    existingData.put(key, newData.get(key));
                    break;
                case "MSISDN":
                case "IMSI":
                case "IMEI":
                default:
                    identites = existingData.get(key) == null? new ArrayList<>() : (List<String>) existingData.get(key);

                    if (identites.isEmpty()){
                        identites.addAll((List<String>) newData.get(key));
                    } else {
                        Set tempSet = new HashSet(identites);
                        tempSet.addAll((List<String>) newData.get(key));
                        identites = new ArrayList<>(tempSet);
                    }

                    existingData.put(key, identites);
                    break;
            }
        }
    }


    public Map<String, String> getRandomTelcoOperator(Map<String, Object> countryData) {
        Map<String, Object> telcoOperatorCodes = (Map<String, Object>) countryData.get("telcoOperatorCode");

        Random random = new Random();
        int randomOperator = random.nextInt(telcoOperatorCodes.size());
        String[] keys = telcoOperatorCodes.keySet()
                                          .toArray(new String[0]);
        return (Map<String, String>) telcoOperatorCodes.get(keys[randomOperator]);
    }

    /**
     * Lenght 15 charactors. 1-3 are country code.
     *
     * @param callingCode
     * @return
     */
    public String generateNewMSISDN(String callingCode) {
        final int officialCount = 15;
        String msisdnValue = callingCode;
        final int msisdnCount = officialCount - msisdnValue.length();
        Random random = new Random();
        for (int i = 0; i < msisdnCount; i++) {
            msisdnValue += random.nextInt(9);
        }

        log.info("Generated new MSISDN: {}", msisdnValue);

        // check in db
        IdentityModel identityModel = identityService.getMsisdn(msisdnValue);

        // If data exist generate new id.
        while (identityModel != null) {
            log.info("Generated new MSISDN {} is already being used.", msisdnValue);
            msisdnValue = generateNewMSISDN(callingCode);
            break;
        }

        log.info("Generated new MSISDN {} is unique.", msisdnValue);
        return msisdnValue;

    }

    private String generateIMSI(String mcc, String mnc) {
        final int officialCount = 15;
        String imsiValue = mcc + mnc;
        final int imsiCount = officialCount - imsiValue.length();
        Random random = new Random();
        for (int i = 0; i < imsiCount; i++) {
            imsiValue += random.nextInt(9);
        }

        log.info("Generated new IMSI: {}", imsiValue);

        // check in db
        IdentityModel imsi = identityService.getImsi(imsiValue);

        // If data exist generate new id.
        while (imsi != null) {
            log.info("Generated new IMSI {} is already being used.", imsiValue);
            imsiValue = generateIMSI(mcc, mnc);
            break;
        }

        log.info("Generated new IMSI {} is unique.", imsiValue);
        return imsiValue;
    }

    private String generateIMEI()  {
        String tac = "";
        try {
            tac = getRandomTAC(); // 8 digits
        } catch (Exception ex) {
            log.error("Failed to get TAC.", ex);
            System.exit(1);
        }
//        String snr = getRandomSNR(); // 6 digits
//        String cd = getRandomCD(); // single digit
//        String imei = tac + snr + cd;

        final int officialCount = 7;
        String imeiValue = tac;
        Random random = new Random();
        for (int i = 0; i < officialCount; i++) {
            imeiValue += random.nextInt(9);
        }

        log.info("Generated new IMEI: {}", imeiValue);

        // check in db
        IdentityModel imei = identityService.getImei(imeiValue);

        // If data exist generate new id.
        while (imei != null) {
            log.info("Generated new IMEI {} is already being used.", imeiValue);
            imeiValue = generateIMEI();
            break;
        }

        log.info("Generated new IMEI {} is unique.", imeiValue);
        return imeiValue;
    }

    public String getRandomTAC() throws Exception {
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        Map<String, Map<String, String>> map = this.imeiTypeAllocationCodeService.getAll();
        int randomTac = threadLocalRandom.nextInt(map.size());
        List<String> keys = new ArrayList<>(map.keySet());
        String key = keys.get(randomTac);
        log.info("TAC: {}, data: {}", key ,map.get(key));
        return key;
    }

    public String generateNewTargetName(String inputFirstName, String inputLastName) {
        String firstName = inputFirstName;
        if (firstName == null || firstName.isEmpty()) {
            firstName = faker.name()
                             .firstName();
        }

        String lastName = inputLastName;
        if (lastName == null || lastName.isEmpty()) {
            lastName = faker.name()
                            .lastName();
        }

        String fullName = firstName + " " + lastName;

        log.info("Generated new target name: {}", fullName);

        // check in db
        Map<String, Object> imeiMap = this.get(fullName);
        Map<String, Object> imeiData = (Map<String, Object>) imeiMap.get("message");

        // If data exist generate new id.
        while (imeiData != null) {
            log.info("Generated new full name [{}] is already being used.", fullName);
            fullName = generateNewTargetName(null, null);
            break;
        }

        log.info("Generated new full name [{}] is unique.", fullName);
        return fullName;
    }

    public List<String> getTargetNames() {
        Map<String, Object> keys = this.get();
        Map<String, Object> targetObjects = (Map<String, Object>) keys.get("message");
        return new ArrayList<>(targetObjects.keySet());
    }

    public List<String> getTargetsWithEqualIdentities() {
        return ipfFirebaseController.getTargetNamesWithEqualIdentities();
    }

    public Map<String, Object> get() {
        ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.FETCHING);
        ValueEventListener eventlistener = new ValueEventListenerImpl(progressStatus);
        this.getDbReference()
            .addListenerForSingleValueEvent(new ValueEventListenerImpl(progressStatus));

        waitTillFinish(progressStatus, 1000l);

        if (progressStatus.isFailed()) {
            return getFailureResult("Failed in firebase request!");
        }

        this.getDbReference()
            .removeEventListener(eventlistener);
        log.info("{}, in {} ms", progressStatus.getStatus(), progressStatus.getTimeTookInMs());
        return getSuccessResult(progressStatus.getValue());
    }
}
