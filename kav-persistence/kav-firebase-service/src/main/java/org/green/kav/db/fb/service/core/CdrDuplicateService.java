package org.green.kav.db.fb.service.core;

import org.green.kav.csv.model.CsvTypeModel;
import org.green.kav.csv.model.cdr.CdrCsvModel;
import org.green.kav.csv.service.CsvService;
import org.green.kav.csv.service.CsvTypeHandler;
import org.green.kav.csv.service.cdr.CdrCsvHandler;
import org.green.kav.db.fb.service.model.identity.TargetModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class CdrDuplicateService {
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private final String headers = "PROVIDER;EVENTTYPE;START;DURATION;STATUS;CGISTARTA;CGISTARTB;LATSTARTA;LATSTARTB;LONSTARTA;LONSTARTB;CGIENDA;CGIENDB;LATENDA;LATENDB;LONENDA;LONENDB;MSISDNA;MSISDNB;MSISDNC;IMEIA;IMEIB;IMSIA;IMSIB";
    private final String delimiter = ";";
    private final Random random = new Random();

    @Autowired
    private CsvService csvService;

    @Autowired
    private TargetService targetService;

    public void process(String cdrFileToDuplicate,
                        String newCdrFileOutputDirectory,
                        String outputFilePostFix,
                        int numberOfRecordForEachTarget,
                        List<String> targetNames) throws Exception {

        CsvTypeHandler csvTypeHandler = new CdrCsvHandler(headers, delimiter);
        List<CsvTypeModel> records = csvService.readCsv(cdrFileToDuplicate,
                                                        csvTypeHandler);

        List<TargetModel> targetModels = targetService.getTargetModels();

        List<TargetModel> targetModelsToUse = new ArrayList<>();
        for (TargetModel targetModel : targetModels) {
            if (targetNames.contains(targetModel.getName())) targetModelsToUse.add(targetModel);
        }

        int count = 0;
        for (TargetModel targetModel : targetModelsToUse) {
            List<CsvTypeModel> newRecords = new ArrayList<>();
            for (int i = 0; i < numberOfRecordForEachTarget; i++) {  // number of records
                for (CsvTypeModel csvTypeModel : records) {
                    newRecords.add(updateValues((CdrCsvModel) csvTypeModel, targetModel, count));
                }
                count++;
            }

            LocalDateTime localDateTime = LocalDateTime.now();
            csvService.writeToCsvFile(newCdrFileOutputDirectory,
                                      targetModel.getName() + "_" + localDateTime.format(dateFormatter) + outputFilePostFix,
                                      csvTypeHandler,
                                      newRecords);
        }
    }

    private CsvTypeModel updateValues(CdrCsvModel csvTypeModel, TargetModel targetModel, int count) {
        LocalDateTime localDateTime = LocalDateTime.parse(csvTypeModel.getStart(), dateFormatter);
        localDateTime = localDateTime.minusMinutes(29);
        int seconds = count + (random.nextInt(21) + 10);
        localDateTime = localDateTime.plusSeconds(seconds);
        String newTime = localDateTime.format(dateFormatter);

        CdrCsvModel clonedModel = csvTypeModel.deepClone();
        System.err.println(targetModel.getName() + " " + csvTypeModel.getStart() + " - 29m + " + seconds + "s -> " + newTime);
        clonedModel.setStart(newTime);
        clonedModel.setMsisdnA(targetModel.getMsisdList().get(0));
        clonedModel.setImeiA(targetModel.getImeiList().get(0));
        clonedModel.setImsiA(targetModel.getImsiList().get(0));
        clonedModel.setDuration(seconds + "");
        return clonedModel;
    }
}
