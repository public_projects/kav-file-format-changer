package org.green.kav.db.fb.service.core;

import com.google.firebase.database.DatabaseReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public class ImeiTypeAllocationCodeService extends AbstractFirebaseService {
    private static final Logger log = LoggerFactory.getLogger(ImeiTypeAllocationCodeService.class);
    private Map<String, Map<String, String>> cachedImeiTypeAllocation;

    @Autowired
    public ImeiTypeAllocationCodeService(DatabaseReference imeiTypeAllocationCodeDbReference) {
        super(imeiTypeAllocationCodeDbReference);
    }

    public Map<String, Map<String, String>> getAll() throws Exception {
        if (this.cachedImeiTypeAllocation == null) {
            Map<String,Object> returnData = this.get();

            if (!returnData.get("code").equals("200")){
                throw new Exception();
            }
            this.cachedImeiTypeAllocation = (Map<String, Map<String, String>>) returnData.get("message");
        }
        return this.cachedImeiTypeAllocation;
    }
}
