package org.green.kav.db.fb.service.core.report;

import org.javatuples.Quartet;
import org.javatuples.Triplet;
import org.javatuples.Tuple;

import java.io.File;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class FlatFileGeneratorReport {
    private String outputFileLocation;
    private AtomicLong totalCreatedCalls = new AtomicLong(0l);
    private Map<String, AreaRecordInfo> areaRecordInfo = new HashMap<>();
    private Map<String, Long> locationCount = new HashMap<>();
    private Map<Tuple, Long> callingCalledCount = new HashMap<>();
    private StringBuilder details = new StringBuilder();

    public FlatFileGeneratorReport(String input) {
        this.outputFileLocation = input;
    }

    public void addAreaCount(String areaName) {
        Long currentCount = locationCount.get(areaName);

        if (currentCount == null) {
            currentCount = 0l;
        }

        locationCount.put(areaName, currentCount += 1);
    }

    public void append(String details) {
        this.details.append(details);
    }

    public void addCallingCalledCount(String calling, String callerLocation, String called) {
        Triplet key = new Triplet(calling, callerLocation, called);
        Long currentCount = callingCalledCount.get(key);
        if (currentCount == null) currentCount = 0l;
        callingCalledCount.put(key, currentCount + 1);
    }

    public void addCallingCalledCount(String calling, String callerLocation, String called, String calledLocation) {
        Quartet key = new Quartet(calling, callerLocation, called, calledLocation);
        Long currentCount = callingCalledCount.get(key);
        if (currentCount == null) currentCount = 0l;
        callingCalledCount.put(key, currentCount + 1);
    }

    public long increment() {
        return totalCreatedCalls.incrementAndGet();
    }

    @Override
    public String toString() {
        return "FlatFileGeneratorCounter{"
                + "\noutputFileLocation='" + outputFileLocation + '\''
                + ", \ntotalCreatedCalls=" + totalCreatedCalls
                + ", \nlocationCount=" + toStringLocationCount()
                + ", \ncallingCalledCount=" + toStringCallingCalledCount()
                + ", \nareaRecordCount=" + toStringAreaRecordInfo()
                + ", \ndetails=" + details + '}';
    }

    private String toStringAreaRecordInfo() {
        StringBuilder sb = new StringBuilder(System.lineSeparator());
        areaRecordInfo.entrySet().forEach(entry -> {
            sb.append(entry.getKey() + " = " + entry.getValue()).append(System.lineSeparator());
        });
        return sb.toString();
    }

    private String toStringCallingCalledCount() {
        StringBuilder sb = new StringBuilder(System.lineSeparator());
        callingCalledCount.entrySet().forEach(entry -> {
            sb.append(entry.getKey() + " = " + entry.getValue()).append(System.lineSeparator());
        });
        return sb.toString();
    }

    private String toStringLocationCount() {
        StringBuilder sb = new StringBuilder(System.lineSeparator());
        locationCount.entrySet().forEach(entry -> {
            sb.append(entry.getKey() + " = " + entry.getValue()).append(System.lineSeparator());
        });
        return sb.toString();
    }


    public void flushOut() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")
                                                               .withZone(ZoneId.of("UTC"));
        LocalDateTime localDateTime = LocalDateTime.now();
        try (FileWriter fw = new FileWriter(new File(outputFileLocation + "//" + localDateTime.format(dateTimeFormatter) + ".log"))) {
            fw.write(this.toString());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setAreaRecordInfo(String areaName, AreaRecordInfo areaRecordInfo) {
        this.areaRecordInfo.put(areaName, areaRecordInfo);
    }

    public AreaRecordInfo getAreaRecordInfo(String areaName) {
        return this.areaRecordInfo.get(areaName);
    }
}