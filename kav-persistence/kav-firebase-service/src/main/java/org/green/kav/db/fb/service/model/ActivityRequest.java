package org.green.kav.db.fb.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.green.kav.common.bean.flat.model.SourceModel;
import java.io.Serializable;
import java.time.LocalDateTime;

public class ActivityRequest implements Serializable {
    private String targetPartyAName;
    private String targetPartyBName;
    private String areaName;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy HH:mm:ss")
    private LocalDateTime start;
    private String flatFileOutputLocation;
    private SourceModel source;

    public String getTargetPartyAName() {
        return targetPartyAName;
    }

    public void setTargetPartyAName(String targetPartyAName) {
        this.targetPartyAName = targetPartyAName;
    }

    public String getTargetPartyBName() {
        return targetPartyBName;
    }

    public void setTargetPartyBName(String targetPartyBName) {
        this.targetPartyBName = targetPartyBName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public String getFlatFileOutputLocation() {
        return flatFileOutputLocation;
    }

    public void setFlatFileOutputLocation(String flatFileOutputLocation) {
        this.flatFileOutputLocation = flatFileOutputLocation;
    }

    public SourceModel getSource() {
        return source;
    }

    public void setSource(SourceModel source) {
        this.source = source;
    }
}
