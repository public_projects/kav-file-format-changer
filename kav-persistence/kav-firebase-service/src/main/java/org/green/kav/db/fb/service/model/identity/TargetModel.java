package org.green.kav.db.fb.service.model.identity;

import java.util.*;

public class TargetModel {
    private String name;
    private String mcngEtl;
    private List<String> msisdList;
    private List<String> imsiList;
    private List<String> imeiList;

    private Map<String, List<String>> OtherIdentities;

    public TargetModel(String name, String mcngEtl, List<String> msisdList, List<String> imsiList, List<String> imeiList) {
        this.name = name;
        this.mcngEtl = mcngEtl;
        this.msisdList = msisdList;
        this.imsiList = imsiList;
        this.imeiList = imeiList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMcngEtl() {
        return mcngEtl;
    }

    public List<String> getMsisdList() {
        return msisdList;
    }
    public void addMsisdn(String msisdn) {
        this.msisdList.add(msisdn);
    }

    public List<String> getImsiList() {
        return imsiList;
    }

    public void addImsi(String imsi) {
        this.imsiList.add(imsi);
    }

    public List<String> getImeiList() {
        return imeiList;
    }

    public void addImei(String imei) {
        this.imeiList.add(imei);
    }

    public void addOtherIdentities(String identityType, List<String> identites){
        if (OtherIdentities == null){
            OtherIdentities = new HashMap<>();
            OtherIdentities.put(identityType, identites);
            return;
        }

        List<String> existingIdentities = OtherIdentities.get(identityType);

        if (existingIdentities == null) {
            existingIdentities = new ArrayList<>();
        }

        existingIdentities.addAll(identites);
        OtherIdentities.put(identityType, existingIdentities);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TargetModel)) return false;
        TargetModel that = (TargetModel) o;
        return getMcngEtl() == that.getMcngEtl() && Objects.equals(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getMcngEtl());
    }

    public Map<String, Map<String, Object>> getDataAsInsertModel(){
        Map<String, Map<String, Object>> container = new HashMap<>();
        Map<String, Object> subContainer = new HashMap<>();
        subContainer.put("MSISDN", this.getMsisdList());
        subContainer.put("IMSI", this.getImsiList());
        subContainer.put("IMEI", this.getImeiList());
        subContainer.put("mcng-etl-id", this.getMcngEtl());
        subContainer.putAll(OtherIdentities == null ? new HashMap<>() : OtherIdentities);

        container.put(this.getName(), subContainer);
        return container;
    }

    @Override
    public String toString() {
        return "TargetModel{" +
                "name='" + name + '\'' +
                ", mcngEtl='" + mcngEtl + '\'' +
                ", msisdList=" + msisdList +
                ", imsiList=" + imsiList +
                ", imeiList=" + imeiList +
                ", OtherIdentities=" + OtherIdentities +
                '}';
    }
}
