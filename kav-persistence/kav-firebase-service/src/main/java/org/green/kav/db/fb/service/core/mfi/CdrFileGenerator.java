package org.green.kav.db.fb.service.core.mfi;

import org.green.kav.db.fb.service.core.mfi.model.CdrModel;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

@Service
public class CdrFileGenerator extends BaseDRGenerator {
    public static final String CDR_FIELD_DELIMITER = ";";

    public List<CdrModel> readMfiCdrFile(String filePath) throws IOException {
        String line = "";
        String splitBy = ";";

        try (BufferedReader br = new BufferedReader(new FileReader(new File(filePath)))) {
            while ((line = br.readLine()) != null)   //returns a Boolean value
            {
                String[] employee = line.split(splitBy);    // use comma as separator
                System.out.println("Employee [First Name=" + employee[0] + ", Last Name=" + employee[1] + ", Designation=" + employee[2] + ", Contact=" + employee[3] + ", Salary= " + employee[4] + ", City= " + employee[5] + "]");
            }
        }

        return null;
    }

    @Override
    public String getHeader() {
        return "PROVIDER;EVENTTYPE;START;DURATION;STATUS;CGISTARTA;CGISTARTB;LATSTARTA;LATSTARTB;LONSTARTA;LONSTARTB;CGIENDA;CGIENDB;LATENDA;LATENDB;LONENDA;LONENDB;MSISDNA;MSISDNB;MSISDNC;IMEIA;IMEIB;IMSIA;IMSIB";
    }
}
