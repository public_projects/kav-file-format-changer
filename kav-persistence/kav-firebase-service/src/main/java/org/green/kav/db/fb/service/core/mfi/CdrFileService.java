package org.green.kav.db.fb.service.core.mfi;

import org.green.kav.common.bean.flat.model.SourceModel;
import org.green.kav.common.model.TargetDetailsModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

public class CdrFileService {
    private final static Logger log = LoggerFactory.getLogger(CdrFileService.class);
    public static final DateTimeFormatter dateFileNameformatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
                                                                                   .withZone(ZoneOffset.UTC);
    private Long fileSizeLimit;
    protected StringBuilder sb = new StringBuilder();
    protected BufferedWriter bufferedWriter;
    private AtomicLong recordCount = new AtomicLong(0l);
    private String currentFile;
    private File outputDirectory;
    private List<String> createdFiles = new ArrayList<>();
    public final String NEWLINE = System.getProperty("line.separator");
    private final String delimeter = ";";
    private final String fileNamePrefix = "cdr";

    public void initializeFileWithHeaderData(String data, String filePath) {
        File cdrFile = new File(filePath);
        this.currentFile = filePath;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(cdrFile));
            bufferedWriter.write(data);
            bufferedWriter.flush();
        }
        catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        }
//        log.info("Written to {}", jsonFile.getAbsolutePath());
    }

    public CdrFileService(String outputDir, Long fileSizeLimit) {
        this.fileSizeLimit = fileSizeLimit;
        this.outputDirectory = getOutputPath(new File(outputDir));
    }

    public final void writeStream(TargetDetailsModel partyA,
                                  TargetDetailsModel partyB,
                                  long callDuration,
                                  Instant timestamp,
                                  Map<String, String> area,
                                  ThreadLocalRandom random, SourceModel source) {
        onReachingFileMaxRecordCount();
        int partyASelection = random.nextInt(partyA.getMsisdn().size());
        int partyBSelection = random.nextInt(partyB.getMsisdn().size());
        List<String> partyAMsisdnList = new ArrayList<>(partyA.getMsisdn());
        initiateIfNoPreviousRecords(partyA.getName(), partyAMsisdnList.get(partyASelection));
//        String data = this.constructRecord(mcngId, partyA, partyB, callDuration, timestamp, area, partyASelection, partyBSelection, source);
    }

    public void onReachingFileMaxRecordCount() {
        boolean reached = fileSizeLimit == recordCount.get();
        if (reached) {
            this.recordCount = new AtomicLong(0l);
            this.close();
        }
    }

    public List<String> getCreatedFiles() {
        StringBuilder sb = new StringBuilder();
        this.createdFiles.forEach(s -> {
            sb.append(s).append(System.lineSeparator());
        });
        return this.createdFiles;
    }

    public void initiateIfNoPreviousRecords(String name, String msisdn) {
        if (this.getRecordCount().get() == 0l) {
            LocalDateTime localDateTime = LocalDateTime.now();
            localDateTime = localDateTime.plus(1, ChronoUnit.MILLIS);
            String newDirectory = outputDirectory.getAbsolutePath() +
                    System.getProperty("file.separator") + "cdr" + localDateTime.format(dateFileNameformatter);
            sb.append("[target: " + name + "][msisdn: " + msisdn + "][written to: " + newDirectory + "]").append(System.lineSeparator());
            File newOutputPath = new File(newDirectory);
            newOutputPath.mkdir();
            initializeFileWithHeaderData(getHeaders() +
                                                 NEWLINE, newDirectory +
                                                 System.getProperty("file.separator") +
                                                 fileNamePrefix + localDateTime.format(dateFileNameformatter));
        }
    }

    public AtomicLong getRecordCount() {
        return recordCount;
    }

    public String getHeaders() {
        return "PROVIDER;EVENTTYPE;START;DURATION;STATUS;CGISTARTA;CGISTARTB;LATSTARTA;LATSTARTB;LONSTARTA;LONSTARTB;CGIENDA;CGIENDB;LATENDA;LATENDB;LONENDA;LONENDB;MSISDNA;MSISDNB;MSISDNC;IMEIA;IMEIB;IMSIA;IMSIB";
    }

    private File getOutputPath(File outputDirectory) {
        File dir = new File(outputDirectory.getAbsolutePath());
        if (!dir.exists()) dir.mkdirs();
        return new File(dir.getAbsolutePath());
    }

    public void close() {
        try {
            bufferedWriter.flush();
            bufferedWriter.close();
        }
        catch (IOException io) {
            io.printStackTrace();
        }
    }
}
