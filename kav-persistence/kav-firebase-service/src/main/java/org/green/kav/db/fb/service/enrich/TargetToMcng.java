package org.green.kav.db.fb.service.enrich;

import org.green.kav.db.fb.service.core.McngService;
import org.green.kav.db.fb.service.core.TargetService;
import org.green.kav.db.fb.service.model.identity.TargetModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class TargetToMcng {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TargetService targetService;

    @Autowired
    private McngService mcngService;

    public void check() {
        List<TargetModel> targetModels = targetService.getTargetModels();

        Map<String, Object> targetMap = mcngService.get(mcngService.getDbReference().child("targetName"));

        for(int i = 0; i<targetModels.size(); i++){
            TargetModel targetModel = targetModels.get(i);
            String mcngEtl = targetModel.getMcngEtl();

            if (mcngEtl == null){
                logger.debug("target: {} don't have mcngId: {}", targetModel.getName(), mcngEtl);
            }
        }
    }
}
