package org.green.kav.db.fb.service.model;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class TargetIdentityRequest {
    private Integer targetCount;
    private Integer msisdnCount;
    private Integer imeiCount;
    private Integer imsiCount;
    private Boolean storeForFutureUse;
    private String outputDirectory;
    private File fileDirectory;

    public Integer getTargetCount() {
        return targetCount;
    }

    public void setTargetCount(Integer targetCount) {
        this.targetCount = targetCount;
    }

    public Integer getMsisdnCount() {
        return msisdnCount;
    }

    public void setMsisdnCount(Integer msisdnCount) {
        this.msisdnCount = msisdnCount;
    }

    public Integer getImeiCount() {
        return imeiCount;
    }

    public void setImeiCount(Integer imeiCount) {
        this.imeiCount = imeiCount;
    }

    public Integer getImsiCount() {
        return imsiCount;
    }

    public void setImsiCount(Integer imsiCount) {
        this.imsiCount = imsiCount;
    }

    public Boolean getStoreForFutureUse() {
        return storeForFutureUse;
    }

    public void setStoreForFutureUse(Boolean storeForFutureUse) {
        this.storeForFutureUse = storeForFutureUse;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    public File getFileDirectory() {
        return fileDirectory;
    }

    public void setFileDirectory(File fileDirectory) {
        this.fileDirectory = fileDirectory;
    }

    public File getFile() {
        if (fileDirectory == null){
            String folderName = LocalDateTime.now()
                                             .format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
            this.fileDirectory = new File(this.outputDirectory + File.separator + folderName);
        }
        return fileDirectory;
    }
}
