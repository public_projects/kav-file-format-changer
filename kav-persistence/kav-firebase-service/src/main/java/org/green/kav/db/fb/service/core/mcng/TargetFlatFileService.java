package org.green.kav.db.fb.service.core.mcng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.cassandra.services.core.McngIdService;
import org.green.kav.common.model.TargetDetailsModel;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;



public class TargetFlatFileService extends FlatFileService {
    public TargetFlatFileService() {
        super("P_TARGET_FLAT.csv");
    }

    public String toCsvString(TargetDetailsModel model) {
        LocalDateTime dateTime = LocalDateTime.now();
        LinkedList<String> csvRowCells = new LinkedList<>();
        csvRowCells.add(model.getMcngId());
        csvRowCells.add(model.getName());
        csvRowCells.add(dateTime.format(getMcngDateTimeFormat()));
        csvRowCells.add("mcng");
        csvRowCells.add("3");
        return String.join(this.getDelimeter(), csvRowCells)
                     .concat(NEWLINE);
    }

    @Override
    protected String getHeaders() {
        return "\"MCTARGETID\"|\"NAME\"|\"DATEID\"|\"SYSTEMID\"|\"AGENCYID\"";
    }
}
