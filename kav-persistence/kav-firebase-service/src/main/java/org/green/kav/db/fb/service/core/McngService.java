package org.green.kav.db.fb.service.core;

import com.google.cloud.firestore.CollectionReference;
import com.google.firebase.database.DatabaseReference;
import org.green.kav.common.model.TargetDetailsModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


@Service
public class McngService extends AbstractFirebaseService {
    private Logger log = LoggerFactory.getLogger(McngService.class);

    @Autowired
    private TargetService targetService;

    /**
     * Asia pacific firestore reference.
     */
    @Autowired
    private CollectionReference mcngCollectionRef;

    @Autowired
    public McngService(DatabaseReference mcngDbReference2) {
        super(mcngDbReference2);
    }

    public Map<String, String> putTarget(List<String> targets) {
        String id = "";

        Map<String, String> output = new HashMap<>();

        if (targets == null || targets.isEmpty()) {
            Map<String, Object> keys = this.targetService.get();
            Map<String, Object> targetObjects = (Map<String, Object>) keys.get("message");
            targets = new ArrayList<>(targetObjects.keySet());
        }

        for (String targetName : targets) {
            Map<String, Object> targetWithIdMap = targetService.get(targetName);
            Map<String, Object> idMap = (Map<String, Object>) targetWithIdMap.get("message");

            if (idMap != null) {
                String foundId = (String) idMap.get("mcng-etl-id");
                output.put(foundId, targetName);
                continue;
            }

            id = generateId(this.getDbReference());
            Map<String, Object> idData = new HashMap<>();
            idData.put("targetName", targetName);
            this.put(this.getDbReference(), id, idData);

            try {
                // The line below saved data in firestore
                mcngCollectionRef.document(id).set(Collections.singletonMap("targetName", targetName)).get();
                log.info("Mcng add {}, {}", id, targetName);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

            output.put(id, targetName);
        }
        return output;
    }

    public List<TargetDetailsModel> getTargetDataTree(List<String> targets) {
        Map<String, String> targetsWithKey = putTarget(targets);
        List<TargetDetailsModel> result = new ArrayList<>();
        for (String key : targetsWithKey.keySet()) {
            String targetName = targetsWithKey.get(key);
            log.info("Get target {} by mcng id {}", targetsWithKey.get(key), key);
            Map<String, Object> targetData = targetService.get(targetName);
            TargetDetailsModel targetDetailsModel = new TargetDetailsModel(key, targetName, targetData);
            result.add(targetDetailsModel);
        }

        return result;
    }

    /**
     * TODO: Use cache to get the data instead!
     *
     * @param mcngIdDbReference
     * @return
     */
    private String generateId(DatabaseReference mcngIdDbReference) {
        String newId = getRandomNumber(6);
        Map<String, Object> mcngIdMap = get(mcngIdDbReference, newId);
        Map<String, Object> mcngIdData = (Map<String, Object>) mcngIdMap.get("message");

        while (mcngIdData != null) {
            log.info("Generated new target id {} is already being used.", newId);
            newId = generateId(mcngIdDbReference);
            break;
        }

        log.info("Generated new target id {} is unique.", newId);
        return newId;
    }
}
