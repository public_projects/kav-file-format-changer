package org.green.kav.db.fb.service.core;

import org.green.kav.db.fb.service.core.mfi.CdrFileService;
import org.green.kav.db.fb.service.core.report.AreaRecordInfo;
import org.green.kav.db.fb.service.core.report.FlatFileGeneratorReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.cassandra.services.core.McngIdService;
import org.green.kav.common.bean.AreaContent;
import org.green.kav.common.bean.CallType;
import org.green.kav.common.bean.flat.model.SourceModel;
import org.green.kav.common.model.CallActivity;
import org.green.kav.common.model.IdentityCollection;
import org.green.kav.common.model.LocationDetail;
import org.green.kav.common.model.TargetDetailsModel;
import org.green.kav.common.model.request.McngBuildRequest;
import org.green.kav.common.model.response.McngBuildResponse;
import org.green.kav.db.fb.service.core.mcng.*;
import org.green.kav.fb.client.core.IpfFirebaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


@Service
public class FlatFileGenerator {
    private final static Logger log = LoggerFactory.getLogger(FlatFileGenerator.class);

    @Autowired
    private McngService mcngService;

    @Autowired
    private McngIdService mcngIdService;

    @Autowired
    private IpfFirebaseController ipfFirebaseController;

    public static Instant randomInstantBetween(Instant startInclusive, Instant endExclusive) {
        long startSeconds = startInclusive.getEpochSecond();
        long endSeconds = endExclusive.getEpochSecond();
        long random = ThreadLocalRandom.current()
                                       .nextLong(startSeconds, endSeconds);

        return Instant.ofEpochSecond(random);
    }

    public McngBuildResponse getIdentityDetailFlatFiles(McngBuildRequest mcngBuildRequest) {
        List<TargetDetailsModel> result = mcngService.getTargetDataTree(mcngBuildRequest.getTargets());

        for (TargetDetailsModel model: result) {
            String msisdn = model.getMsisdn().stream().findFirst().get();
            log.info("{}: msisd: {}", model.getName(), msisdn);
        }

        getIdentityDetailFlatFiles(result, new File(mcngBuildRequest.getFileDirectory().getAbsolutePath()));

        return new McngBuildResponse(result,
                                     mcngBuildRequest.getFileDirectory()
                                                     .getAbsolutePath());
    }

    public McngBuildResponse getIdentityDetailFlatFiles(List<TargetDetailsModel> result, File outputFile) {
        log.info("directory: {}",outputFile.getAbsolutePath());
        FlatFileService targetFlatFileService = new TargetFlatFileService();
        targetFlatFileService.write(result, outputFile);
        FlatFileService targetMsisdnFlatFileService = new TargetMsisdnFlatFileService();
        targetMsisdnFlatFileService.write(result, outputFile);
        FlatFileService targetImeiFlatFileService = new TargetImeiFlatFileService();
        targetImeiFlatFileService.write(result, outputFile);
        FlatFileService targetImsiFlatFileService = new TargetImsiFlatFileService();
        targetImsiFlatFileService.write(result, outputFile);
        return new McngBuildResponse(result, outputFile.getAbsolutePath());
    }

    public void generateCallActivity(List<CallActivity> callActivities, String flatFileOutputLocation, long maxRecordInFile)
            throws UnsupportedOperationException {
        FlatFileGeneratorReport report = new FlatFileGeneratorReport(flatFileOutputLocation);
        AtomicLong totalRecordMcngId = new AtomicLong(LocalDateTime.now()
                                                                   .toEpochSecond(ZoneOffset.UTC));

        TargetCallFlatFileService targetCallFlatFileService = new TargetCallFlatFileService(flatFileOutputLocation, maxRecordInFile);
        TargetLocFlatFileService targetLocFlatFileService = new TargetLocFlatFileService(flatFileOutputLocation, maxRecordInFile);
        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
        try {
            for (CallActivity callActivity : callActivities) {
                LocationDetail locationDetail = callActivity.getPartyAStartArea();
                LocalDateTime time = callActivity.getStartTime();

                Map<String, String> areaMap = new HashMap<>();
                areaMap.put("cgi", locationDetail.getCgi());
                areaMap.put("lat", locationDetail.getLatitude());
                areaMap.put("long", locationDetail.getLongitude());

                if (callActivity.getCallType()
                                .equals(CallType.SMS)) {
                    throw new UnsupportedOperationException("SMS not supported at the moment!");
                } else {
                    targetCallFlatFileService.writeStream(String.valueOf(totalRecordMcngId),
                                                          callActivity.getPartyA(),
                                                          callActivity.getPartyB(),
                                                          callActivity.getDuration(),
                                                          time.toInstant(ZoneOffset.UTC),
                                                          areaMap,
                                                          threadLocalRandom,
                                                          new SourceModel(callActivity.getAgencyId(), callActivity.getSystemId()));
                }

                targetLocFlatFileService.writeStream(String.valueOf(totalRecordMcngId),
                                                     callActivity.getPartyA(),
                                                     callActivity.getPartyB(),
                                                     callActivity.getDuration(),
                                                     time.toInstant(ZoneOffset.UTC),
                                                     areaMap,
                                                     threadLocalRandom,
                                                     new SourceModel(callActivity.getAgencyId(), callActivity.getSystemId()));

                report(report, callActivity, locationDetail);
                totalRecordMcngId.getAndIncrement();
            }
        }
        finally {
            targetCallFlatFileService.close();
            targetLocFlatFileService.close();

            report.append(targetCallFlatFileService.getDetailedInfo());
            report.append(targetLocFlatFileService.getDetailedInfo());
            report.flushOut();
        }
        log.info("Completed!");
    }

    private void report(FlatFileGeneratorReport report, CallActivity callActivity, LocationDetail locationDetail) {
        report.addCallingCalledCount(callActivity.getPartyA()
                                                 .getName(),
                                     callActivity.getPartyAStartArea()
                                                 .getAreaName(),
                                     callActivity.getPartyB()
                                                 .getName(),
                                     callActivity.getPartyBStartArea()
                                                 .getAreaName());

        report.addAreaCount(locationDetail.getAreaName());
        report.increment();
    }

    public void generateActivity(String partyA,
                                 String partyB,
                                 String areaName,
                                 LocalDateTime start,
                                 String flatFileOutputLocation,
                                 SourceModel source) throws Exception {
        AtomicLong totalRecordMcngId = new AtomicLong(LocalDateTime.now()
                                                                   .toEpochSecond(ZoneOffset.UTC));
        TargetCallFlatFileService targetCallFlatFileService = new TargetCallFlatFileService(flatFileOutputLocation, 1L);
        TargetLocFlatFileService targetLocFlatFileService = new TargetLocFlatFileService(flatFileOutputLocation, 1L);
        Instant instant = start.toInstant(ZoneOffset.UTC);
        ThreadLocalRandom random = ThreadLocalRandom.current();
        AreaContent areaContent = ipfFirebaseController.getAllAreas().get(areaName);

        TargetDetailsModel targetModelPartyA = getTargetByName(ipfFirebaseController.getTargetByName(partyA));
        TargetDetailsModel targetModelPartyB = getTargetByName(ipfFirebaseController.getTargetByName(partyB));

        for (IdentityCollection identity: ipfFirebaseController.getAllTargets()) {
            TargetDetailsModel targetDetailsModel = new TargetDetailsModel();
            targetDetailsModel.setName(identity.getTargetName());
            targetDetailsModel.setImei(identity.getImei());
            targetDetailsModel.setImsi(identity.getImsi());
            targetDetailsModel.setMsisdn(identity.getMsisdn());
            targetDetailsModel.setMcngId(identity.getMcngId());

            if (partyA.equalsIgnoreCase(identity.getTargetName())) {
                targetModelPartyA = targetDetailsModel;
            }

            if (partyB.equalsIgnoreCase(identity.getTargetName())) {
                targetModelPartyB = targetDetailsModel;
            }
        }

        Map<String, String> areaMap = new HashMap<>();
        areaMap.put("cgi", areaContent.getCgi());
        areaMap.put("lat", areaContent.getLat());
        areaMap.put("long", areaContent.getLongitude());

        targetCallFlatFileService.writeStream(String.valueOf(totalRecordMcngId), targetModelPartyA, targetModelPartyB, 10, instant, areaMap, random, source);
        targetLocFlatFileService.writeStream(String.valueOf(totalRecordMcngId), targetModelPartyA, targetModelPartyB, 10, instant, areaMap, random, source);
    }

    private TargetDetailsModel getTargetByName(IdentityCollection identity) throws Exception {
        TargetDetailsModel targetDetailsModel = new TargetDetailsModel();
        targetDetailsModel.setName(identity.getTargetName());
        targetDetailsModel.setImei(identity.getImei());
        targetDetailsModel.setImsi(identity.getImsi());
        targetDetailsModel.setMsisdn(identity.getMsisdn());
        targetDetailsModel.setMcngId(identity.getMcngId());
        return targetDetailsModel;
    }

    public void generateCallActivityRandomSecond(List<TargetDetailsModel> targetPartyA,
                                                List<TargetDetailsModel> targetPartyB,
                                                List<AreaContent> involvedAreas,
                                                String flatFileOutputLocation,
                                                long maxRecordInfile,
                                                int recordCount,
                                                int minSecDiffBetweenCalls,
                                                int maxSecDiffBetweenCalls,
                                                SourceModel source) {
        FlatFileGeneratorReport report = new FlatFileGeneratorReport(flatFileOutputLocation);
        AtomicLong totalRecordMcngId = new AtomicLong(LocalDateTime.now()
                                                                   .toEpochSecond(ZoneOffset.UTC));
        TargetCallFlatFileService targetCallFlatFileService = new TargetCallFlatFileService(flatFileOutputLocation, maxRecordInfile);
        TargetLocFlatFileService targetLocFlatFileService = new TargetLocFlatFileService(flatFileOutputLocation, maxRecordInfile);
        CdrFileService cdrFileService = new CdrFileService(flatFileOutputLocation, maxRecordInfile);

        involvedAreas = consolidateAreaContent(ipfFirebaseController.getAllAreas(), involvedAreas);
        ThreadLocalRandom random = ThreadLocalRandom.current();

//        try (FileWriter fw = new FileWriter(new File(
//                "D://opt//" + ActivityFlatFileService.dateFileNameformatter.format(LocalDateTime.now()) + ".log"))) {
        try {
            for (TargetDetailsModel partyA : targetPartyA) {

                log.error("partyA missing identities: \n msisdn: {}\n imsi: {}\n imei: {}", partyA.getMsisdn(), partyA.getImsi(), partyA.getImei());

                Map<String, AreaRecordInfo> areaRecordTime = new HashMap<>();
                for (AreaContent areaContent : involvedAreas) {
                    AtomicInteger count = new AtomicInteger(0);
                    LocalDateTime activityTime = areaContent.getFrom();
                    AreaRecordInfo areaRecordInfo = areaRecordTime.get(areaContent.getName());

                    while (count.intValue() != recordCount) {
                        TargetDetailsModel partyB = targetPartyB.get(random.nextInt(targetPartyB.size()));

                        if (partyB.equals(partyA)) {
                            if (targetPartyB.size() == 1){
                                break;
                            }
                            continue;
                        }

//                            fw.write(partyA.getName() + " -> " + partyB.getName() + " at " +
//                                     ActivityFlatFileService.dateFileNameformatter.format(time) + " in " + areaContent.getName() + System.lineSeparator());

                        Map<String, String> areaMap = new HashMap<>();
                        areaMap.put("cgi", areaContent.getCgi());
                        areaMap.put("lat", areaContent.getLat());
                        areaMap.put("long", areaContent.getLongitude());

                        Instant recordTime = activityTime.toInstant(ZoneOffset.UTC);
                        if (areaRecordInfo == null) areaRecordInfo = new AreaRecordInfo(recordTime);
                        areaRecordInfo.setEndTime(recordTime);
                        report.setAreaRecordInfo(areaContent.getName(), areaRecordInfo);

                        targetCallFlatFileService.writeStream(String.valueOf(totalRecordMcngId), partyA, partyB, 10, recordTime, areaMap, random, source);
                        targetLocFlatFileService.writeStream(String.valueOf(totalRecordMcngId), partyA, partyB, 10, recordTime, areaMap, random, source);
                        cdrFileService.writeStream(partyA, partyB, 10, recordTime, areaMap, random, source);

                        report.addCallingCalledCount(partyA.getName(), areaContent.getName(), partyB.getName());
                        report.addAreaCount(areaContent.getName());
                        report.increment();

                        // Time increased for the next activity.
                        activityTime = activityTime.plusSeconds(random.nextLong(minSecDiffBetweenCalls, maxSecDiffBetweenCalls));

                        count.incrementAndGet();
                        totalRecordMcngId.getAndIncrement();
                    }
                }
            }
        }
        finally {
            targetCallFlatFileService.close();
            targetLocFlatFileService.close();

            report.append(targetCallFlatFileService.getDetailedInfo());
            report.append(targetLocFlatFileService.getDetailedInfo());
            report.flushOut();
        }
        log.info("Completed!");
    }

    private List<AreaContent> consolidateAreaContent(Map<String, AreaContent> allAreas, List<AreaContent> involvedAreas) {
        List<AreaContent> result = new ArrayList<>();
        for (AreaContent involvedArea : involvedAreas) {
            result.add(allAreas.get(involvedArea.getName()));
        }

        return result;
    }

    private AreaContent getAreaContent(List<AreaContent> allAreas, String givenAreaName) {
        AreaContent involvedArea = new AreaContent();
        Collections.sort(allAreas);
            for (AreaContent fetchedArea : allAreas) {
                if (fetchedArea.getName()
                                .equals(givenAreaName)) {
                    involvedArea.setLongitude(fetchedArea.getLongitude());
                    involvedArea.setCgi(fetchedArea.getCgi());
                    involvedArea.setLat(fetchedArea.getLat());
                }
            }
        return involvedArea;
    }

    public void generateCallActivities(long callMadeByEachTarget,
                                       List<TargetDetailsModel> targetPartyA,
                                       List<TargetDetailsModel> targetPartyB,
                                       Map<String, Object> involvedAreas,
                                       LocalDateTime from,
                                       LocalDateTime to,
                                       String flatFileOutputLocation,
                                       long maxRecordInfile,
                                       boolean skipIdDuplicateCheck,
                                       SourceModel source) {
        FlatFileGeneratorReport flatFileGeneratorReport = new FlatFileGeneratorReport(flatFileOutputLocation);
        ThreadLocalRandom random = ThreadLocalRandom.current();

        AtomicLong totalRecordMcngId = new AtomicLong(LocalDateTime.now()
                                                                   .toEpochSecond(ZoneOffset.UTC));
        List<Long> existingMcngIds = new ArrayList<>();
        if (!skipIdDuplicateCheck) {
            existingMcngIds = mcngIdService.getAllMcngId();
        }

        List<String> keys = new ArrayList<>(involvedAreas.keySet());
        TargetCallFlatFileService targetCallFlatFileService = new TargetCallFlatFileService(flatFileOutputLocation, maxRecordInfile);
        TargetLocFlatFileService targetLocFlatFileService = new TargetLocFlatFileService(flatFileOutputLocation, maxRecordInfile);

        for (TargetDetailsModel partyA : targetPartyA) {
            flatFileGeneratorReport.append("Processing target: " + partyA.getName() + ", msisdn: " + partyA.getMsisdn() + System.lineSeparator());
            for (int i = 0; i < callMadeByEachTarget; i++) {
                Instant newDate = randomInstantBetween(from.toInstant(ZoneOffset.UTC), to.toInstant(ZoneOffset.UTC));

                String areaName = keys.get(random.nextInt(keys.size()));
                long mcngId = getUniqueMcngIdForCallActivity(totalRecordMcngId, existingMcngIds);
                TargetDetailsModel partyB = targetPartyB.get(random.nextInt(targetPartyB.size()));

                while (partyB.equals(partyA)) {
                    log.info("recepientTarget [{}] is same as caller[{}]", partyB.getName(), partyA.getName());
                    partyB = targetPartyB.get(random.nextInt(targetPartyB.size()));
                }

                long callDuration = random.nextLong(1000);
                flatFileGeneratorReport.addCallingCalledCount(partyA.getName(), areaName, partyB.getName());
                flatFileGeneratorReport.addAreaCount(areaName);
                flatFileGeneratorReport.increment();
//                log.info("{}; {}; {}; {}; {}", mcngId, callKey, areaName, callDuration, dateFileNameformatter.format(newDate));
                Map<String, String> areaMap = (Map<String, String>) involvedAreas.get(areaName);

                targetCallFlatFileService.writeStream(String.valueOf(mcngId), partyA, partyB, callDuration, newDate, areaMap, random, source);
                targetLocFlatFileService.writeStream(String.valueOf(mcngId), partyA, partyB, callDuration, newDate, areaMap, random, source);
            }
            totalRecordMcngId.getAndIncrement();
        }

        targetCallFlatFileService.close();
        targetLocFlatFileService.close();
        List<String> callFiles = targetCallFlatFileService.getCreatedFiles();
        List<String> locFiles = targetLocFlatFileService.getCreatedFiles();

        flatFileGeneratorReport.append(targetCallFlatFileService.getDetailedInfo());
        flatFileGeneratorReport.append(targetLocFlatFileService.getDetailedInfo());
        flatFileGeneratorReport.flushOut();
        log.info("Completed");
    }

    private long getUniqueMcngIdForCallActivity(AtomicLong totalRecordMcngId, List<Long> existingMcngIds) {
        while (existingMcngIds.contains(totalRecordMcngId.getAndIncrement())) {
            log.info("mcngId [{}] already used!", totalRecordMcngId.get());
        }

        return totalRecordMcngId.get();
    }
}
