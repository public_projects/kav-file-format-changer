package org.green.kav.db.fb.service.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.db.fb.service.model.identity.AreaModel;
import org.green.kav.db.fb.service.model.identity.TargetModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class ProximityAnalysisService {
    private Logger log = LoggerFactory.getLogger(ProximityAnalysisService.class);

    @Autowired
    private TargetService targetService;

    @Autowired
    private AreaService areaService;

    @Async
    public void generate(int totalRecords, long timeInMillis) throws IOException {
        long start = System.currentTimeMillis();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        Random random = new Random();

        List<TargetModel> targetModels = this.targetService.getTargetModels();
        log.info("Target size: {}", targetModels.size());

        List<AreaModel> areaModels = this.areaService.getAreaModels();
        log.info("Area size: {}", areaModels.size());

        LocalDateTime callTime =  LocalDateTime.ofInstant(Instant.ofEpochMilli(timeInMillis), ZoneId.systemDefault());

        List<String> recordData = new ArrayList<>(totalRecords);
        for (int i = 0; i < totalRecords; i++) {
            String identity = getRandomIdentityStr(targetModels, random);
            String coordinateStr = getRandomCoordinateStr(areaModels, random);
            callTime = callTime.plusSeconds(5l);
            String formattedDate = dateFormatter.format(callTime);
            recordData.add(identity+";"+coordinateStr+";"+formattedDate);
        }

        FileWriter writer = new FileWriter(UUID.randomUUID() + ".csv");
        writer.write("IDENTITY;LATITUDE;LONGITUDE;TIME" + System.lineSeparator());
        for(String str: recordData) {
            writer.write(str + System.lineSeparator());
        }
        writer.close();
        log.info("Completed in: {} ms", System.currentTimeMillis() - start);
    }

    private String getRandomCoordinateStr(List<AreaModel> areaModels, Random random) {
        double[] sampleData = {0.001, 0.002, 0.003, 0.004, 0.005};

        String latLongStr;
        AreaModel areaModel = areaModels.get(random.nextInt(areaModels.size()));
        double newlatitude = getNewValue(sampleData, random, areaModel.getLatitude());
        double newLongitude = getNewValue(sampleData, random, areaModel.getLongitude());
        log.debug("latitude: {}, newlatitude {}, longitude: {}, newLongitude: {}",
                  areaModel.getLatitude(),
                  newlatitude,
                  areaModel.getLongitude(),
                  newLongitude);

        latLongStr = newlatitude + ";" + newLongitude;
        return latLongStr;
    }

    private String getRandomIdentityStr(List<TargetModel> targetModels, Random random){
        TargetModel targetModel = targetModels.get(random.nextInt(targetModels.size()));
        return targetModel.getName().replaceAll(" ", "_").replaceAll("'", "_");
    }

    private double getNewValue(double[] sampleData, Random random, double coordinate) {
        double randomValue = sampleData[random.nextInt(sampleData.length)];
        log.debug("randomValue: {}", randomValue);
        double newValue = 0;
        if (random.nextBoolean()) {
            newValue = coordinate - randomValue;
        } else {
            newValue = coordinate + randomValue;
        }
        return newValue;
    }
}
