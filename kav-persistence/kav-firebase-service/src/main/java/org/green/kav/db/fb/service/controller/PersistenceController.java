package org.green.kav.db.fb.service.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.green.kav.common.model.TargetDetailsModel;
import org.green.kav.common.model.request.AreaRequest;
import org.green.kav.common.model.request.CallActivityRequest;
import org.green.kav.common.model.request.McngBuildRequest;
import org.green.kav.common.model.response.McngBuildResponse;
import org.green.kav.db.fb.service.core.CdrDuplicateService;
import org.green.kav.db.fb.service.enrich.TargetToMcng;
import org.green.kav.db.fb.service.model.*;
import org.green.kav.db.fb.service.core.AreaService;
import org.green.kav.db.fb.service.core.CgiService;
import org.green.kav.db.fb.service.core.CountryService;
import org.green.kav.db.fb.service.core.FlatFileGenerator;
import org.green.kav.db.fb.service.core.IdentityService;
import org.green.kav.db.fb.service.core.McngFlatFileGeneratorJob;
import org.green.kav.db.fb.service.core.ProximityAnalysisService;
import org.green.kav.db.fb.service.core.TargetService;
import org.green.kav.db.fb.service.model.identity.IdentityModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PersistenceController {
    private static final Logger log = LoggerFactory.getLogger(PersistenceController.class);
    public static final String COUNTRIES_URL = "/countries";
    public static final String COUNTRIES_SHORT_URL = "/countries/short";
    public static final String COUNTRY_URL = "/country/{code}";
    public static final String TARGET_URL = "/target/{code}";
    public static final String CREATE_TARGET_URL = "/target/generate";
    public static final String TARGET = "/target";
    public static final String TARGET_BY_IDENTITY_COUNT = "/target/by/identity/count";
    public static final String IDENTITIES_URL = "/identities";
    public static final String IDENTITY_URL = "/identity";
    public static final String IDENTITY_MSISDN_CODE_URL = "/identity/MSISDN/{code}";
    public static final String IDENTITY_IMSI_CODE_URL = "/identity/IMSI/{code}";
    public static final String IDENTITY_IMEI_CODE_URL = "/identity/IMEI/{code}";
    public static final String CREATE_MCNG_URL = "/create/mcng";
    public static final String GENERATE_MCNG_TARGET = "/generate/mcng/target/by/identity/count";
    public static final String GENERATE_CDR_URL = "/generate/cdr";
    public static final String TARGET_NAMES = "/target/names";
    public static final String CREATE_AREA_URL = "/create/area";
    public static final String CREATE_CSV_FROM_JSON = "/json/to/csv";
    public static final String GENERATE_CALL_ACTIVITY_2 = "/generate/mcng/call/activity2";
    public static final String ENRICH_AREA_CGI = "/enrich/area_to_cgi";
    public static final String GENERATE_SINGLE_ACTIVITY = "/generate/single/activity";
    public static final String GET_TARGET_SAME_IDENTITY_COUNT = "/target/name/sameIdentityCount";

    public static final String ASSOCIATE_IDENTITY = "/associate/identity";
    public static final String DUPLICATE_CDR = "/duplicate/cdr";

    @Autowired
    private AreaService areaService;

    @Autowired
    private CgiService cgiService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private TargetService targetService;

    @Autowired
    private ProximityAnalysisService proximityAnalysisService;

    @Autowired
    private IdentityService identityService;

    @Autowired
    private FlatFileGenerator flatFileGenerator;

    @Autowired
    private McngFlatFileGeneratorJob mcngFlatFileGeneratorJob;

    @Autowired
    private TargetToMcng targetToMcng;

    @Autowired
    private CdrDuplicateService cdrDuplicateService;

    @CrossOrigin
    @PostMapping(value=ENRICH_AREA_CGI)
    public void enrichAreaCgi() {
        this.areaService.enrichCgi();
    }

    @CrossOrigin
    @PostMapping(value=ASSOCIATE_IDENTITY)
    public ResponseEntity<Map<String, String>> associateIdentity(@RequestBody IdentityAssociationRequest request) throws Exception {

        Map<String, String> response = new HashMap<>();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @CrossOrigin
    @PostMapping(value=DUPLICATE_CDR)
    public ResponseEntity<Map<String, String>> duplicateCdr(@RequestBody DuplicateCdrRequest request) throws Exception {
        cdrDuplicateService.process(request.getCdrFileToDuplicate(),
                                    request.getNewCdrFileOutputDirectory(),
                                    request.getOutputFilePostFix(),
                                    request.getNumberOfRecordForEachTarget(),
                                    request.getTargetNames());

        Map<String, String> response = new HashMap<>();
        response.put("Generated results in: ", request.getNewCdrFileOutputDirectory());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(GET_TARGET_SAME_IDENTITY_COUNT)
    public ResponseEntity<List<String>> getTargetWithSameIdentityCount(){
        return new ResponseEntity<>(this.targetService.getTargetsWithEqualIdentities(), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping(value=GENERATE_SINGLE_ACTIVITY)
    public ResponseEntity<Map<String, String>>  generateSingle(@RequestBody ActivityRequest request) throws Exception {
        this.flatFileGenerator.generateActivity(request.getTargetPartyAName(),
                                                request.getTargetPartyAName(),
                                                request.getAreaName(),
                                                request.getStart(),
                                                request.getFlatFileOutputLocation(),
                                                request.getSource());
        Map<String, String> response = new HashMap<>();
        response.put("message", "Request is under process");
        response.put("result will be put in: ", request.getFlatFileOutputLocation());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/generate/mcng/call/activity", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> asyncGenerateMcngData(@RequestBody McngCdrLocRequest mcngCdrLocRequest) throws Exception {

        mcngFlatFileGeneratorJob.run(mcngCdrLocRequest.getFlatFileOutputLocation(),
                                     mcngCdrLocRequest.getTargetPartyANames(),
                                     mcngCdrLocRequest.getTargetPartyBNames(),
                                     mcngCdrLocRequest.getFrom(),
                                     mcngCdrLocRequest.getTo(),
                                     mcngCdrLocRequest.getMaxRecordInFlatFile(),
                                     mcngCdrLocRequest.getCallCountByEachTarget(),
                                     mcngCdrLocRequest.getSource());
        Map<String, String> response = new HashMap<>();
        response.put("message", "Request is under process");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = GENERATE_CALL_ACTIVITY_2, method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> asyncGenerateMcngData2(@RequestBody McngCdrLocRequest mcngCdrLocRequest) throws Exception {
        Map<String, Object> response = new HashMap<>();
        CallActivityGeneratorResponse callActivityGeneratorResponse = mcngFlatFileGeneratorJob.callGenerator(mcngCdrLocRequest.getTargetPartyANames(),
                                                                                                             mcngCdrLocRequest.getTargetPartyBNames(),
                                                                                                             mcngCdrLocRequest.getAreaContents(),
                                                                                                             mcngCdrLocRequest.getMaxRecordInFlatFile(),
                                                                                                             mcngCdrLocRequest.getFlatFileOutputLocation(),
                                                                                                             mcngCdrLocRequest.getMaxRecordsPerTargetA(),
                                                                                                             mcngCdrLocRequest.getMinSecDiffBetweenCalls(),
                                                                                                             mcngCdrLocRequest.getMaxSecDiffBetweenCalls(),
                                                                                                             mcngCdrLocRequest.getSource());

        response.put("message", "Request is under process");
        response.put("request", callActivityGeneratorResponse);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping(value = "/generate/mcng/etl/activity", method = RequestMethod.POST)
    public ResponseEntity<Map<String, String>> asyncGenerateMcngData3(@RequestBody CallActivityRequest callActivityRequest)  {
        mcngFlatFileGeneratorJob.run3(callActivityRequest);
        Map<String, String> response = new HashMap<>();
        response.put("message", "Request is under process");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping(value = TARGET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> deleteTarget(@RequestBody Map<String, String> targetRequest) {
        StopWatch watch = new StopWatch();
        watch.start();

        String fullName = targetRequest.get("fullName");

        if (fullName == null) {
            Map<String, Object> responseData = new HashMap<>();
            responseData.put("message", "Required parameter [fullName] is missing!");
            return ResponseEntity.badRequest()
                                 .body(responseData);
        }

        Map<String, Object> result = targetService.deleteTarget(fullName);
        watch.stop();
        log.info("Completed [{}] within {} ms", TARGET, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @DeleteMapping(value = IDENTITY_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> deleteIdentity(@RequestBody Map<String, String> targetRequest) {
        StopWatch watch = new StopWatch();
        watch.start();

        String type = targetRequest.get("type");
        String code = targetRequest.get("identity");

        if (code == null) {
            Map<String, Object> responseData = new HashMap<>();
            responseData.put("message", "Required parameter [fullName] is missing!");
            return ResponseEntity.badRequest()
                                 .body(responseData);
        }

        Map<String, Object> result = identityService.deleteIdentity(type, code);
        watch.stop();
        log.info("Completed [{}] within {} ms", TARGET, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @PutMapping(value = TARGET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> createTargetWithIdentities(@RequestBody TargetRequest targetRequest) {
        StopWatch watch = new StopWatch();
        watch.start();

        Map<String, Object> result = targetService.createByIdentities(targetRequest.getFirstName(),
                                                                      targetRequest.getLastName(),
                                                                      targetRequest.getMsisdn(),
                                                                      targetRequest.getImei(),
                                                                      targetRequest.getImsi());
        watch.stop();
        log.info("Completed [{}] within {} ms", TARGET, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @PutMapping(value = "random/targets/{count}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> createTargetWithIdentities(@PathVariable(name = "count") int count) {
        StopWatch watch = new StopWatch();
        watch.start();

        Map<String, Object> result = targetService.generateRandomTargets(count);
        watch.stop();
        log.info("Completed [{}] within {} ms", "random/targets/"+count, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = "random/proximity/analysis/{dataCount}/{epochTime}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createRandomProximityAnalysisData(@PathVariable(name = "dataCount") int dataCount,
                                                            @PathVariable(name = "epochTime") long epochTime) {
        StopWatch watch = new StopWatch();
        watch.start();
        try {
            proximityAnalysisService.generate(dataCount, epochTime);
        } catch (IOException e) {
            return ResponseEntity.badRequest().body(e);
        }
        watch.stop();
        log.info("Completed {} within {} ms", COUNTRIES_SHORT_URL, watch.getTotalTimeMillis());
        return ResponseEntity.ok("Started async processing...");
    }


    @CrossOrigin
    @GetMapping(value = COUNTRIES_SHORT_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CountryService.ShortCountryCode> getCountriesShort() {
        StopWatch watch = new StopWatch();
        watch.start();

        CountryService.ShortCountryCode result = countryService.getShortCountryCodes();
        watch.stop();
        log.info("Completed {} within {} ms", COUNTRIES_SHORT_URL, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = TARGET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<CountryService.ShortCountryCode> getTargetByName() {
        StopWatch watch = new StopWatch();
        watch.start();

        CountryService.ShortCountryCode result = countryService.getShortCountryCodes();
        watch.stop();
        log.info("Completed {} within {} ms", TARGET, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = IDENTITIES_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getAIdentities() {
        StopWatch watch = new StopWatch();
        watch.start();

//        Map<String, Object> result = identityService.get();
        Map<String, Object> result = null;
//
//        identityService.getAll();
//        cgiService.removeByDbRef(cgiService.getDbReference().child("7"));
//        cgiService.removeByDbRef(cgiService.getDbReference().child("aq"));

        targetToMcng.check();

        watch.stop();
        log.info("Completed {} within {} ms", IDENTITY_URL, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = IDENTITY_IMEI_CODE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IdentityModel> getIdentityWthImei(@PathVariable(name = "code") String code) {
        StopWatch watch = new StopWatch();
        watch.start();

        IdentityModel result = identityService.getImei(code);
        watch.stop();
        log.info("Completed [{}] with [code = {}], within {} ms", IDENTITY_IMSI_CODE_URL, code, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = TARGET_NAMES, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> getTargetName() {
        StopWatch watch = new StopWatch();
        watch.start();

        List<String> result = targetService.getTargetNames();
        watch.stop();
        log.info("Completed [{}], within {} ms", TARGET_NAMES, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = IDENTITY_IMSI_CODE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IdentityModel> getIdentityWthImsi(@PathVariable(name = "code") String code) {
        StopWatch watch = new StopWatch();
        watch.start();
        IdentityModel identityModel = identityService.getImsi(code);
        watch.stop();
        log.info("Completed [{}] with [code = {}], within {} ms", IDENTITY_IMSI_CODE_URL, code, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(identityModel);
    }

    @CrossOrigin
    @GetMapping(value = IDENTITY_MSISDN_CODE_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IdentityModel> getIdentityWthMsisdn(@PathVariable(name = "code") String code) {
        StopWatch watch = new StopWatch();
        watch.start();

        IdentityModel identityModel = identityService.getMsisdn(code);
        watch.stop();
        log.info("Completed [{}] with [code = {}], within {} ms", IDENTITY_MSISDN_CODE_URL, code, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(identityModel);
    }

    @CrossOrigin
    @GetMapping(value = COUNTRIES_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getAllCountries() {
        StopWatch watch = new StopWatch();
        watch.start();

        Map<String, Object> result = countryService.get();
        watch.stop();
        log.info("Completed {} within {} ms", COUNTRIES_URL, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = COUNTRY_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getAllCountryByCode(@PathVariable(name = "code") String code) {
        StopWatch watch = new StopWatch();
        watch.start();

        Map<String, Object> result = countryService.get(code);
        watch.stop();
        log.info("Completed [{}] with [code = {}], within {} ms", COUNTRIES_URL, code, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @GetMapping(value = TARGET_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> getTargetByCode(@PathVariable(name = "code") String code) {
        StopWatch watch = new StopWatch();
        watch.start();

        Map<String, Object> result = targetService.get(code);
        watch.stop();
        log.info("Completed [{}] with [code = {}], within {} ms", TARGET_URL, code, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @PutMapping(value = CREATE_TARGET_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, Object>> createTargetByCode(@RequestBody Map<String, String> requestTargetBody) {
        StopWatch watch = new StopWatch();
        watch.start();

        String code = requestTargetBody.get("countryCode");
        String firstName = requestTargetBody.get("firstName");
        String lastName = requestTargetBody.get("lastName");

        Map<String, Object> result = targetService.createTarget(code, firstName, lastName);
        watch.stop();
        log.info("Completed [{}] with [code = {}], within {} ms", CREATE_TARGET_URL, code, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(result);
    }

    @CrossOrigin
    @PutMapping(value = TARGET_BY_IDENTITY_COUNT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TargetDetailsModel>> generateByIdentities(@RequestBody TargetIdentityRequest requestTargetBody) {
        StopWatch watch = new StopWatch();
        watch.start();

        List<TargetDetailsModel> results = targetService.generateRandomTargets(
                requestTargetBody.getTargetCount(),
                requestTargetBody.getMsisdnCount(),
                requestTargetBody.getImeiCount(),
                requestTargetBody.getImsiCount(),
                requestTargetBody.getStoreForFutureUse());
        watch.stop();

        log.info("Completed [{}], within {} ms", TARGET_BY_IDENTITY_COUNT, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(results);
    }

    @CrossOrigin
    @PutMapping(value = GENERATE_MCNG_TARGET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<McngBuildResponse> generateMcngTarget(@RequestBody TargetIdentityRequest requestTargetBody){
        StopWatch watch = new StopWatch();
        watch.start();

        List<TargetDetailsModel> result = targetService.generateRandomTargets(
                requestTargetBody.getTargetCount(),
                requestTargetBody.getMsisdnCount(),
                requestTargetBody.getImeiCount(),
                requestTargetBody.getImsiCount(),
                requestTargetBody.getStoreForFutureUse());
        watch.stop();

        McngBuildResponse mcngBuildResponse = flatFileGenerator.getIdentityDetailFlatFiles(result, requestTargetBody.getFile());
        log.info("Completed [{}], within {} ms", GENERATE_MCNG_TARGET, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(mcngBuildResponse);
    }

    @CacheEvict(value="areas",  allEntries = true)
    @PostMapping(value = CREATE_AREA_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Map<String, Object>>> createArea(@RequestBody List<AreaRequest> areaRequests) {
        StopWatch watch = new StopWatch();
        watch.start();
        List<Map<String, Object>> resultAreas = areaService.put(areaRequests);
        List<Map<String, Object>> resultCgis = cgiService.put(areaRequests);
        resultAreas.addAll(resultCgis);
        watch.stop();
        log.info("Completed [{}] with [record size = {}], within {} ms", CREATE_AREA_URL, areaRequests.size(), watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(resultAreas);
    }

    @PostMapping(value = GENERATE_CDR_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity generateCDR(@RequestBody List<Object> cdrRequest) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writerWithDefaultPrettyPrinter()
                                  .writeValueAsString(cdrRequest);
        log.info("input data: {}", json);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = CREATE_MCNG_URL, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<McngBuildResponse> createMcng(@RequestBody McngBuildRequest mcngBuildRequest) {
        StopWatch watch = new StopWatch();
        watch.start();
        watch.prettyPrint();
        McngBuildResponse mcngBuildResponse = flatFileGenerator.getIdentityDetailFlatFiles(mcngBuildRequest);
        watch.stop();
        log.info("Completed [{}]  within {} ms", CREATE_MCNG_URL, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(mcngBuildResponse);
    }

    @PostMapping(value = CREATE_CSV_FROM_JSON, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> createArea(@RequestBody String json) throws Exception {
        StopWatch watch = new StopWatch();
        watch.start();
        JsonNode jsonTree = new ObjectMapper().readTree(json);
        CsvSchema.Builder csvSchemaBuilder = CsvSchema.builder();
        JsonNode firstObject = jsonTree.elements()
                                       .next();
        firstObject.fieldNames()
                   .forEachRemaining(csvSchemaBuilder::addColumn);
        CsvSchema csvSchema = csvSchemaBuilder.build()
                                              .withHeader()
                                              .withColumnSeparator(';')
                                              .withColumnSeparator(';')
                                              .withoutQuoteChar();

        CsvMapper csvMapper = new CsvMapper();
        String csvStr = csvMapper.writerFor(JsonNode.class)
                                 .with(csvSchema)
                                 .writeValueAsString(jsonTree);
        watch.stop();
        Map<String, String> data = new HashMap<>();
        data.put("response", csvStr);
        log.info("Completed [{}], within {} ms", CREATE_CSV_FROM_JSON, watch.getTotalTimeMillis());
        return ResponseEntity.ok()
                             .body(data);
    }
}
