package org.green.kav.db.fb.service.model.identity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class IdentityModel {
    @JsonProperty("IMSI")
    private List<String> IMSI = new ArrayList<>();
    @JsonProperty("MSISDN")
    private List<String> MSISDN = new ArrayList<>();
    @JsonProperty("IMEI")
    private List<String> IMEI = new ArrayList<>();
    @JsonProperty("targets")
    private Map<String, Map<String, String>> targets;

    public IdentityModel(){
    }

    public List<String> getIMSI() {
        return IMSI;
    }

    public void setIMSI(List<String> IMSI) {
        this.IMSI = IMSI;
    }

    public List<String> getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(List<String> MSISDN) {
        this.MSISDN = MSISDN;
    }

    public List<String> getIMEI() {
        return IMEI;
    }

    public void setIMEI(List<String> IMEI) {
        this.IMEI = IMEI;
    }

    public Map<String, Map<String, String>> getTargets() {
        return targets;
    }

    public void setTargets(Map<String, Map<String, String>> targets) {
        this.targets = targets;
    }

    @Override
    public String toString() {
        return "IdentityModel{" + "IMSI=" + IMSI + ", MSISDN=" + MSISDN + ", IMEI=" + IMEI + ", targets='" + targets + '\'' + '}';
    }
}
