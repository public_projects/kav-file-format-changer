package org.green.kav.db.fb.service.core.mcng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.common.bean.flat.model.SourceModel;
import org.green.kav.common.model.TargetDetailsModel;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;



public class TargetCallFlatFileService extends ActivityFlatFileService {

    public TargetCallFlatFileService(String outputDir, Long fileSizeLimit) {
        super("P_CALL_FLAT.csv", fileSizeLimit, outputDir);
    }

    public String constructRecord(String mcngId,
                                  String partyAMcngId,
                                  String msisdnA,
                                  String imsiA,
                                  String imeiA,
                                  String msisdnB,
                                  long callDuration,
                                  Instant timestamp,
                                  Map<String, String> area,
                                  SourceModel source) {
        LocalDateTime ldt = LocalDateTime.ofInstant(timestamp, ZoneOffset.UTC);
        LinkedList<String> csvRowCells = new LinkedList<>();
        csvRowCells.add(mcngId); // SESSIONLOG_MCINTERCEPTID
        csvRowCells.add(msisdnA); // SES_PHONE_DIR1_IDENTITY_VALUE
        csvRowCells.add(imsiA); // SES_IMSI_IDENTITY_VALUE
        csvRowCells.add(imeiA); // SES_IMEI_IDENTITY_VALUE
        csvRowCells.add(msisdnB); // SES_PHONE_DIR2_IDENTITY_VALUE
        csvRowCells.add(this.dateFileNameformatter.format(timestamp)); // SES_PHONE_DIR1_STARTDATE
        csvRowCells.add(String.valueOf(callDuration)); // SES_PHONE_DIR1_DURATION
        csvRowCells.add("1"); // SES_PHONE_DIR1_DURATION
        csvRowCells.add("1"); // SES_PHONE_DIR1_INID_DIRECTION
        csvRowCells.add(partyAMcngId); // SES_PHONE_DIR1_TRIG_TARGETID
        csvRowCells.add("1"); // SES_PHONE_DIR1_INID_MARKEDTYPE
        csvRowCells.add("0"); // SES_IMSI_INID_MARKEDTYPE
        csvRowCells.add("0"); // SES_IMEI_INID_MARKEDTYPE
        csvRowCells.add("0"); // SES_PHONE_DIR2_INID_MARKEDTYPE
        csvRowCells.add(mcngId); // SES_PHONE_DIR1_TRIG_TRIGGERID
        csvRowCells.add(source.getAgentId()); // SES_PHONE_DIR1_INID_MCAGENCYID, e.g: 3
        csvRowCells.add(area.get("cgi")); // SES_PHONE_DIR1_CGI
        csvRowCells.add(""); // SES_PHONE_DIR1_CELL_NETWORK
        csvRowCells.add(source.getSystemId()); // SYSTEMID, e.g: mcng
        LocalDateTime rightNow = LocalDateTime.now();

        csvRowCells.add("vsal://lun2/" + mcngId + "/" + ldt.getYear() + "/" + ldt.getMonth() + "/" + ldt.getDayOfMonth() + "/dtucs0311/" +
                        rightNow.getYear() + "/" + rightNow.getMonth()
                                                           .getValue() + "/1.mp3"); // CONTENT_LOCATORS

        csvRowCells.add("testAPN");// APNNAME
        csvRowCells.add(partyAMcngId);// SIP_CALLID
        csvRowCells.add("SIPEVENTROY");// SIP_EVENT
        csvRowCells.add("testroy user AGENT");// USERAGENT
        csvRowCells.add("DIGI");// PROVIDERINFO

        return String.join(this.getDelimeter(), csvRowCells)
                     .concat(NEWLINE);
    }

    public String constructRecord(String mcngId,
                                  TargetDetailsModel partyA,
                                  TargetDetailsModel partyB,
                                  long callDuration,
                                  Instant timestamp,
                                  Map<String, String> area,
                                  int identityASelection,
                                  int identityBSelection,
                                  SourceModel sourceModel) {
        return constructRecord(mcngId,
                partyA.getMcngId(),
                new ArrayList<>(partyA.getMsisdn()).get(identityASelection),
                new ArrayList<>(partyA.getImsi()).get(identityASelection),
                new ArrayList<>(partyA.getImei()).get(identityASelection),
                new ArrayList<>(partyB.getMsisdn()).get(identityBSelection),
                callDuration,
                timestamp,
                area,
                sourceModel);
    }

    @Override
    protected String getHeaders() {
        return "\"SESSIONLOG_MCINTERCEPTID\"|\"SES_PHONE_DIR1_IDENTITY_VALUE\"|\"SES_IMSI_IDENTITY_VALUE\"|\"SES_IMEI_IDENTITY_VALUE\"|\"SES_PHONE_DIR2_IDENTITY_VALUE\"|\"SES_PHONE_DIR1_STARTDATE\"|\"SES_PHONE_DIR1_DURATION\"|\"SES_PHONE_DIR1_SESSIONTYPE\"|\"SES_PHONE_DIR1_INID_DIRECTION\"|\"SES_PHONE_DIR1_TRIG_TARGETID\"|\"SES_PHONE_DIR1_INID_MARKEDTYPE\"|\"SES_IMSI_INID_MARKEDTYPE\"|\"SES_IMEI_INID_MARKEDTYPE\"|\"SES_PHONE_DIR2_INID_MARKEDTYPE\"|\"SES_PHONE_DIR1_TRIG_TRIGGERID\"|\"SES_PHONE_DIR1_INID_MCAGENCYID\"|\"SES_PHONE_DIR1_CGI\"|\"SES_PHONE_DIR1_CELL_NETWORK\"|\"SYSTEMID\"|\"CONTENT_LOCATORS\"|\"APNNAME\"|\"SIP_CALLID\"|\"SIP_EVENT\"|\"USERAGENT\"|\"PROVIDERINFO\"";
    }
}
