package org.green.kav.db.fb.service.core;

import org.green.kav.db.fb.service.model.CallActivityGeneratorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.common.bean.AreaContent;
import org.green.kav.common.bean.flat.model.SourceModel;
import org.green.kav.common.model.IdentityCollection;
import org.green.kav.common.model.TargetDetailsModel;
import org.green.kav.common.model.request.CallActivityRequest;
import org.green.kav.fb.client.core.IpfFirebaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class McngFlatFileGeneratorJob {
    private Logger log = LoggerFactory.getLogger(McngFlatFileGeneratorJob.class);

    @Autowired
    private FlatFileGenerator flatFileGenerator;

    @Autowired
    private IpfFirebaseController ipfFirebaseController;

    @Autowired
    private McngService mcngService;

    @Autowired
    private AreaService areaService;

    @Async("processExecutor")
    public void run(String fileOutputDir,
                    List<String> targetNamesPartyA,
                    List<String> targetNamesPartyB,
                    LocalDateTime from,
                    LocalDateTime to,
                    long maxRecordInFlatFileTarget,
                    long maxRecordInfile,
                    SourceModel source) throws Exception {
        Map<String, Object> areas = areaService.get();

        log.info("target start Date: {}", from.toInstant(ZoneOffset.UTC));
        log.info("target end Date: {}", to.toInstant(ZoneOffset.UTC));

        List<TargetDetailsModel> partyATargetModels = mcngService.getTargetDataTree(targetNamesPartyA);
        List<TargetDetailsModel> partyBTargetModels = mcngService.getTargetDataTree(targetNamesPartyB);

        flatFileGenerator.generateCallActivities(maxRecordInFlatFileTarget,
                                                 partyATargetModels,
                                                 partyBTargetModels,
                                                 (Map<String, Object>) areas.get("message"),
                                                 from,
                                                 to,
                                                 fileOutputDir,
                                                 maxRecordInfile,
                                                 true,
                                                 source);
    }

    public CallActivityGeneratorResponse callGenerator(List<String> targetNamesPartyAls,
                                                       List<String> targetNamesPartyBls,
                                                       List<AreaContent> areaRequest,
                                                       long maxRecordInfile,
                                                       String fileOutputDir,
                                                       int maxRecords,
                                                       int minSecDiffBetweenCalls,
                                                       int maxSecDiffBetweenCalls,
                                                       SourceModel source) throws Exception {
        log.info("started [callGenerator] ...");

        List<IdentityCollection> identityCollections = ipfFirebaseController.getTargetsWithEqualIdentities();
        Map<String, AreaContent> allArea = ipfFirebaseController.getAllAreas();

        List<TargetDetailsModel> targetModelsPartyA = new ArrayList<>();
        List<TargetDetailsModel> targetModelsPartyB = new ArrayList<>();

        getCompleteTargetModels(targetNamesPartyAls,
                                targetNamesPartyBls,
                                identityCollections,
                                targetModelsPartyA,
                                targetModelsPartyB);

        List<AreaContent> finalAreaContent = constructAreaInfo(allArea, areaRequest);

        asyncRun(targetModelsPartyA,
                 targetModelsPartyB,
                 finalAreaContent,
                 maxRecordInfile,
                 fileOutputDir,
                 maxRecords,
                 minSecDiffBetweenCalls,
                 maxSecDiffBetweenCalls,
                 source);

        return new CallActivityGeneratorResponse(targetModelsPartyA,
                                                 targetModelsPartyB,
                                                 finalAreaContent);
    }

    @Async("processExecutor")
    public void asyncRun(List<TargetDetailsModel> targetModelsPartyA,
                         List<TargetDetailsModel> targetModelsPartyB,
                         List<AreaContent> finalAreaContent,
                         long maxRecordInfile,
                         String fileOutputDir,
                         int maxRecords,
                         int minSecDiffBetweenCalls,
                         int maxSecDiffBetweenCalls,
                         SourceModel source){

        flatFileGenerator.generateCallActivityRandomSecond(targetModelsPartyA,
                                                           targetModelsPartyB,
                                                           finalAreaContent,
                                                           fileOutputDir,
                                                           maxRecordInfile,
                                                           maxRecords,
                                                           minSecDiffBetweenCalls,
                                                           maxSecDiffBetweenCalls,
                                                           source);

        log.info("completed [callGenerator] file output at: {}...", fileOutputDir);
    }

    private List<AreaContent> constructAreaInfo(Map<String, AreaContent> areaContents, List<AreaContent> requestAreas) throws Exception {
        List<AreaContent> finalAreaContent = new ArrayList<>();
        for (AreaContent areaContent: requestAreas) {
            String areaName = areaContent.getName();
            AreaContent foundArea = areaContents.get(areaContent.getName());
            if (foundArea == null) throw new Exception("Given area: " + areaName + " not found!");
            foundArea.setFrom(areaContent.getFrom());
            foundArea.setTo(areaContent.getTo());
            finalAreaContent.add(foundArea);
        }
        return finalAreaContent;
    }

    private void getCompleteTargetModels(List<String> targetNamesPartyA,
                           List<String> targetNamesPartyB,
                           List<IdentityCollection> identityCollections,
                           List<TargetDetailsModel> targetModelsPartyA,
                           List<TargetDetailsModel> targetModelsPartyB) {
        for (IdentityCollection identityTarget: identityCollections) {
            TargetDetailsModel targetDetailsModel = new TargetDetailsModel();
            String name = identityTarget.getTargetName();
            List imeis = identityTarget.getImei();
            List imsis = identityTarget.getImsi();
            List msisdns = identityTarget.getMsisdn();

            if (!isValidateIdentity(name, imeis, "imei") ||
                    !isValidateIdentity(name, imsis, "imsi") ||
                    !isValidateIdentity(name, msisdns, "msisdn"))
                continue;

            if (msisdns.size() != imsis.size() || imsis.size() != imeis.size()) {
                log.info("Skipping: name: {}, has {} msisdn, {} imei, and {} imsi", name, msisdns.size(), imeis.size(), imsis.size());
                continue;
            }

            targetDetailsModel.setName(identityTarget.getTargetName());
            targetDetailsModel.setImei(identityTarget.getImei());
            targetDetailsModel.setImsi(identityTarget.getImsi());
            targetDetailsModel.setMsisdn(msisdns);
            targetDetailsModel.setMcngId(identityTarget.getMcngId());

            if (targetNamesPartyA.contains(identityTarget.getTargetName())) {
                targetModelsPartyA.add(targetDetailsModel);
            }

            if (targetNamesPartyB.contains(identityTarget.getTargetName())) {
                targetModelsPartyB.add(targetDetailsModel);
            }
        }
    }

    private boolean isValidateIdentity(String name, List identityLs, String identityType) {
        if (identityLs == null || identityLs.isEmpty()) {
            log.error("Target {} missing {}", name, identityType);
            return false;
        }
        return true;
    }

    @Async("processExecutor")
    public void run3(CallActivityRequest callActivityRequest) {
        flatFileGenerator.generateCallActivity(callActivityRequest.getCallActivities(),
                                               callActivityRequest.getFileOutputDir(),
                                               callActivityRequest.getMaxRecordInfile());
    }
}
