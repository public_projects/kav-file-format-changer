package org.green.kav.db.fb.service.core.mfi.model;

public class CdrModel {
    private String provider = "Movilnet";
    private String eventType = "VOICE";
    private String start;
    private int duration;
    private String status = "CALLING";
    private String cgiStartA;
    private String cgiStartB;
    private String latStartA;
    private String latStartB;
    private String lonStartA;
    private String lonStartB;
    private String cgiEndA;
    private String cgiEndB;
    private String latEndA;
    private String latEndB;
    private String lonEndA;
    private String lonEndB;
    private String msisdnA;
    private String msisdnB;
    private String msisdnC;
    private String imeiA;
    private String imeiB;
    private String imsiA;
    private String imsiB;

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCgiStartA() {
        return cgiStartA;
    }

    public void setCgiStartA(String cgiStartA) {
        this.cgiStartA = cgiStartA;
    }

    public String getCgiStartB() {
        return cgiStartB;
    }

    public void setCgiStartB(String cgiStartB) {
        this.cgiStartB = cgiStartB;
    }

    public String getLatStartA() {
        return latStartA;
    }

    public void setLatStartA(String latStartA) {
        this.latStartA = latStartA;
    }

    public String getLatStartB() {
        return latStartB;
    }

    public void setLatStartB(String latStartB) {
        this.latStartB = latStartB;
    }

    public String getLonStartA() {
        return lonStartA;
    }

    public void setLonStartA(String lonStartA) {
        this.lonStartA = lonStartA;
    }

    public String getLonStartB() {
        return lonStartB;
    }

    public void setLonStartB(String lonStartB) {
        this.lonStartB = lonStartB;
    }

    public String getCgiEndA() {
        return cgiEndA;
    }

    public void setCgiEndA(String cgiEndA) {
        this.cgiEndA = cgiEndA;
    }

    public String getCgiEndB() {
        return cgiEndB;
    }

    public void setCgiEndB(String cgiEndB) {
        this.cgiEndB = cgiEndB;
    }

    public String getLatEndA() {
        return latEndA;
    }

    public void setLatEndA(String latEndA) {
        this.latEndA = latEndA;
    }

    public String getLatEndB() {
        return latEndB;
    }

    public void setLatEndB(String latEndB) {
        this.latEndB = latEndB;
    }

    public String getLonEndA() {
        return lonEndA;
    }

    public void setLonEndA(String lonEndA) {
        this.lonEndA = lonEndA;
    }

    public String getLonEndB() {
        return lonEndB;
    }

    public void setLonEndB(String lonEndB) {
        this.lonEndB = lonEndB;
    }

    public String getMsisdnA() {
        return msisdnA;
    }

    public void setMsisdnA(String msisdnA) {
        this.msisdnA = msisdnA;
    }

    public String getMsisdnB() {
        return msisdnB;
    }

    public void setMsisdnB(String msisdnB) {
        this.msisdnB = msisdnB;
    }

    public String getMsisdnC() {
        return msisdnC;
    }

    public void setMsisdnC(String msisdnC) {
        this.msisdnC = msisdnC;
    }

    public String getImeiA() {
        return imeiA;
    }

    public void setImeiA(String imeiA) {
        this.imeiA = imeiA;
    }

    public String getImeiB() {
        return imeiB;
    }

    public void setImeiB(String imeiB) {
        this.imeiB = imeiB;
    }

    public String getImsiA() {
        return imsiA;
    }

    public void setImsiA(String imsiA) {
        this.imsiA = imsiA;
    }

    public String getImsiB() {
        return imsiB;
    }

    public void setImsiB(String imsiB) {
        this.imsiB = imsiB;
    }
}
