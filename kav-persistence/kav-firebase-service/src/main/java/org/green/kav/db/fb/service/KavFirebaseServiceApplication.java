package org.green.kav.db.fb.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@SpringBootApplication
@EnableCaching
@EnableAsync
@ComponentScan({"org.green.kav",
				"org.green.kav.common",
				"org.green.kav.db.fb",
				"org.green.kav.fb.client",
				"org.green.cassandra.services",
				"org.green.cassandra.astra.db",
				"org.green.cassandra.astra.db.config"})
public class KavFirebaseServiceApplication {

	private final static Logger log = LoggerFactory.getLogger(KavFirebaseServiceApplication.class);

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(KavFirebaseServiceApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	@Bean(name="processExecutor")
	public TaskExecutor workExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
		threadPoolTaskExecutor.setThreadNamePrefix("Async-");
		threadPoolTaskExecutor.setCorePoolSize(3);
		threadPoolTaskExecutor.setMaxPoolSize(3);
		threadPoolTaskExecutor.setQueueCapacity(600);
		threadPoolTaskExecutor.afterPropertiesSet();
		log.info("ThreadPoolTaskExecutor set");
		return threadPoolTaskExecutor;
	}
}
