package org.green.kav.db.fb.service.core.mcng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.common.bean.flat.model.SourceModel;
import org.green.kav.common.model.TargetDetailsModel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;



public abstract class ActivityFlatFileService extends FlatFileService {
    private final static Logger log = LoggerFactory.getLogger(ActivityFlatFileService.class);
    public static final DateTimeFormatter dateFileNameformatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
                                                                                   .withZone(ZoneOffset.UTC);
    protected final DateTimeFormatter mcngDateTimeFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS")
                                                                            .withZone(ZoneOffset.UTC);

    protected StringBuilder sb = new StringBuilder();
    protected BufferedWriter bufferedWriter;
    private long fileSizeLimit = 500000l;
    private File outputDirectory;
    private AtomicLong recordCount = new AtomicLong(0l);
    private String currentFile;
    private List<String> createdFiles = new ArrayList<>();

    public ActivityFlatFileService(String fileName, Long fileSizeLimit, String outputDirectory) {
        super(fileName);
        this.fileSizeLimit = fileSizeLimit;
        this.outputDirectory = getOutputPath(new File(outputDirectory));
    }


    public AtomicLong getRecordCount() {
        return recordCount;
    }

    public File getOutputDirectory() {
        return outputDirectory;
    }

    public String getCurrentFile() {
        return currentFile;
    }

    public void initializeFileWithHeaderData(String data, String filePath) {
        File jsonFile = new File(filePath);
        this.currentFile = filePath;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(jsonFile));
            createdFiles.add(jsonFile.getAbsolutePath());
            bufferedWriter.write(data);
            bufferedWriter.flush();
        }
        catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        }
//        log.info("Written to {}", jsonFile.getAbsolutePath());
    }

    public final void writeStream(String mcngId,
                                  TargetDetailsModel partyA,
                                  TargetDetailsModel partyB,
                                  long callDuration,
                                  Instant timestamp,
                                  Map<String, String> area,
                                  ThreadLocalRandom random,
                                  SourceModel source) {
        onReachingFileMaxRecordCount();
        int partyASelection = random.nextInt(partyA.getMsisdn().size());
        int partyBSelection = random.nextInt(partyB.getMsisdn().size());
        List<String> partyAMsisdnList = new ArrayList<>(partyA.getMsisdn());
        initiateIfNoPreviousRecords(partyA.getName(), partyAMsisdnList.get(partyASelection));
        String data = this.constructRecord(mcngId, partyA, partyB, callDuration, timestamp, area, partyASelection, partyBSelection, source);
        write(data);
       this.incrementRecordCount();
    }

    public abstract String constructRecord(String mcngId,
                                           TargetDetailsModel partyA,
                                           TargetDetailsModel partyB,
                                           long callDuration,
                                           Instant timestamp,
                                           Map<String, String> cgi,
                                           int partyAidentitySelection,
                                           int partyBIdentitySelection,
                                           SourceModel source);

    public abstract String constructRecord(String mcngId,
                                           String partyAMcngId,
                                           String msisdnA,
                                           String imsiA,
                                           String imeiA,
                                           String msisdnB,
                                           long callDuration,
                                           Instant timestamp,
                                           Map<String, String> area,
                                           SourceModel source);

    public final void write(String data) {
        try {
            bufferedWriter.write(data);
            bufferedWriter.flush();
        }
        catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        }
    }

    public void initiateIfNoPreviousRecords(String name, String msisdn) {
        if (this.getRecordCount().get() == 0l) {
            LocalDateTime localDateTime = LocalDateTime.now();
            localDateTime = localDateTime.plus(1, ChronoUnit.MILLIS);
            String newDirectory = this.getOutputDirectory().getAbsolutePath() +
                                  System.getProperty("file.separator") + "export" + localDateTime.format(mcngDateTimeFormat);
            sb.append("[target: " + name + "][msisdn: " + msisdn + "][written to: " + newDirectory + "]").append(System.lineSeparator());
            File newOutputPath = new File(newDirectory);
            newOutputPath.mkdir();
            initializeFileWithHeaderData(getHeaders() +
                                         NEWLINE, newDirectory +
                                                  System.getProperty("file.separator") +
                                                  this.getFileName());
        }
    }

    public String getDetailedInfo() {
        return sb.toString();
    }

    public List<String> getCreatedFiles() {
        StringBuilder sb = new StringBuilder();
        this.createdFiles.forEach(s -> {
            sb.append(s).append(System.lineSeparator());
        });
        return this.createdFiles;
    }

    public void close() {
        try {
            bufferedWriter.flush();
            bufferedWriter.close();
        }
        catch (IOException io) {
            io.printStackTrace();
        }
    }

    public void onReachingFileMaxRecordCount() {
        boolean reached = fileSizeLimit == recordCount.get();
        if (reached) {
            this.recordCount = new AtomicLong(0l);
            this.close();
        }
    }

    public long incrementRecordCount() {
        return this.recordCount.incrementAndGet();
    }

    @Override
    public void write(List<TargetDetailsModel> targets, File outputDirectory) {
        throw new UnsupportedOperationException();
    }

    public String toCsvString(TargetDetailsModel model) {
        throw new UnsupportedOperationException();
    }

    protected abstract String getHeaders();

    private File getOutputPath(File outputDirectory) {
        File dir = new File(outputDirectory.getAbsolutePath());
        if (!dir.exists()) dir.mkdirs();
        return new File(dir.getAbsolutePath());
    }
}
