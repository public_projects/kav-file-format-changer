package org.green.kav.db.fb.service.core;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.core.ApiFuture;
import com.google.firebase.database.DatabaseReference;
import org.green.kav.db.fb.service.listener.CompletionListenerImpl;
import org.green.kav.db.fb.service.model.ProgressStatus;
import org.green.kav.db.fb.service.model.identity.IdentityCollectionModel;
import org.green.kav.db.fb.service.model.identity.IdentityModel;
import org.green.kav.fb.client.core.IpfFirebaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class IdentityService extends AbstractFirebaseService {
    private IdentityCollectionModel identityCache;

    @Autowired
    private IpfFirebaseController ipfFirebaseController;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    public IdentityService(DatabaseReference identityDbReference2) {
        super(identityDbReference2);
    }

    @PostConstruct
    public void refreshCache() {
        String identites = null;
        try {
            identites = ipfFirebaseController.getIdentities().block();

            identityCache = objectMapper.readValue(identites, IdentityCollectionModel.class);
            log.debug("Number of IMEI: {}, MSISDN: {} and IMSI: {}",
                      identityCache.getIMEI().size(),
                      identityCache.getMSISDN().size(),
                      identityCache.getIMSI().size());
        } catch (Exception e) {
            log.debug("Identity: {}", identites);
            e.printStackTrace();
        }
    }

    public Map<String, Object> deleteIdentity(String type, String code) {
        DatabaseReference databaseReference = this.getDbReference()
                                                  .child(type);

        Map<String, Object> identity = this.get(databaseReference.child(code));

        if (identity != null) {
            Map<String, Object> innerData = (Map<String, Object>) identity.get("message");

            List<String> msisdns = new ArrayList<>();
            List<String> imsis = new ArrayList<>();
            List<String> imei = new ArrayList<>();

            Object object  = innerData.get("MSISDN");
            if (object!=null){
                msisdns = (List<String>) object;
            }

            object = innerData.get("IMSI");
            if (object!=null){
                imsis = (List<String>) object;
            }

            object = innerData.get("IMEI");
            if (object!=null){
                imei = (List<String>) object;
            }

            AtomicLong deletedCount = new AtomicLong();
            msisdns.forEach(s -> {
                deletedCount.incrementAndGet();
                this.remove("MSISDN", s);
                log.info("deleted: {} MSISDN", deletedCount.incrementAndGet());
            });
            imsis.forEach(s -> this.remove("IMSI", s));
            imei.forEach(s -> this.remove("IMEI", s));
        }

        return identity;
    }

    public Map<String, Object> remove(String identityType, String id){
        DatabaseReference dbRef = this.getDbReference().child(identityType).child(id);
        return this.removeByDbRef(dbRef);
    }

    public Map<String, Object> putMSISDN(List<String> identities, String targetName, String targetMcngId) {
        Map<String, Object> fullSetData = new HashMap<>();
        for (String identity : identities){
            fullSetData.putAll(putMSISDN(identity,  targetName,  targetMcngId));
        }
        return fullSetData;
    }

    public Map<String, Object> putMSISDN(String identity, String targetName, String targetMcngId) {
        return putIdentity("MSISDN",  identity,  targetName,  targetMcngId);
    }

    public Map<String, Object> putIMEI(List<String> identities, String targetName, String targetMcngId) {
        Map<String, Object> fullSetData = new HashMap<>();
        for (String identity : identities){
            fullSetData.putAll(putIMEI(identity, targetName, targetMcngId));
        }
        return fullSetData;
    }

    public Map<String, Object> putIMEI(String identity, String targetName, String targetMcngId) {
        return putIdentity("IMEI",  identity,  targetName,  targetMcngId);
    }

    public Map<String, Object> putIMSI(List<String> identities, String targetName, String targetMcngId) {
        Map<String, Object> fullSetData = new HashMap<>();
        for (String identity : identities){
            fullSetData.putAll(putIMSI(identity, targetName, targetMcngId));
        }
        return fullSetData;
    }

    public Map<String, Object> putIMSI(String identity, String targetName, String targetMcngId) {
        return putIdentity("IMSI",  identity,  targetName,  targetMcngId);
    }

    public Map<String, Object> putIdentity(String identityType, String identity, String targetName, String targetMcngId) {
        StopWatch watch = new StopWatch();
        watch.start();

        DatabaseReference databaseReference = this.getDbReference()
                .child(identityType);

        ProgressStatus progressStatus = new ProgressStatus(ProgressStatus.Status.CREATING);
        CompletionListenerImpl completionListener = new CompletionListenerImpl(progressStatus);
        databaseReference.child(identity)
                .child("targets")
                .child(targetMcngId)
                .setValue(Collections.singletonMap("targetName", targetName), completionListener);
        watch.stop();
        log.info("Completed {} ms", watch.getTotalTimeMillis());
        return get(databaseReference.child(identity));
    }

    public IdentityModel getMsisdn(String code) {
        IdentityModel identityModel = null;
        if (identityCache == null) {
            DatabaseReference databaseReference = this.getDbReference()
                                                      .child("MSISDN");

            Map<String, Object> data = this.get(databaseReference.child(code));
            Object output = data.get("message");

            if (output == null){
                return null;
            }

            Map<String, Object> identityData = (Map<String, Object>) output;
            identityModel = new IdentityModel();
            identityModel.setTargets((Map<String, Map<String, String>>) identityData.get("targets"));

            return identityModel;
        }
        return identityCache.getMSISDN().get(code);
    }

    public IdentityModel getImsi(String code) {
        IdentityModel identityModel = null;
        if (identityCache == null) {
            DatabaseReference databaseReference = this.getDbReference()
                                                      .child("IMSI");

            Map<String, Object> data = this.get(databaseReference.child(code));
            Object output = data.get("message");

            if (output == null){
                return null;
            }

            Map<String, Object> identityData = (Map<String, Object>) output;
            identityModel = new IdentityModel();
            identityModel.setTargets((Map<String, Map<String, String>>) identityData.get("targets"));

            return identityModel;
        }

        return identityCache.getIMSI().get(code);
    }

    public IdentityModel getImei(String code) {
        IdentityModel identityModel = null;
        if (identityCache == null) {
            DatabaseReference databaseReference = this.getDbReference()
                                                      .child("IMEI");

            Map<String, Object> data = this.get(databaseReference.child(code));
            Object output = data.get("message");

            if (output == null) {
                return null;
            }

            Map<String, Object> identityData = (Map<String, Object>) output;
            identityModel = new IdentityModel();
            identityModel.setTargets((Map<String, Map<String, String>>) identityData.get("targets"));

            return identityModel;
        }

        return identityCache.getIMEI().get(code);
    }
}
