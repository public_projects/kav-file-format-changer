package org.green.kav.db.fb.service.listener;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.db.fb.service.model.ProgressStatus;
import java.util.concurrent.CompletableFuture;


public class CompletionListenerImpl implements DatabaseReference.CompletionListener {
    private ProgressStatus progressStatus;
    private CompletableFuture futureResult;

    public CompletionListenerImpl(ProgressStatus progressStatus){
        this.progressStatus = progressStatus;
        this.futureResult = new CompletableFuture();
    }

    @Override
    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
        progressStatus.completed();
        futureResult.complete(progressStatus);
    }

    public CompletableFuture getFutureResult() {
        return futureResult;
    }

    public ProgressStatus getProgressStatus() {
        return progressStatus;
    }
}
