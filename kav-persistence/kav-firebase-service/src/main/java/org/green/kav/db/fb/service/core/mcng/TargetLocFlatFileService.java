package org.green.kav.db.fb.service.core.mcng;

import org.green.kav.common.bean.flat.model.SourceModel;
import org.green.kav.common.model.TargetDetailsModel;

import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Map;



public class TargetLocFlatFileService extends ActivityFlatFileService {

    public TargetLocFlatFileService(String outputDir, Long fileSizeLimit) {
        super("P_LOC_FLAT.csv", fileSizeLimit, outputDir);
    }

    @Override
    public String constructRecord(String mcngId,
                                  TargetDetailsModel partyA,
                                  TargetDetailsModel partyB,
                                  long callDuration,
                                  Instant timestamp,
                                  Map<String, String> cgi,
                                  int partyAIdentitySelection,
                                  int partyBIdentitySelection,
                                  SourceModel source) {
        return this.constructRecord(mcngId,
                partyA.getMcngId(),
                new ArrayList<>(partyA.getMsisdn()).get(partyAIdentitySelection),
                new ArrayList<>(partyA.getImsi()).get(partyAIdentitySelection),
                new ArrayList<>(partyA.getImei()).get(partyAIdentitySelection),
                new ArrayList<>(partyB.getMsisdn()).get(partyBIdentitySelection),
                callDuration,
                timestamp,
                cgi,
                source);
    }

    @Override
    public String constructRecord(String mcngId,
                                  String partyAMcngId,
                                  String msisdnA,
                                  String imsiA,
                                  String imeiA,
                                  String msisdnB,
                                  long callDuration,
                                  Instant timestamp,
                                  Map<String, String> cgi,
                                  SourceModel source) {

        LinkedList<String> csvRowCells = new LinkedList<>();
        csvRowCells.add(mcngId); // SES4LOC_PHONE_MCINTERCEPTID
        csvRowCells.add(source.getAgentId()); // SES4LOC_PHONE_MCAGENCYID, e.g: 3
        csvRowCells.add(msisdnA); // SES4LOC_PHONE_IDENTITY_VALUE
        csvRowCells.add(imsiA); // SES4LOC_IMSI_IDENTITY_VALUE
        csvRowCells.add(imeiA); // SES4LOC_IMEI_IDENTITY_VALUE
        csvRowCells.add(cgi.get("long")); // SES4LOC_GEOCOR_SPATIAL_X
        csvRowCells.add(cgi.get("lat")); // SES4LOC_GEOCOR_SPATIAL_Y
        csvRowCells.add(""); // SES4LOC_GEOCOR_GEOACCURACY
        csvRowCells.add(cgi.get("cgi")); // SES4LOC_CELL_CELLID_CGI
        csvRowCells.add(dateFileNameformatter.format(timestamp)); // SES4LOC_TIMESTAMP
        csvRowCells.add(source.getSystemId()); // SYSTEMID, e.g: mcng
        return String.join(this.getDelimeter(), csvRowCells)
                     .concat(NEWLINE);
    }

    @Override
    protected String getHeaders() {
        return "\"SES4LOC_PHONE_MCINTERCEPTID\"|\"SES4LOC_PHONE_MCAGENCYID\"|\"SES4LOC_PHONE_IDENTITY_VALUE\"|\"SES4LOC_IMSI_IDENTITY_VALUE\"|\"SES4LOC_IMEI_IDENTITY_VALUE\"|\"SES4LOC_GEOCOR_SPATIAL_X\"|\"SES4LOC_GEOCOR_SPATIAL_Y\"|\"SES4LOC_GEOCOR_GEOACCURACY\"|\"SES4LOC_CELL_CELLID_CGI\"|\"SES4LOC_TIMESTAMP\"|\"SYSTEMID\"";
    }
}
