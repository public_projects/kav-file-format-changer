package org.green.kav.db.fb.service.model;

import org.springframework.util.StopWatch;

import java.time.LocalDate;

public class ProgressStatus {
    public enum Status {
        CREATING, COMPLETED, FAILED, UPDATING, FETCHING, DELETING
    }

    private StopWatch watch;
    private Status status;
    private Object value;
    private LocalDate updatedDate;

    public ProgressStatus(Status status) {
        this.status = status;
        this.updatedDate = LocalDate.now();
        this.watch = new StopWatch();
        this.watch.start();
    }

    public void completed(Object t) {
        this.status = Status.COMPLETED;
        this.value = t;
        this.updatedDate = LocalDate.now();
        watch.stop();
    }

    public void completed() {
        this.status = Status.COMPLETED;
        this.updatedDate = LocalDate.now();
        watch.stop();
    }

    public void failed() {
        this.status = Status.FAILED;
        this.updatedDate = LocalDate.now();
        watch.stop();
    }

    public boolean isInprogress() {
        if (isCompleted()) {
            return false;
        }
        else if (isFailed()) {
            return false;
        }

        return true;
    }

    public StopWatch getWatch() {
        return watch;
    }

    public void setWatch(StopWatch watch) {
        this.watch = watch;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean isCompleted() {
        return this.status == Status.COMPLETED;
    }

    public boolean isFailed() {
        return this.status == Status.FAILED;
    }

    public long getTimeTookInMs() {
        return watch.getTotalTimeMillis();
    }
}
