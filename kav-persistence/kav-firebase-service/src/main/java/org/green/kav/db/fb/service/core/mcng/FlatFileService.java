package org.green.kav.db.fb.service.core.mcng;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.kav.common.model.TargetDetailsModel;
import org.green.kav.db.fb.service.core.TargetService;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;


public abstract class FlatFileService {
    private final static Logger log = LoggerFactory.getLogger(FlatFileService.class);
    public final String NEWLINE = System.getProperty("line.separator");
    private final String delimeter = "|";
    private final DateTimeFormatter mcngDateTimeFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private String fileName;

    @Autowired
    private TargetService targetService;

    public DateTimeFormatter getMcngDateTimeFormat() {
        return mcngDateTimeFormat;
    }

    public TargetService getTargetService() {
        return targetService;
    }

    public FlatFileService(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getDelimeter() {
        return delimeter;
    }

    public void write(List<TargetDetailsModel> targets, File outputDirectory) {
        File newFile = getOutputPath(outputDirectory);

        try {
            FileWriter out = new FileWriter(newFile.getAbsoluteFile());
            out.write(getHeaders() + NEWLINE);
            log.trace("creating {} file at directory {}", newFile.getName(), newFile.getAbsolutePath());
            for (TargetDetailsModel targetDetailsModel : targets) {
                out.write(toCsvString(targetDetailsModel));
            }
            out.flush();
            out.close();
        }
        catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    protected abstract String toCsvString(TargetDetailsModel model);

    protected abstract String getHeaders();

    private File getOutputPath(File outputDirectory) {
        File dir = new File(outputDirectory.getAbsolutePath());
        if (!dir.exists()) dir.mkdirs();
        return new File(dir.getAbsolutePath() + System.getProperty("file.separator") + this.getFileName());
    }
}
