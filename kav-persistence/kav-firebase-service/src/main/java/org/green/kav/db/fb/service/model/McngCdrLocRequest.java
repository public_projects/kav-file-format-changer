package org.green.kav.db.fb.service.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.green.kav.common.bean.AreaContent;
import org.green.kav.common.bean.flat.model.SourceModel;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class McngCdrLocRequest implements Serializable {
    private String flatFileOutputLocation;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime from;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime to;
    private List<String> targetPartyANames;
    private List<String> targetPartyBNames;
    private List<AreaContent> areaContents;
    private long callCountByEachTarget;
    private long maxRecordInFlatFile;
    private int maxRecordsPerTargetA;
    private int maxSecDiffBetweenCalls;
    private int minSecDiffBetweenCalls = 10;
    private SourceModel source;

    public String getFlatFileOutputLocation() {
        return flatFileOutputLocation;
    }

    public void setFlatFileOutputLocation(String flatFileOutputLocation) {
        this.flatFileOutputLocation = flatFileOutputLocation;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }

    public List<String> getTargetPartyANames() {
        return targetPartyANames;
    }

    public void setTargetPartyANames(List<String> targetPartyANames) {
        this.targetPartyANames = targetPartyANames;
    }

    public List<String> getTargetPartyBNames() {
        return targetPartyBNames;
    }

    public void setTargetPartyBNames(List<String> targetPartyBNames) {
        this.targetPartyBNames = targetPartyBNames;
    }

    public List<AreaContent> getAreaContents() {
        return areaContents;
    }

    public void setAreaContents(List<AreaContent> areaContents) {
        this.areaContents = areaContents;
    }

    public long getCallCountByEachTarget() {
        return callCountByEachTarget;
    }

    public void setCallCountByEachTarget(long callCountByEachTarget) {
        this.callCountByEachTarget = callCountByEachTarget;
    }

    public long getMaxRecordInFlatFile() {
        return maxRecordInFlatFile;
    }

    public void setMaxRecordInFlatFile(long maxRecordInFlatFile) {
        this.maxRecordInFlatFile = maxRecordInFlatFile;
    }

    public int getMaxRecordsPerTargetA() {
        return maxRecordsPerTargetA;
    }

    public void setMaxRecordsPerTargetA(int maxRecordsPerTargetA) {
        this.maxRecordsPerTargetA = maxRecordsPerTargetA;
    }

    public int getMaxSecDiffBetweenCalls() {
        return maxSecDiffBetweenCalls;
    }

    public void setMaxSecDiffBetweenCalls(int maxSecDiffBetweenCalls) {
        this.maxSecDiffBetweenCalls = maxSecDiffBetweenCalls;
    }

    public int getMinSecDiffBetweenCalls() {
        return minSecDiffBetweenCalls;
    }

    public void setMinSecDiffBetweenCalls(int minSecDiffBetweenCalls) {
        this.minSecDiffBetweenCalls = minSecDiffBetweenCalls;
    }

    public SourceModel getSource() {
        return source;
    }

    public void setSource(SourceModel source) {
        this.source = source;
    }
}
