package org.green.kav.db.fb.service.model;

import org.green.kav.common.bean.AreaContent;
import org.green.kav.common.model.TargetDetailsModel;

import java.io.Serializable;
import java.util.List;

public class CallActivityGeneratorResponse implements Serializable {
    private final List<TargetDetailsModel> targetModelsPartyA;
    private final List<TargetDetailsModel> targetModelsPartyB;
    private final List<AreaContent> areaRequestContents;

    public CallActivityGeneratorResponse(List<TargetDetailsModel> targetModelsPartyA, List<TargetDetailsModel> targetModelsPartyB, List<AreaContent> areaRequestContents){
        this.targetModelsPartyA = targetModelsPartyA;
        this.targetModelsPartyB = targetModelsPartyB;
        this.areaRequestContents = areaRequestContents;
    }

    public List<TargetDetailsModel> getTargetModelsPartyA() {
        return targetModelsPartyA;
    }

    public List<TargetDetailsModel> getTargetModelsPartyB() {
        return targetModelsPartyB;
    }

    public List<AreaContent> getAreaRequestContents() {
        return areaRequestContents;
    }
}
