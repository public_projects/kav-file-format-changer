package org.green.kav.db.fb.service.core;

import com.google.firebase.database.DatabaseReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.green.util.common.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


@Service
public class CountryService extends AbstractFirebaseService{
    private final static Logger log = LoggerFactory.getLogger(CountryService.class);

    @Autowired
    public CountryService(DatabaseReference countryCodeDbReference) {
        super(countryCodeDbReference);
    }

    public ShortCountryCode getShortCountryCodes() {
        CountryCode[] countryCodes = CountryCode.values();
        Map<String, Map<String, String>> countryCodeMapMap = new HashMap(countryCodes != null ? countryCodes.length : 125);
        Arrays.asList(CountryCode.values()).forEach(countryCode -> {
            Map<String, String> countryCodeMap = new HashMap<>(3);
            countryCodeMap.put("country", countryCode.getCountry());
            countryCodeMap.put("code", countryCode.getAlpha3Code());
            countryCodeMap.put("numeric", countryCode.getNumeric());
            countryCodeMapMap.put(countryCode.toString(), countryCodeMap);
        });
        return new ShortCountryCode(countryCodeMapMap);
    }

    public class ShortCountryCode {
        private final Map<String, Map<String, String>> countryCodeLs;

        public ShortCountryCode(Map<String, Map<String, String>> countryCodeLs) {
            this.countryCodeLs = countryCodeLs;
        }

        public Map<String, Map<String, String>> getCountryCodeLs() {
            return countryCodeLs;
        }
    }
}
