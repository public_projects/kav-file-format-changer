package org.green.kav.db.fb.service.model.identity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.HashMap;
import java.util.Map;

public class IdentityCollectionModel {
    @JsonProperty("IMEI")
    private Map<String, IdentityModel> IMEI = new HashMap<>();
    @JsonProperty("MSISDN")
    private Map<String, IdentityModel> MSISDN = new HashMap<>();
    @JsonProperty("IMSI")
    private Map<String, IdentityModel> IMSI = new HashMap<>();

    public IdentityCollectionModel(){}

    public IdentityCollectionModel(Map<String, IdentityModel> IMEI, Map<String, IdentityModel> MSISDN, Map<String, IdentityModel> IMSI) {
        this.IMEI = IMEI;
        this.MSISDN = MSISDN;
        this.IMSI = IMSI;
    }

    public Map<String, IdentityModel> getIMEI() {
        return IMEI;
    }

    public void setIMEI(Map<String, IdentityModel> IMEI) {
        this.IMEI = IMEI;
    }

    public Map<String, IdentityModel> getMSISDN() {
        return MSISDN;
    }

    public void setMSISDN(Map<String, IdentityModel> MSISDN) {
        this.MSISDN = MSISDN;
    }

    public Map<String, IdentityModel> getIMSI() {
        return IMSI;
    }

    public void setIMSI(Map<String, IdentityModel> IMSI) {
        this.IMSI = IMSI;
    }
}
