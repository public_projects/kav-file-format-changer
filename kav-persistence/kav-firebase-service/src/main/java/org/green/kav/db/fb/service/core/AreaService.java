package org.green.kav.db.fb.service.core;

import com.google.firebase.database.DatabaseReference;
import org.green.kav.common.model.request.AreaRequest;
import org.green.kav.db.fb.service.model.identity.AreaModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class AreaService extends AbstractFirebaseService {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CgiService cgiService;

    @Autowired
    public AreaService(DatabaseReference areaDbReference2) {
        super(areaDbReference2);
    }

    public List<Map<String, Object>> put(List<AreaRequest> areaRequestLs) {
        List<Map<String, Object>> persistedArea = new ArrayList<>(areaRequestLs.size());
        for (AreaRequest areaRequest : areaRequestLs) {
            Map<String, Object> areaData = new HashMap<>(4);
            areaData = super.put(areaRequest.getName(), areaRequest.getDataByName());
            areaData.put("name", areaRequest.getName());
            persistedArea.add(areaData);
        }
        return persistedArea;
    }

    public List<AreaModel> getAreaModels() {
        List<AreaModel> areaModels = new ArrayList<>();
        AtomicInteger areaCount = new AtomicInteger();
        this.get()
            .forEach((s, o) -> {
                if (o instanceof Map) {
                    ((Map) o).forEach((o1, o2) -> {
                        log.debug("{}) Area: {}, {}", areaCount.incrementAndGet(), o1, o2);

                        Double latitude = (Double) ((Map) o2).get("latitude");
                        Double longitude = (Double) ((Map) o2).get("longitude");
                        areaModels.add(new AreaModel((String) o1,
                                                     (String) ((Map) o2).get("cgi"),
                                                     latitude, longitude));
                    });
                }
            });
        return areaModels;
    }

    public void getAreaByNames(List<String> areaNames) {
        for (String area: areaNames){
            Map<String, Object> message = this.get(area);
            Map<String, Object> areaData = (Map<String, Object>) message.get("message");
        }
    }

    public void enrichCgi() {
        List<AreaModel> areaModels = getAreaModels();
        Map<String, Object> values = new HashMap<>();
        areaModels.forEach(areaModel -> {
            values.put("lat", areaModel.getLatitude()+"");
            values.put("long", areaModel.getLongitude()+"");
            values.put("name", areaModel.getName());
            cgiService.put(areaModel.getCgi(), values);
        });
    }
}
