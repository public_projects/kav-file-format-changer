package org.green.kav.db.fb.service;

import com.google.cloud.firestore.CollectionReference;
import com.google.firebase.database.DatabaseReference;
import org.green.kav.common.model.IdentityCollection;
import org.green.kav.common.model.TargetDetailsModel;
import org.green.kav.db.fb.service.core.*;
import org.green.kav.db.fb.service.listener.CompletionListenerImpl;
import org.green.kav.db.fb.service.model.ProgressStatus;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.green.cassandra.services.astra.dao.AstraDAO;
import org.green.cassandra.services.astra.entity.TargetIdentityCountEntity;
import org.green.kav.db.fb.service.model.identity.AreaModel;
import org.green.kav.db.fb.service.model.identity.TargetModel;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.junit.jupiter.api.Disabled;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.jupiter.api.Assertions;
import org.springframework.context.annotation.Lazy;

@SpringBootTest
class KavFirebaseServiceApplicationTests {
	protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	@Lazy
	@Autowired
	private AstraDAO targetIdentityCountDAOImpl;

	@Autowired
	private McngService mcngService;

	@Autowired
	private TargetService targetService;

	@Autowired
	private AreaService areaService;

	@Autowired
	private IdentityService identityService;

	@Autowired
	private ImeiTypeAllocationCodeService imeiTypeAllocationCodeService;

	@Autowired
	private CollectionReference mcngCollectionRef;

	@Autowired
	private DatabaseReference areaDbReference2;

	@Autowired
	private DatabaseReference identityDbReference2;

	@Autowired
	private DatabaseReference mcngDbReference2;

	@Test
	@Disabled
	public void getTargetMsisdn() {
		String[] msisdnArray = {"Albina Wolf",
										 "Eunice Feest",
										 "Idella Auer",
										 "Clinton Senger",
										 "Akeem Flatley",
										 "Jack Napier",
										 "Cesar Turner",
										 "Francesca Hane",
										 "Adelbert Toy",
										 "Dereck Boyer",
										 "Harleen Frances",
										 "Rosemary Hermiston",
										 "Harold Jordan",
										 "Juan Sloan",
										 "Kaleb Bins",
										 "Cindy Hartmann",
										 "Leila Mitchell",
										 "Jessica Cruz",
										 "Mary Jane",
										 "Vanessa Altenwerth"
		};

		List<TargetDetailsModel> models = mcngService.getTargetDataTree(Arrays.asList(msisdnArray));

		for (TargetDetailsModel model: models) {
			String msisdn = model.getMsisdn().get(0);
			String id = "006Smsisdn0" + msisdn.length() + "S" + msisdn;
			System.err.println(id);
			String data = "{" + "\"" + "identityId" + "\"" + ":" + "\"" + id + "\"" + "}";
//			System.err.println(data);
		}
	}

	/**
	 * Never run the below!
	 */
	@Test
	public void migrateArea() {
		List<AreaModel> allArea = areaService.getAreaModels();
		for (AreaModel area : allArea) {
			LOGGER.info("{}", area);
//			Map<String, Object> subContainer = new HashMap<>();
//			subContainer.put("cgi", area.getCgi());
//			subContainer.put("latitude", area.getLatitude());
//			subContainer.put("longitude", area.getLongitude());
//			put(areaDbReference2, area.getName(), subContainer); // Don't run this line!!!!
		}
	}
	@Test
	public void migrateMcng() {
		long start = System.currentTimeMillis();
		Map<String, Object> data =  mcngService.get("id");
		LOGGER.info("Took: {}", System.currentTimeMillis() - start);
		if (data != null){
			Map<String, Object> message = (Map<String, Object>) data.get("message");
			for (String id: message.keySet()){
				LOGGER.info("{}", message.get(id));
			}
		}
	}

	@Test
	void contextLoads() throws Exception {
		String msisdn = " ";

		String newMsisdn = ((msisdn == null || msisdn.trim()
													 .isEmpty()) ?
							null :
							msisdn.trim()
								  .toLowerCase());
		Assertions.assertNull(newMsisdn);

		msisdn = "";
		newMsisdn = ((msisdn == null || msisdn.trim()
											  .isEmpty()) ?
					 null :
					 msisdn.trim()
						   .toLowerCase());
		Assertions.assertNull(newMsisdn);
	}

	@Test
	@Disabled
	public void insertTargetIdentityCountTest() {
		List<TargetModel> targetModels = this.targetService.getTargetModels();

		targetModels.forEach(targetModel -> {
			List<String> msisdns = targetModel.getMsisdList();
			String targetName = targetModel.getName();
			String mcngEtl = targetModel.getMcngEtl();

			TargetIdentityCountEntity msisdnCountEntity = new TargetIdentityCountEntity(targetName, mcngEtl, "MSISDN", msisdns.size());
			targetIdentityCountDAOImpl.insert(msisdnCountEntity);

			List<String> imeis = targetModel.getImeiList();
			TargetIdentityCountEntity imeiCountEntity = new TargetIdentityCountEntity(targetName, mcngEtl, "IMEI", imeis.size());
			targetIdentityCountDAOImpl.insert(imeiCountEntity);

			List<String> imsis = targetModel.getImsiList();
			TargetIdentityCountEntity imsiCountEntity = new TargetIdentityCountEntity(targetName, mcngEtl, "IMSI", imsis.size());
			targetIdentityCountDAOImpl.insert(imsiCountEntity);
		});
	}

	@Test
	public void buildIdentites(){
		List<TargetModel> targetModels = this.targetService.getTargetModels();

		targetModels.forEach(targetModel -> {
			LOGGER.info("{}", targetModel);
//			defineIdentity(targetModel, "MSISDN", targetModel.getMsisdList()); // Don't run this. It was meant to be executed only once. It is done once
//			defineIdentity(targetModel, "IMEI", targetModel.getImeiList()); // Don't run this. It was meant to be executed only once. It is done once
//			defineIdentity(targetModel, "IMSI", targetModel.getImsiList()); // Don't run this. It was meant to be executed only once. It is done once
		});
	}

	@Test
	public void createTarget(){
		mcngService.putTarget(Collections.singletonList("Abigale Heller"));

		IdentityCollection identityCollection = IdentityCollection.of("Abu Dabie", "msisdn21", "imsi1", "imei1", "mcng1");
		targetService.putTarget(identityCollection.getTargetContainer());

		Map<String, Object> content = new HashMap<>();
		Map<String, Object> identitiesMap = new HashMap<>();
		identitiesMap.put("COMPANYID", Collections.singletonList("12344"));
		content.put("Abu Dabie", identitiesMap);

		targetService.putTarget(content);
	}

	@Test
	public void associateIdentityTest(){
		targetService.associateIdentity("Adelbert Toy", "toy", "PASSID");
	}

	@Test
	public void getTargetByName() {
		TargetModel targetModel = targetService.getTargetByName("Abigale Heller");
		LOGGER.info("{}", targetModel);
	}

	@Test
	public void createMSISDN(){
		identityService.putMSISDN("002", "Simpleton Bob", "003");
	}

	@Test
	public void buildMcngData(){
		List<TargetModel> targetModels = this.targetService.getTargetModels();
		targetModels.forEach(targetModel -> {
			LOGGER.info("{}", targetModel);
			Map<String, Object> name = new HashMap<>();
			name.put("targetName", targetModel.getName());
//			put(mcngDbReference2, targetModel.getMcngEtl(), name);  // Don't run this. It was meant to be executed only once. It is done once
		});
	}

	private void defineIdentity(TargetModel targetModel, String type, List<String> identities) {
		for (String identityValue : identities) {
			DatabaseReference leafRef = identityDbReference2.child(type).
					child(identityValue).
					child("targets");
			Map<String, Object> name = new HashMap<>();
			name.put("targetName", targetModel.getName());
			put(leafRef, targetModel.getMcngEtl(), name);
		}
	}

	private void put(DatabaseReference ref, String id, Map<String, Object> targetData) {
		CompletionListenerImpl completionListener = new CompletionListenerImpl(new ProgressStatus(ProgressStatus.Status.CREATING));
		ref.child(id).setValue(targetData, completionListener);
	}

	@Test
	@Disabled
	public void getTargetIdentityCount() {
		List<TargetIdentityCountEntity> targetIdentityCountEntities = targetIdentityCountDAOImpl.fetchAll();

		for (TargetIdentityCountEntity entity : targetIdentityCountEntities) {
			if (entity.getCount() == 1) {
				LOGGER.info("name: {}, {} -> {}", entity.getName(), entity.getIdentityType(), entity.getCount());
			}
		}
	}

	@Test
	@Disabled
	public void getMcngIdFromFirestore(){
		String mcngEtlId = "827211";

		try {
			LOGGER.info("{}", mcngCollectionRef.document(mcngEtlId).get().get().getData());
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	@Test
	@Disabled
	public void testStoreMcngEtlIdInFirestore(){
		for (TargetModel targetModel: targetService.getTargetModels()) {
			String name = targetModel.getName();

			Map<String, Object> data = new HashMap<>();
			data.put("MSISDN", targetModel.getMsisdList());
			data.put("IMSI", targetModel.getImsiList());
			data.put("IMEI", targetModel.getImeiList());
			String mcngEtlId = targetModel.getMcngEtl();

			try {
				// The line below saved data in firestore
				mcngCollectionRef.document(mcngEtlId).set(Collections.singletonMap("targetName", name)).get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}

//		try {
//			mcngCollectionRef.document("006505").set(Collections.singletonMap("targetName", "Rashawn Cole")).get();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		}
//
//		mcngCollectionRef.listDocuments().forEach(documentReference -> {
//			ApiFuture<DocumentSnapshot> apiFuture = documentReference.get();
//			try {
//				DocumentSnapshot docSnap = apiFuture.get();
//				Map<String, Object> dataMap = docSnap.getData();
//				System.err.println(docSnap.getId() + " -> " + dataMap);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			} catch (ExecutionException e) {
//				e.printStackTrace();
//			}
//		});
	}

	@Test
	@Disabled
	public void generatePA_data() throws IOException {
		long start = System.currentTimeMillis();
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
		Random random = new Random();

		List<TargetModel> targetModels = this.targetService.getTargetModels();
		LOGGER.info("Target size: {}", targetModels.size());

		List<AreaModel> areaModels = this.areaService.getAreaModels();
		LOGGER.info("Area size: {}", areaModels.size());

		LocalDateTime callTime = LocalDateTime.of(2020, Month.JUNE, 30, 10, 11, 12);

		String fileName = UUID.randomUUID() + ".csv";
		FileWriter writer = new FileWriter(fileName);
		writer.write("IDENTITY;LATITUDE;LONGITUDE;TIME" + System.lineSeparator());
		writer.flush();

		int totalRecords = 2000000;
		final int batchSize = 10000;
		int currentRecordCount = 1;
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < totalRecords; i++) {
			currentRecordCount++;
			String identity = getRandomIdentityStr(targetModels, random);
			String coordinateStr = getRandomCoordinateStr(areaModels, random);
			callTime = callTime.plusSeconds(5l);
			String formattedDate = String.valueOf(callTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
			str.append(identity + ";" + coordinateStr + ";" + formattedDate + System.lineSeparator());

			if (currentRecordCount == batchSize) {
				LOGGER.info("Written record count: {}", i);
				currentRecordCount = 0;
				writer.write(str.toString());
				writer.flush();
				str = new StringBuilder();
			}
		}

		LOGGER.info("Written record count: {}", currentRecordCount);

		writer.write(str.toString());
		writer.close();
		LOGGER.info("Completed in: {} ms. File: {}, total record count: {}", System.currentTimeMillis() - start, fileName, totalRecords);
	}

	private String getRandomCoordinateStr(List<AreaModel> areaModels, Random random) {
		double[] sampleData = {0.001, 0.002, 0.003, 0.004, 0.005};

		String latLongStr;
		AreaModel areaModel = areaModels.get(random.nextInt(areaModels.size()));
		double newlatitude = getNewValue(sampleData, random, areaModel.getLatitude());
		double newLongitude = getNewValue(sampleData, random, areaModel.getLongitude());
//        log.debug("latitude: {}, newlatitude {}, longitude: {}, newLongitude: {}",
//                 areaModel.getLatitude(),
//                 newlatitude,
//                 areaModel.getLongitude(),
//                 newLongitude);

		latLongStr = newlatitude + ";" + newLongitude;

		return latLongStr;
	}

	private String getRandomIdentityStr(List<TargetModel> targetModels, Random random){
		TargetModel targetModel = targetModels.get(random.nextInt(targetModels.size()));
		return targetModel.getName().replaceAll(" ", "_").replaceAll("'", "_");
	}

	private double getNewValue(double[] sampleData, Random random, double coordinate) {
		double randomValue = sampleData[random.nextInt(sampleData.length)];
//        log.debug("randomValue: {}", randomValue);
		double newValue = 0;
		if (random.nextBoolean()) {
			newValue = coordinate - randomValue;
		} else {
			newValue = coordinate + randomValue;
		}
		return newValue;
	}

	@Test
	@Disabled
	public void pushToFirebase() {
		Map<String, Object> imeiTypeMap = this.imeiTypeAllocationCodeService.get();
		LOGGER.info("{}", imeiTypeMap.size());

		Map<String, Map<String, String>> imeiData = (Map<String, Map<String, String>>) imeiTypeMap.get("message");

		LOGGER.info("size: {}", imeiData.size());

//        Map<String, Object> device = new HashMap<>();
//        device.put("model", "Nokia");
//        device.put("manufacturer", "Nokia 3306");
//        this.imeiTypeAllocationCodeService.put("test", device);
	}

	@Disabled("Disabled until CustomerService is up!")
	@Test
	public void generateRandomTarget() throws Exception {
//        Map codes = ipfFirebaseController.getCountries()
//                                         .block();
//        List<String> codeLs = new ArrayList();
//        codes.keySet().forEach(o -> {codeLs.add((String) o);});
//
//        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
//        Map<String, Object> createdTargets = new HashMap();
//        Map<String, Object> validCountry = new HashMap<>();
//        for (int i = 0; i < 5; i++) {
//            validCountry = targetService.getValidCountry(codes, codeLs, threadLocalRandom);
//            log.info("country: {}", validCountry);
//            List<String> callingCodes = (List<String>) validCountry.get("callingCodes");
//            String callingCode = callingCodes.get(0);
//            log.info("calling code: {}", callingCode);
//
//            Map<String, String> providerCodeMap = targetService.getRandomTelcoOperator(validCountry);
//            log.info("provider calling code: {}", providerCodeMap);
//        }

//        Map<String, Object> result = targetService.generateRandomTargets(5, 3, 3, 3, false);

//        log.info("{}", result);
	}

//    @Test
//    void repairTargets() {
//        Map<String, Object> mcngData = this.mcngService.get();
//        Map<String, Object> mcngMsg = (Map<String, Object>) mcngData.get("message");
//        log.info("targetName: {}", mcngMsg.get("targetName"));
//
//        Map<String, String> targetMcngIdMap = new HashMap<>();
//        ((Map<String, Object>) mcngMsg.get("targetName")).forEach((s, o) -> {
//            targetMcngIdMap.put(s, (String) ((Map<String, Object>) o).get("id"));
//        });
//
//        log.info("Target mcng id: {}", targetMcngIdMap);
//
//        Map<String, Object> targetData = this.targetService.get();
//        Map<String, Object> targetMsg = (Map<String, Object>) targetData.get("message");
//
//        targetMsg.forEach((s, o) -> {
//            String mcngEtl = (String) ((Map<String, Object>) o).get("mcng-etl-id");
//
//            if (mcngEtl == null) {
//                log.info("To set mcngEtl-id: {} for target: {}", targetMcngIdMap.get(s), s);
//
//                if (targetMcngIdMap.get(s) != null) {
//                    List<String> imeiLs = (List<String>) ((Map<String, Object>) o).get("IMEI");
//                    List<String> imsiLs = (List<String>) ((Map<String, Object>) o).get("IMSI");
//                    List<String> msidsnLs = (List<String>) ((Map<String, Object>) o).get("MSISDN");
//                    IdentityCollection identityCollection = new IdentityCollection(s,
//                                                                                   msidsnLs.get(0),
//                                                                                   imsiLs.get(0),
//                                                                                   imeiLs.get(0),
//                                                                                   targetMcngIdMap.get(s));
//                    Map<String, Object> mapData = identityCollection.getTargetContainer();
//                    targetService.putTarget(mapData);
//                }
//            }
//        });
//    }

}
