package org.green.kav.db.fb.service;

import org.green.kav.csv.model.CsvTypeModel;
import org.green.kav.csv.model.cdr.CdrCsvModel;
import org.green.kav.csv.service.CsvTypeHandler;
import org.green.kav.csv.service.cdr.CdrCsvHandler;
import org.green.kav.db.fb.service.core.TargetService;
import org.green.kav.db.fb.service.core.mfi.CdrFileGenerator;
import org.green.kav.db.fb.service.model.identity.TargetModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.green.kav.csv.service.CsvService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SpringBootTest
public class CdrFileGeneratorTest {



    @Autowired
    private CdrFileGenerator cdrFileGenerator;

    @Autowired
    private CsvService csvService;

    @Autowired
    private TargetService targetService;



    @Test
    void contextLoads() throws Exception {
        String rootFile = "P:\\work\\6. trovicor - 05 08 2019\\convoy-data\\c6-100 areas\\20230528165330180_mfi_cdr-100-area-root-record-NEVER-DELETE.csv";
        String newFilesOutputDirectory = "P:\\work\\6. trovicor - 05 08 2019\\convoy-data\\c7-100-area-100-targets";
        String headers = "PROVIDER;EVENTTYPE;START;DURATION;STATUS;CGISTARTA;CGISTARTB;LATSTARTA;LATSTARTB;LONSTARTA;LONSTARTB;CGIENDA;CGIENDB;LATENDA;LATENDB;LONENDA;LONENDB;MSISDNA;MSISDNB;MSISDNC;IMEIA;IMEIB;IMSIA;IMSIB";
        String outputFilePostfix = "_mfi_c7-100-area-100-targets.csv";
        String delimiter = ";";
        int numberOfRecordForEachTarget = 1;


    }



    public List<String> targetNames() {
        String[] targets = new String[]{
                "Reagan Fritsch",
                "Jazmyn Bashirian",
                "Kayden Wilderman",
                "Dedrick McCullough",
                "Donnie Leffler",
                "Murray Kassulke",
                "Eliseo Lowe",
                "Christ Robel",
                "Paula Heaney",
                "Libbie Glover",
                "Nikko Nikolaus",
                "Sandrine Funk",
                "Branson Thiel",
                "Autumn Watsica",
                "Edd Beer",
                "Merritt DuBuque",
                "Kayli Howell",
                "Torrance Waters",
                "Garfield Gutmann",
                "Roderick Durgan",
                "Guiseppe Macejkovic",
                "Boris Brown",
                "Chaim DuBuque",
                "Dianna Bahringer",
                "Edwin Maggio",
                "Madelynn Collins",
                "Chauncey Schroeder",
                "Kathryne Russel",
                "Shaina Hane",
                "Israel Marquardt",
                "Clinton Jacobson",
                "Kristopher Hintz",
                "Earnestine Bosco",
                "Maeve Oberbrunner",
                "Parker Lakin",
                "Joannie Rutherford",
                "Novella McLaughlin",
                "Aniya Schmidt",
                "Naomie Lind",
                "Sincere Von",
                "Amos Beer",
                "Emmet Funk",
                "Travon Weissnat",
                "Dejuan Abbott",
                "Esta Friesen",
                "Antonetta Gislason",
                "Haylee Zieme",
                "Clementine Bailey",
                "Stefan Klocko",
                "Randall Koch",
                "Camille Ferry",
                "Garrett Turner",
                "Izabella Larson",
                "Giuseppe Hermann",
                "Elza Schaefer",
                "Carli Douglas",
                "Tomas Cole",
                "May Yundt",
                "Santino Franecki",
                "Zaria Eichmann",
                "Haskell Schoen",
                "Willow Swift",
                "Irwin VonRueden",
                "Lonie Jacobson",
                "Helena Stroman",
                "Orie Schumm",
                "Cyrus Renner",
                "Randi Hyatt",
                "Tyrell Parker",
                "Mitchel Hoeger",
                "Hope Ziemann",
                "Margarett Koch",
                "Tod O'Kon",
                "Marco Krajcik",
                "Garry Bogan",
                "Macey McGlynn",
                "Mabelle Jacobs",
                "Flavio Tillman",
                "Gayle Harvey",
                "Isobel Kuhlman",
                "Adam Pollich",
                "Hester Paucek",
                "Kirsten Rau",
                "Carlie Gislason",
                "Eliane Gorczany",
                "Kevon Lindgren",
                "Alena O'Hara",
                "Zechariah Rowe",
                "Wilfred Wiza",
                "Bradly McLaughlin",
                "Katrine Fritsch",
                "Camylle Schroeder",
                "Holly Mayer",
                "Wilhelm Boyer",
                "Ward Frami",
                "Edwardo Hermiston",
                "Nicole Graham",
                "Georgianna Schneider",
                "Jordi Goyette",
                "Alejandrin Hayes"};
        return Arrays.asList(targets);
    }
}
