package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.camel.processor.factory.PreprocessorFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class CsvFileConsumerProcessor3 implements Processor {
    private String delimiter;

    public CsvFileConsumerProcessor3(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn().getBody(File.class);
        log.info("Processing given file: {}", file);
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            CsvSchema csv = CsvSchema.emptySchema().withHeader().withColumnSeparator(delimiter.charAt(0));
            CsvMapper csvMapper = new CsvMapper();
            csvMapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS, true);
            MappingIterator<Map<String,String>>  mappingIterator =  csvMapper.reader().forType(Map.class).with(csv).readValues(file);
            List<Map<String, String>> result = new ArrayList<>();
            while (mappingIterator.hasNextValue()) {
                Map<String,String> value = mappingIterator.nextValue();
                result.add(value);
            }

            String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
            log.info("[data]{}", data);
            exchange.getIn().setBody(data);
            exchange.getIn().setHeader(PreprocessorFactory.ORIGINAL_FILE_NAME, file.getName());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
