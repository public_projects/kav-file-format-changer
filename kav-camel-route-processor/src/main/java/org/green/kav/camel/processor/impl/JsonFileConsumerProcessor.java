package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.common.bean.impl.CdrModel;

import java.io.File;

@Slf4j
public class JsonFileConsumerProcessor implements Processor {

    private String outputFileLocation;

    public JsonFileConsumerProcessor(String outputFileLocation) {
        this.outputFileLocation = outputFileLocation;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn().getBody(File.class);
        log.info("Processing given file: {}", file);

        JsonNode jsonTree = new ObjectMapper().readTree(file);
        CsvSchema.Builder csvSchemaBuilder = CsvSchema.builder();
        JsonNode firstObject = jsonTree.elements().next();
        firstObject.fieldNames().forEachRemaining(fieldName -> {csvSchemaBuilder.addColumn(fieldName);} );
        CsvSchema csvSchema = csvSchemaBuilder.build().withHeader().withColumnSeparator(';').withColumnSeparator(';').withoutQuoteChar();

        CsvMapper csvMapper = new CsvMapper();
        csvMapper.writerFor(JsonNode.class)
                .with(csvSchema)
                .writeValue(new File(outputFileLocation + File.separator + file.getName() + ".csv"), jsonTree);
    }
}
