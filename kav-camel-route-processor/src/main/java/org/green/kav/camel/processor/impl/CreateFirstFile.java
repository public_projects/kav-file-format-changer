package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.spi.ProcessorFactory;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.common.model.response.CdrRequest;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
public class CreateFirstFile implements Processor {
    private final ObjectMapper mapper = new ObjectMapper();
    private final DateTimeFormatter mcngDateTimeFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    private String outputDirectory;

    public CreateFirstFile(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    private static boolean isDirEmpty(final Path directory) throws IOException {
        try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(directory)) {
            return !dirStream.iterator()
                             .hasNext();
        }
    }

    private static void copyFileUsingStream(File source, File dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        }
        finally {
            is.close();
            os.close();
        }
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        LocalDateTime dateTime = LocalDateTime.now();

        File newFile = (File) exchange.getIn()
                                        .getBody();

        if (isDirEmpty(Paths.get(outputDirectory))) {
            String newFileName = dateTime.format(mcngDateTimeFormat);
            copyFileUsingStream(newFile, new File(outputDirectory + File.separator + newFileName + ".json"));
        }
        else {
            File outputFileDirectory = new File(outputDirectory);
            File[] files = outputFileDirectory.listFiles();

            if (files.length > 1) {
                throw new Exception("Too many output file. Not sure which one to merge this with!");
            }

            File oldFile = files[0];
            List<CdrRequest> existingCdrs = mapper.readValue(oldFile, new TypeReference<List<CdrRequest>>(){});
            List<CdrRequest> newCdrs = mapper.readValue(newFile, new TypeReference<List<CdrRequest>>(){});

            existingCdrs.addAll(newCdrs);

            mapper.writerWithDefaultPrettyPrinter().writeValue(oldFile, existingCdrs);

            log.info("Total excisting data: {} from file: {}", existingCdrs.size(), oldFile.getName());
            log.info("Total new data: {} from file: {}", newCdrs.size(), newFile.getName());
        }
    }
}
