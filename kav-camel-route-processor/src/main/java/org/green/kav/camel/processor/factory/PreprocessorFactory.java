package org.green.kav.camel.processor.factory;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.camel.processor.impl.McngFileProcessor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
public class PreprocessorFactory {
    public static final String PROCESSOR = "PROCESSOR";
    public static final String ORIGINAL_FILE_NAME = "OriginalFileName";
}
