package org.green.kav.camel.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.common.bean.flat.FlatFileModelService;

import java.time.LocalDateTime;

@Slf4j
public class McngFileDateRenewProcessor implements Processor {
    private LocalDateTime requiredDate;
    public McngFileDateRenewProcessor(LocalDateTime localDateTime) {
        this.requiredDate = localDateTime;
    }
    @Override
    public void process(Exchange exchange) throws Exception {
        FlatFileModelService flatFileModelService = exchange.getIn().getBody(FlatFileModelService.class);
        log.info("Processing given file: {}", flatFileModelService.getClass());
        long diffNum = flatFileModelService.getDateDiffToNewDate(requiredDate);
        flatFileModelService.addDaysToCurrentDates(diffNum);
        flatFileModelService.getRootFlatFileModels().forEach(rootFlatFileModel -> {
//            log.info("Updated dates: {}", rootFlatFileModel.getSES_PHONE_DIR1_STARTDATE());
        });
        exchange.getIn().setBody(flatFileModelService);
    }
}
