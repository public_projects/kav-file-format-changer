package org.green.kav.camel.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KavCamelRouteProcessorApplication {
	public static void main(String[] args) {
		SpringApplication.run(KavCamelRouteProcessorApplication.class, args);
	}
}
