package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.common.bean.SplitCsvFileContainer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Slf4j
public class WriteSplitFileProcessor implements Processor {

    private String outputFileLocation;

    public WriteSplitFileProcessor(String outputFileLocation) {
        this.outputFileLocation = outputFileLocation;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();

        List<SplitCsvFileContainer> data = exchange.getIn().getBody(List.class);
        log.info("data: {}", data);

        data.forEach(splitCsvFileContainer -> {
            writeToFile(splitCsvFileContainer.getContent(), outputFileLocation + File.separator + splitCsvFileContainer.getNewFileName());
        });

    }

    public void writeToFile(String jsonStr, String filePath) {
        File jsonFile = new File(filePath);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(jsonFile));
            writer.write(jsonStr);

        }
        catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        }
        finally {
            try {
                writer.close();
            }
            catch (IOException io) {
                // Just ignore
            }
        }
        log.info("Written to {}", jsonFile.getAbsolutePath());
    }
}
