package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.common.bean.flat.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class McngFileProcessor implements Processor {

    public static final Map<String, Class> fileRecordDateColumn = new HashMap<>();

    static {
        fileRecordDateColumn.put(TargetFlatFileModelService.FILE_NAME, TargetFlatFileModelService.class);
        fileRecordDateColumn.put(MsisdnFlatFileModelService.FILE_NAME, MsisdnFlatFileModelService.class);
        fileRecordDateColumn.put(ImsiFlatFileModelService.FILE_NAME, ImsiFlatFileModelService.class);
        fileRecordDateColumn.put(ImeiFlatFileModelService.FILE_NAME, ImeiFlatFileModelService.class);
        fileRecordDateColumn.put(CallFlatFileModelService.FILE_NAME, CallFlatFileModelService.class);
        fileRecordDateColumn.put(SmsFlatFileModelService.FILE_NAME, SmsFlatFileModelService.class);
        fileRecordDateColumn.put(LocationFlatFileModelService.FILE_NAME, LocationFlatFileModelService.class);
    }

    public McngFileProcessor() {
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn()
                            .getBody(File.class);
        log.info("Processing given file: {}", file);
        ObjectMapper mapper = new ObjectMapper();

        FlatFileModelService flatFileModel = getModel(file.getName());
        int rowCount = 0;
        String st;
        BufferedReader br = new BufferedReader(new FileReader(file));
        boolean fileEmpty = true;
        try {
            while ((st = br.readLine()) != null) {
                st = st.trim();
                if (st.length() == 0) continue;
                rowCount++;
//                log.debug("Row {} raw. content: {}", rowCount, st);
                if (rowCount == 1) {
//                    log.debug("header: {}", st);
                    flatFileModel.addDataHeader(st);
                    continue;
                }
                flatFileModel.addData(st);
                fileEmpty = false;
            }
        }
        catch (IOException ioe) {
            log.error("Failed to read content of file: {}", file);
        }
        finally {
            if (br != null) {
                br.close();
            }

            if (fileEmpty){
                log.error("Given file: {} is empty!", file.getName());
                throw new Exception("Given file " + file.getName() + " is empty!");
            }
        }

        flatFileModel.sortByDate();

//        csvFileContainer.sort();
//        csvFileContainer.renewDates();
        exchange.getIn().setBody(flatFileModel);
//        if (csvFileContainer.isSortableData()) {
//            exchange.getIn().setHeader(PreprocessorFactory.ORIGINAL_FILE_NAME, file.getName() + "_sorted");
//        }
//        else {
//            exchange.getIn().setHeader(PreprocessorFactory.ORIGINAL_FILE_NAME, file.getName());
//        }
    }

    public FlatFileModelService getModel(String fileName) throws Exception {
        Class cls = fileRecordDateColumn.get(fileName);
        Class c = Class.forName(cls.getCanonicalName());
        FlatFileModelService flatFileModel = (FlatFileModelService) c.newInstance();
        log.info("{}", flatFileModel.getClass().getCanonicalName());

        if (flatFileModel == null) {
            throw new UnsupportedOperationException();
        }
        return flatFileModel;
    }
}
