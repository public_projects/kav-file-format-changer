package org.green.kav.camel.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.common.bean.CsvFileContainer;
import org.green.kav.common.bean.SplitCsvFileContainer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Slf4j
public class SplitCsvContentProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn().getBody(File.class);
        log.info("Processing given file: {}", file);

        BufferedReader br = new BufferedReader(new FileReader(file));

        String fileNameWithoutExtension = file.getName();
        fileNameWithoutExtension = fileNameWithoutExtension.substring(0, fileNameWithoutExtension.indexOf("."));

        log.info("file name without extension: {}", fileNameWithoutExtension);

        int rowCount = 0;
        String st;
        List<SplitCsvFileContainer> splitCsvFileContainers = new ArrayList<>();

        try {
            SplitCsvFileContainer newFile = new SplitCsvFileContainer();
            String header = "";
            while ((st = br.readLine()) != null) {
                st = st.trim();
                if (st.length() == 0) continue;
                rowCount++;
                if (rowCount == 1) {
                    log.debug("header: {}", st);
                    header = st;
                    continue;
                }
                newFile = new SplitCsvFileContainer();
                newFile.setContent(header + System.lineSeparator() + st);
                newFile.setNewFileName(fileNameWithoutExtension + "-" + (rowCount-1) + ".csv");
                splitCsvFileContainers.add(newFile);
                log.debug("row: {}, content {}", rowCount, st);
            }
        }
        catch (IOException ioe) {
            log.error("Failed to read content of file: {}", file);
        }
        finally {
            if (br != null) {
                br.close();
            }
        }

        exchange.getIn().setBody(splitCsvFileContainers);
    }
}
