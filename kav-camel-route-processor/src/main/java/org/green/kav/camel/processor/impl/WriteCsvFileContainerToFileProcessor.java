package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.common.bean.CsvFileContainer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


@Slf4j
public class WriteCsvFileContainerToFileProcessor implements Processor {

    private String outputFileLocation;
    private String fileExtention;

    public WriteCsvFileContainerToFileProcessor(String outputFileLocation, String fileExtention) {
        this.outputFileLocation = outputFileLocation;
        this.fileExtention = fileExtention;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();
        CsvFileContainer contentTypes = exchange.getIn().getBody(CsvFileContainer.class);
        String fileOriginalName = (String) exchange.getIn().getHeader(PreprocessorFactory.ORIGINAL_FILE_NAME);
        writeToFile(contentTypes.toCSV().toString(), outputFileLocation + File.separator + fileOriginalName + fileExtention);
        exchange.getIn().setBody(new File(outputFileLocation + File.separator + fileOriginalName + fileExtention));
    }

    public void writeToFile(String jsonStr, String filePath) {
        File jsonFile = new File(filePath);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(jsonFile));
            writer.write(jsonStr);
        } catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        } finally {
            try {
                writer.close();
            } catch (IOException io) {
                // Just ignore
            }
        }
        log.info("Written to {}", jsonFile.getAbsolutePath());
    }
}
