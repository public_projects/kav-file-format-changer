package org.green.kav.camel.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.io.InputStream;

@Slf4j
public class FtpFileConsumerProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        String fileName = (String) exchange.getIn().getHeader("CamelFileName");
        log.info("File name: {}", fileName);

        String text = exchange.getIn().getBody(String.class);
        byte[] bytes = exchange.getIn().getBody(byte[].class);
        InputStream is = exchange.getIn().getBody(InputStream.class);
        /**
         * TODO: Follow https://stackoverflow.com/questions/31779575/how-to-get-the-actual-file-object-from-camel-ftp-route-exchange to get the file.
         */
    }
}
