package org.green.kav.camel.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.common.bean.CsvFileContainer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
public class CsvFileSortingConsumerProcessor implements Processor {
    private String delimiter;
    private boolean renewDates = false;

    public CsvFileSortingConsumerProcessor(String delimiter, boolean renewDates) {
        this.delimiter = delimiter;
        this.renewDates = renewDates;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn().getBody(File.class);
        log.info("Processing given file: {}", file);

        CsvFileContainer csvFileContainer = new CsvFileContainer(delimiter, renewDates);

        BufferedReader br = new BufferedReader(new FileReader(file));

        int rowCount = 0;
        String st;

        try {
            while ((st = br.readLine()) != null) {
                st = st.trim();
                if (st.length() == 0) continue;
                rowCount++;
//                log.debug("Row {} raw. content: {}", rowCount, st);
                if (rowCount == 1) {
//                    log.debug("header: {}", st);
                    csvFileContainer.setHeaderStr(convertArrayToList(st.split(delimiter)));
                    continue;
                }
                csvFileContainer.getDataLs().add(convertArrayToList(st.split(delimiter)));
            }
        }
        catch (IOException ioe) {
            log.error("Failed to read content of file: {}", file);
        }
        finally {
            if (br != null) {
                br.close();
            }
        }

        csvFileContainer.sort();
        csvFileContainer.renewDates();
        exchange.getIn().setBody(csvFileContainer);
        if (csvFileContainer.isSortableData()) {
            exchange.getIn().setHeader(PreprocessorFactory.ORIGINAL_FILE_NAME, file.getName() + "_sorted");
        }
        else {
            exchange.getIn().setHeader(PreprocessorFactory.ORIGINAL_FILE_NAME, file.getName());
        }
    }

    private static <T> List<T> convertArrayToList(T array[]) {
        // create a list from the Array
        return Arrays.stream(array).collect(Collectors.toList());
    }
}
