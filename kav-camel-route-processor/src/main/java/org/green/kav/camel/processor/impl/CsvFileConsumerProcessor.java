package org.green.kav.camel.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.common.bean.CsvFileContainer;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CsvFileConsumerProcessor implements Processor {
    private String delimiter;

    public CsvFileConsumerProcessor(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn().getBody(File.class);
        log.info("Processing given file: {}", file);

        CsvFileContainer csvFileContainer = new CsvFileContainer(this.delimiter, false);

        BufferedReader br = new BufferedReader(new FileReader(file));

        int rowCount = 0;
        String st;

        try {
            while ((st = br.readLine()) != null) {
                st = st.trim();
                if (st.length() == 0) continue;
                rowCount++;
//                log.debug("Row {} raw. content: {}", rowCount, st);
                if (rowCount == 1) {
//                    log.debug("header: {}", st);
//
                    csvFileContainer.setHeaderStr(convertArrayToList(st.split(delimiter)));
                    continue;
                }
                csvFileContainer.getDataLs().add(convertArrayToList(st.split(delimiter)));
            }
        } catch (IOException ioe) {
            log.error("Failed to read content of file: {}", file);
        } finally {
            if (br != null) {
                br.close();
            }
        }

        csvFileContainer.sort();
        exchange.getIn().setBody(csvFileContainer);
        exchange.getIn().setHeader(PreprocessorFactory.ORIGINAL_FILE_NAME, file.getName());
    }


    private static <T> List<T> convertArrayToList(T array[]) {
        // create a list from the Array
        return Arrays.stream(array).collect(Collectors.toList());
    }
}
