package org.green.kav.camel.processor.impl;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GeneralLoggingProcessor implements Processor {
    private static Logger log = LoggerFactory.getLogger(Logger.class);
    private String queueName;

    public GeneralLoggingProcessor(String queueName){
        this.queueName = queueName;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        log.info("[{}], received message: {}{}", queueName, System.lineSeparator() ,exchange.getIn().getBody());
    }
}
