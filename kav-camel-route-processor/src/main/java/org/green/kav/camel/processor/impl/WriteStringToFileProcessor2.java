package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.camel.processor.factory.PreprocessorFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Slf4j
public class WriteStringToFileProcessor2 implements Processor {

    private String outputFileLocation;

    public WriteStringToFileProcessor2(String outputFileLocation) {
        this.outputFileLocation = outputFileLocation;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();

        String data = exchange.getIn().getBody(String.class);
        String fileOriginalName = (String) exchange.getIn().getHeader(PreprocessorFactory.ORIGINAL_FILE_NAME);
        writeToFile(data, outputFileLocation + File.separator + fileOriginalName + ".json");
    }

    public void writeToFile(String jsonStr, String filePath) {
        File jsonFile = new File(filePath);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(jsonFile));
            writer.write(jsonStr);
        } catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        } finally {
            try {
                writer.close();
            } catch (IOException io) {
                // Just ignore
            }
        }
        log.info("Written to {}", jsonFile.getAbsolutePath());
    }
}
