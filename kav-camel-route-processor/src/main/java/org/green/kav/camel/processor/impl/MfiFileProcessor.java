package org.green.kav.camel.processor.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.camel.processor.model.MfiStructureInfo;
import org.javatuples.Pair;

/**
 * Still in progress 20220212
 */
@Slf4j
public class MfiFileProcessor implements Processor {

    private static List<String> commonDateHeaderNames = new ArrayList<>();
    public static final String MFI_DELIMETER = ";";
    public static final DateTimeFormatter mfiDateFormater = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

    static {
        commonDateHeaderNames.add("START");
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        File file = exchange.getIn().getBody(File.class);
        log.info("Processing given file: {}", file);
        exchange.getIn().setBody(foundDates(getDateColumn(file), file));
    }

    private Pair<String, Integer> getDateColumn(File file) throws Exception {
        Pair<String, Integer> dateColumn = null;
        String rowString = null;
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            while ((rowString = br.readLine()) != null) {
                rowString = rowString.trim();
                if (rowString.length() == 0) continue;
                log.debug("Row content: {}", rowString);
                log.debug("header: {}", rowString);
                dateColumn = getDateColumn(rowString);
                break;
            }
        } catch (IOException ioe) {
            log.error("Failed to read content of file: {}", file);
        } finally {
            if (br != null) {
                br.close();
            }

            if (dateColumn == null) {
                log.error("Given file: {} don't have date column!", file.getName());
                throw new Exception("Given file " + file.getName() + " is empty!");
            }
        }
        return dateColumn;
    }

    private Pair<String, Integer> getDateColumn(String rowString) {
        String[] headerColumns = rowString.toUpperCase(Locale.getDefault()).split(MFI_DELIMETER);

        Arrays.stream(headerColumns).filter(s -> commonDateHeaderNames.contains(s));

        Pair<String, Integer> dateColumnPair = null;
        for (int i = 0; i < headerColumns.length; i++) {
            if (commonDateHeaderNames.contains(headerColumns[i])) {
                dateColumnPair = Pair.with(headerColumns[i], i);
                break;
            }
        }
        return dateColumnPair;
    }

    private MfiStructureInfo foundDates(Pair<String, Integer> dateColumn, File file) throws Exception {
        MfiStructureInfo mfiStructureInfo = new MfiStructureInfo();

        int columnIndex = dateColumn.getValue1();

        int rowCount = 0;
        String rowString = null;

        BufferedReader br = new BufferedReader(new FileReader(file));
        LocalDateTime now = LocalDateTime.now();
        try {
            while ((rowString = br.readLine()) != null) {
                rowString = rowString.trim();
                if (rowString.length() == 0) continue;
                rowCount++;
                log.debug("Row {} raw. content: {}", rowCount, rowString);
                if (rowCount == 1) {
                    log.debug("header: {}", rowString);
                    continue;
                }
                String dateStr = rowString.split(MFI_DELIMETER)[columnIndex];
                LocalDateTime givenDate = LocalDateTime.parse(dateStr, mfiDateFormater);

                if (mfiStructureInfo.getOldestDate() == null){
                    mfiStructureInfo.setOldestDate(givenDate);
                } else {
                    if (mfiStructureInfo.getOldestDate().isBefore(givenDate)){
                        log.debug("previous oldest date: {}, new oldest date: {}", mfiStructureInfo.getOldestDate(), givenDate);
                        mfiStructureInfo.setOldestDate(givenDate);
                    }
                }

                mfiStructureInfo.getDateString().add(dateStr);
            }
        } catch (IOException ioe) {
            log.error("Failed to read content of file: {}", file);
        } finally {
            if (br != null) {
                br.close();
            }

            if (dateColumn == null) {
                log.error("Given file: {} don't have date column!", file.getName());
                throw new Exception("Given file " + file.getName() + " is empty!");
            }
        }

        log.info("{}", mfiStructureInfo.getDateString());
        return mfiStructureInfo;
    }
}
