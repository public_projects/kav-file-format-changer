package org.green.kav.camel.processor.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.common.bean.ContentType;
import org.green.kav.common.bean.CsvFileContainer;

import java.util.List;

@Slf4j
public class CsvToPojoProcessor implements Processor {
    private List<Class> contentTypes;

    public CsvToPojoProcessor(List<Class> contentTypes) {
        this.contentTypes = contentTypes;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter();

        CsvFileContainer csvFileContainer = exchange.getIn().getBody(CsvFileContainer.class);
        String matchingClass = null;

        for (Class contentType : contentTypes) {
            Class<?> clazz = Class.forName(contentType.getName());

            String paramForLog = null;
            try {
                for (String parameter : csvFileContainer.getHeaderStr()) {
                    paramForLog = parameter;
                    clazz.getDeclaredField(parameter.toLowerCase());
                }
            } catch (NoSuchFieldException e) {
                log.error("Ignore this class type!. class '{}' doesn't have the field with name '{}'. So it could not be this class!", contentType.getName(), paramForLog);
                continue; // Next class type
            }
            matchingClass = contentType.getName();
        }

        log.info("matchingClass: {}", matchingClass);
        Class<?> clazz = Class.forName(matchingClass);
        ContentType modelInstance = (ContentType) clazz.newInstance();

        List<ContentType> contentTypes = modelInstance.mapToPojo(csvFileContainer.getDataLs());

        exchange.getIn().setBody(contentTypes);
    }
}
