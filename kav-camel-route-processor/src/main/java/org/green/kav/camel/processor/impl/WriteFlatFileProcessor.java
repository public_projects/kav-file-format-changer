package org.green.kav.camel.processor.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.green.kav.common.bean.flat.FlatFileModelService;
import org.green.kav.common.bean.flat.model.RootFlatFileModel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Slf4j
public class WriteFlatFileProcessor implements Processor {

    private String outputLocation;

    public WriteFlatFileProcessor(String outputLocation) {
        this.outputLocation = outputLocation;
    }

    @Override
    public void process(Exchange exchange) throws Exception {
        FlatFileModelService flatFileModelService = exchange.getIn().getBody(FlatFileModelService.class);
//        log.info("Processing given file: {}", flatFileModelService.getClass());
        String newOutputLocation = outputLocation + File.separator + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));

        Files.createDirectories(Paths.get(newOutputLocation));
        newOutputLocation = newOutputLocation + File.separator + flatFileModelService.getFileName();

        File outputFilePath = new File(newOutputLocation);
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(outputFilePath));
            writer.write(flatFileModelService.getHeaderSingleLine());

            log.info("Writing {}, record count: {}.", newOutputLocation, flatFileModelService.getRootFlatFileModels().size());

            for (RootFlatFileModel rootFlatFileModel: flatFileModelService.getRootFlatFileModels()) {
                writer.write(flatFileModelService.getDataCsv(rootFlatFileModel));
            }
        } catch (IOException ioe) {
            log.error("Failed to write to file: {}", ioe);
        } finally {
            try {
                writer.close();
            } catch (IOException io) {
                // Just ignore
            }
        }
    }
}
