package org.green.kav.camel.processor.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MfiStructureInfo {
    private String dateHeaderName;
    private int dateHeaderIndexNumber;
    private List<String> dateString = new ArrayList<>();
    private List<LocalDateTime> newOrderedDates;
    private LocalDateTime oldestDate;

    public String getDateHeaderName() {
        return dateHeaderName;
    }

    public void setDateHeaderName(String dateHeaderName) {
        this.dateHeaderName = dateHeaderName;
    }

    public int getDateHeaderIndexNumber() {
        return dateHeaderIndexNumber;
    }

    public void setDateHeaderIndexNumber(int dateHeaderIndexNumber) {
        this.dateHeaderIndexNumber = dateHeaderIndexNumber;
    }

    public List<String> getDateString() {
        return dateString;
    }

    public void setDateString(List<String> dateString) {
        this.dateString = dateString;
    }

    public List<LocalDateTime> getNewOrderedDates() {
        return newOrderedDates;
    }

    public void setNewOrderedDates(List<LocalDateTime> newOrderedDates) {
        this.newOrderedDates = newOrderedDates;
    }

    public LocalDateTime getOldestDate() {
        return oldestDate;
    }

    public void setOldestDate(LocalDateTime oldestDate) {
        this.oldestDate = oldestDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MfiStructureInfo that = (MfiStructureInfo) o;
        return dateHeaderIndexNumber == that.dateHeaderIndexNumber && Objects.equals(dateHeaderName, that.dateHeaderName) && Objects.equals(
                oldestDate,
                that.oldestDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateHeaderName, dateHeaderIndexNumber, oldestDate);
    }
}
