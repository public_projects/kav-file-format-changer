package org.green.kav.camel;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.green.kav.common.bean.impl.CdrModel;
import org.green.kav.common.bean.ContentType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@Slf4j
@SpringBootTest
class KavCamelRouteManagerApplicationTests {

	@Test
	void contextLoads() throws Exception {
		Class<?> clazz = Class.forName(CdrModel.class.getCanonicalName());
		ContentType cdrModelInstance = (ContentType) clazz.newInstance();
		log.debug("class name: {}", clazz.getSimpleName());
		Field field = clazz.getDeclaredField("status");
		field.setAccessible(true);
		field.set(cdrModelInstance, "bunkkker");
		log.info("values {}",cdrModelInstance.toString());
	}

	@Test
	void contextLoads1() throws Exception {
		List<String> a = Collections.EMPTY_LIST;
		for (String b: a) log.info("{}", b);
	}

//	@Test
//	public void redCsvToJson() throws Exception {
//		File input = new File(
//				"D:\\Users\\sm13\\Documents\\Bugs\\IP-7227 area search participant call count\\comman-target\\P_TARGET_FLAT.csv");
//		try {
//			CsvSchema csv = CsvSchema.emptySchema().withHeader();
//			CsvMapper csvMapper = new CsvMapper();
//			MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader().forType(Map.class).with(csv).readValues(input);
//			List<Map<?, ?>> list = mappingIterator.readAll();
//			System.out.println(list);
//		}
//		catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
}
