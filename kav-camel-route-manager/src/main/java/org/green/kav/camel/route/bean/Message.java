package org.green.kav.camel.route.bean;

import java.io.Serializable;
import java.util.Map;

public class Message<T extends Serializable> {
    public final T body;
    public final Map<String, Object> headers;

    public Message(T body, Map<String, Object> headers) {
        this.body = body;
        this.headers = headers;
    }

    public String toString() {
        return "Message: headers[" + this.headers + "], type: " + this.body;
    }
}
