package org.green.kav.camel.route.bean;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Predicate;

public class HeaderPredicate implements Predicate {
    private final String header;
    private final Object value;

    public HeaderPredicate(String header, Object value) {
        this.header = header;
        this.value = value;
    }

    public HeaderPredicate(String header, Boolean value) {
        this.header = header;
        this.value = value;
    }

    public HeaderPredicate(String header) {
        this(header, (Boolean) null);
    }

    public boolean test(Message input) {
        if (input != null && input.headers != null && input.headers.containsKey(this.header)) {
            return this.value == null || this.value.equals(input.headers.get(this.header));
        }
        else {
            return false;
        }
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        else if (o != null && this.getClass() == o.getClass()) {
            HeaderPredicate that = (HeaderPredicate) o;
            if (this.header != null) {
                if (this.header.equals(that.header)) {
                    return this.value != null ? this.value.equals(that.value) : that.value == null;
                }
            }
            else if (that.header == null) {
                return this.value != null ? this.value.equals(that.value) : that.value == null;
            }

            return false;
        }
        else {
            return false;
        }
    }

    public int hashCode() {
        int result = this.header != null ? this.header.hashCode() : 0;
        result = 31 * result + (this.value != null ? this.value.hashCode() : 0);
        return result;
    }

    public String toString() {
        return "input.headers[" + this.header + "]=" + this.value;
    }

    @Override
    public boolean matches(Exchange exchange) {
        return true;
    }

    @Override
    public void init(CamelContext context) {
        Predicate.super.init(context);
    }

    @Override
    public void initPredicate(CamelContext context) {
        Predicate.super.initPredicate(context);
    }
}
