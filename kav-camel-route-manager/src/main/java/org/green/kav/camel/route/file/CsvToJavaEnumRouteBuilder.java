package org.green.kav.camel.route.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.camel.processor.impl.CSVEnumGenerator;
import org.green.kav.manager.config.container.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CsvToJavaEnumRouteBuilder extends RouteBuilder {
    @Autowired
    private MyConfig myConfig;

    @Override
    public void configure() throws Exception {
        from("file:" + myConfig.getCsvToJavaEnumFrom() + "?include=.*csv&preMove=staging&move=.completed")
                .process(new CSVEnumGenerator());
    }
}
