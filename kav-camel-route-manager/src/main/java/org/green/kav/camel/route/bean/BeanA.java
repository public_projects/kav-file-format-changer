package org.green.kav.camel.route.bean;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
@Slf4j
public class BeanA {
    public File handleBeanA(File file) {
        log.info("File a: {}", file.getAbsolutePath());
        return null;
    }
}
