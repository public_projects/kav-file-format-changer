package org.green.kav.camel.route.messaging.queue;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.PollingConsumer;
import org.apache.camel.Predicate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.language.simple.ast.BooleanExpression;
import org.apache.camel.language.simple.types.SimpleToken;
import org.apache.camel.model.language.HeaderExpression;
import org.apache.camel.spi.RoutePolicy;
import org.green.kav.camel.processor.impl.GeneralLoggingProcessor;
import org.green.kav.camel.route.bean.HeaderPredicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessagingQueueRoute extends RouteBuilder {
    @Autowired
    private CamelContext camelContext;
    @Override
    public void configure() throws Exception {

        HeaderPredicate internationalHeader = new HeaderPredicate("international", Boolean.TRUE);
        // copy local file to activemq queue. NOTE: must manually create my-activemq-queue queue first.
        from("file:D:/Users/sm13/Documents/kav-camel-route-manager/newQueue").to("activemq:my-activemq-queue");
        // Get data from queue
//        from("activemq:International.Call").setHeader("international", () -> Boolean.valueOf(true)).process(new GeneralLoggingProcessor("International.Call"));

//        from("activemq:International.Call").log("International.Call"); // didn't run
//        from("activemq:International.Call").recipientList().constant(internationalHeader).log("International.Call"); // didn't run
//        from("activemq:International.Call").filter(internationalHeader).process(new GeneralLoggingProcessor("International.Call")); // didn't run
//        from("activemq:International.Call").setHeader("international", new HeaderExpression("international")).process(new GeneralLoggingProcessor("International.Call"));
//        from("activemq:International.Call").removeHeader("international").process(new GeneralLoggingProcessor("International.Call"));
//        from("activemq:International.Call").setHeader("international").constant(Boolean.valueOf(true)).process(new GeneralLoggingProcessor("International.Call"));
//        from("activemq:international.call").process(new GeneralLoggingProcessor("International.Call"));
//        from("activemq:Communication.Call").process(new GeneralLoggingProcessor("Communication.Call"));
//        from("activemq:dispatch").process(new GeneralLoggingProcessor("dispatch"));
//        from("activemq:Target.Identities.Aggregator.Imsi").process(new GeneralLoggingProcessor("Target.Identities.Aggregator.Imsi"));
//        from("activemq:MCNG.Geo").process(new GeneralLoggingProcessor("MCNG.Geo"));
//        from("activemq:Communication.Online").process(new GeneralLoggingProcessor("Communication.Online"));
//        from("activemq:Target.Service.Usage").process(new GeneralLoggingProcessor("Target.Service.Usage"));
//        from("activemq:SimChange.Call").process(new GeneralLoggingProcessor("SimChange.Call"));
//        from("activemq:TargetProfileBuilding.Online").process(new GeneralLoggingProcessor("TargetProfileBuilding.Online"));
//        from("activemq:RefreshData.Adsl").process(new GeneralLoggingProcessor("RefreshData.Adsl"));
//        from("activemq:RefreshData.Online").process(new GeneralLoggingProcessor("RefreshData.Online"));
//        from("activemq:McngImei").process(new GeneralLoggingProcessor("McngImei"));
//        from("activemq:Activities.New.Call").process(new GeneralLoggingProcessor("Activities.New.Call"));
//        from("activemq:IdentityUsage.Call").process(new GeneralLoggingProcessor("IdentityUsage.Call"));
//        from("activemq:RefreshData.Imei").process(new GeneralLoggingProcessor("RefreshData.Imei"));
//        from("activemq:Activities.New.Call.IdentityRelations").process(new GeneralLoggingProcessor("Activities.New.Call.IdentityRelations"));
//        from("activemq:messageBoard").process(new GeneralLoggingProcessor("messageBoard"));
//        from("activemq:DrQuery.IPDR").process(new GeneralLoggingProcessor("DrQuery.IPDR"));
//        from("activemq:MCNG.Imsi").process(new GeneralLoggingProcessor("MCNG.Imsi"));
//        from("activemq:drs").process(new GeneralLoggingProcessor("drs"));
//        from("activemq:Target.Identities.IdentityUsage.Delete").process(new GeneralLoggingProcessor("Target.Identities.IdentityUsage.Delete"));
//        from("activemq:TargetProfileBuilding.Document").process(new GeneralLoggingProcessor("TargetProfileBuilding.Document"));
//        from("activemq:DeviceChange.Call").process(new GeneralLoggingProcessor("DeviceChange.Call"));
//        from("activemq:McngLiid").process(new GeneralLoggingProcessor("McngLiid"));
//        from("activemq:TargetProfileBuilding.Ownership").process(new GeneralLoggingProcessor("TargetProfileBuilding.Ownership"));
//        from("activemq:mailbox").process(new GeneralLoggingProcessor("mailbox"));
//        from("activemq:McngTarget").process(new GeneralLoggingProcessor("McngTarget"));
//        from("activemq:TargetProfileBuilding.Adsl").process(new GeneralLoggingProcessor("TargetProfileBuilding.Adsl"));
//        from("activemq:DrQuery.Status").process(new GeneralLoggingProcessor("DrQuery.Status"));
//        from("activemq:Activities.Geo").process(new GeneralLoggingProcessor("Activities.Geo"));
//        from("activemq:IdentityUsage.Online").process(new GeneralLoggingProcessor("IdentityUsage.Online"));
//        from("activemq:Target.Service.Activity").process(new GeneralLoggingProcessor("Target.Service.Activity"));
//        from("activemq:drs.adhoc").process(new GeneralLoggingProcessor("drs.adhoc"));
//        from("activemq:Activities.New.Call.Identity").process(new GeneralLoggingProcessor("Activities.New.Call.Identity"));
//        from("activemq:Activities.Online.Relation").process(new GeneralLoggingProcessor("Activities.Online.Relation"));
//        from("activemq:MCNG.Call").process(new GeneralLoggingProcessor("MCNG.Call"));
//        from("activemq:Target.Identities.Aggregator.Adsl").process(new GeneralLoggingProcessor("Target.Identities.Aggregator.Adsl"));
//        from("activemq:RefreshData.Msisdn").process(new GeneralLoggingProcessor("RefreshData.Msisdn"));
//        from("activemq:MCNG.Liid").process(new GeneralLoggingProcessor("MCNG.Liid"));
//        from("activemq:RefreshData.MsisdnIp").process(new GeneralLoggingProcessor("RefreshData.MsisdnIp"));
//        from("activemq:DrQuery.Request").process(new GeneralLoggingProcessor("DrQuery.Request"));
//        from("activemq:Target.Identities.Aggregator.Msisdn").process(new GeneralLoggingProcessor("Target.Identities.Aggregator.Msisdn"));
//        from("activemq:McngUpdate").process(new GeneralLoggingProcessor("McngUpdate"));
//        from("activemq:RefreshData.Imsi").process(new GeneralLoggingProcessor("RefreshData.Imsi"));
//        from("activemq:McngImsi").process(new GeneralLoggingProcessor("McngImsi"));
//        from("activemq:ActiveMQ.DLQ").process(new GeneralLoggingProcessor("ActiveMQ.DLQ"));
//        from("activemq:ImeiTypeAllocationCode").process(new GeneralLoggingProcessor("ImeiTypeAllocationCode"));
//        from("activemq:International.IdentityDissociation").process(new GeneralLoggingProcessor("International.IdentityDissociation"));
//        from("activemq:Target.Identities.Aggregator.Imei").process(new GeneralLoggingProcessor("Target.Identities.Aggregator.Imei"));
//        from("activemq:targetActivitiesNotification").process(new GeneralLoggingProcessor("targetActivitiesNotification"));
//        from("activemq:MCNG.Imei").process(new GeneralLoggingProcessor("MCNG.Imei"));
//        from("activemq:MCNG.Msisdn").process(new GeneralLoggingProcessor("MCNG.Msisdn"));
//        from("activemq:MobileDeviceRegistry.Call").process(new GeneralLoggingProcessor("MobileDeviceRegistry.Call"));
//        from("activemq:Identity.Search.Online").process(new GeneralLoggingProcessor("Identity.Search.Online"));
//        from("activemq:TargetProfileBuilding.Msisdn").process(new GeneralLoggingProcessor("TargetProfileBuilding.Msisdn"));
//        from("activemq:MobileDeviceRegistryBuilder.Call").process(new GeneralLoggingProcessor("MobileDeviceRegistryBuilder.Call"));
//        from("activemq:Target.Identities.Aggregator.Online").process(new GeneralLoggingProcessor("Target.Identities.Aggregator.Online"));
//        from("activemq:Target.Identities.IdentityOrigin").process(new GeneralLoggingProcessor("Target.Identities.IdentityOrigin"));
//        from("activemq:TargetProfileBuilding.Call").process(new GeneralLoggingProcessor("TargetProfileBuilding.Call"));
//        from("activemq:McngCaseTarget").process(new GeneralLoggingProcessor("McngCaseTarget"));
//        from("activemq:Activities.Online").process(new GeneralLoggingProcessor("Activities.Online"));
//        from("activemq:Target.Identities.IdentityOrigin.Delete").process(new GeneralLoggingProcessor("Target.Identities.IdentityOrigin.Delete"));
//        from("activemq:McngMsisdn").process(new GeneralLoggingProcessor("McngMsisdn"));
//        from("activemq:Msisdn.Ownership").process(new GeneralLoggingProcessor("Msisdn.Ownership"));
//        from("activemq:Identity.Search.Call").process(new GeneralLoggingProcessor("Identity.Search.Call"));
//        from("activemq:DrQuery.CDR").process(new GeneralLoggingProcessor("DrQuery.CDR"));
//        from("activemq:McngCase").process(new GeneralLoggingProcessor("McngCase"));
//        from("activemq:RefreshData.Target").process(new GeneralLoggingProcessor("RefreshData.Target"));
    }
}
