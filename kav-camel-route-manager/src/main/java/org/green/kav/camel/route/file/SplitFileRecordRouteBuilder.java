package org.green.kav.camel.route.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.camel.processor.impl.SplitCsvContentProcessor;
import org.green.kav.camel.processor.impl.WriteSplitFileProcessor;
import org.green.kav.manager.config.container.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SplitFileRecordRouteBuilder extends RouteBuilder {
    @Autowired
    private MyConfig myConfig;

    @Override
    public void configure() throws Exception {
        from("file:" + myConfig.getSplitFilesFrom() + "?include=.*csv&preMove=staging&move=.completed")
                .process(new SplitCsvContentProcessor())
                .process(new WriteSplitFileProcessor(myConfig.getSplitFilesOutput()));
    }
}
