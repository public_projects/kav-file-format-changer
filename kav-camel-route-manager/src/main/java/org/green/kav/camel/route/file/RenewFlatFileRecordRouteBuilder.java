package org.green.kav.camel.route.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.kav.camel.processor.impl.McngFileDateRenewProcessor;
import org.green.kav.camel.processor.impl.McngFileProcessor;
import org.green.kav.camel.processor.impl.WriteFlatFileProcessor;
import org.green.kav.manager.config.container.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RenewFlatFileRecordRouteBuilder extends RouteBuilder {
    @Autowired
    private MyConfig myConfig;

    @Override
    public void configure() throws Exception {
        from("file:" + myConfig.getRenewFlatFileDir() + "?include=.*csv&preMove=staging&move=.completed&moveFailed=.error")
                .process(new McngFileProcessor())
                .process(new McngFileDateRenewProcessor(myConfig.getRenewFlatFileDateDiffFrmNow()))
                .process(new WriteFlatFileProcessor(myConfig.getRenewFlatFileDirOutput()));
    }
}
