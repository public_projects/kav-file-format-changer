package org.green.kav.camel.route.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.kav.camel.processor.impl.MfiFileProcessor;
import org.green.kav.manager.config.container.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RenewMfiFIleRecordRouteBuilder extends RouteBuilder {
    @Autowired
    private MyConfig myConfig;

    @Override
    public void configure() {
        from("file:" + myConfig.getRenewMfiFileDir() + "?include=.*csv&preMove=staging&move=.completed&moveFailed=.error")
                .process(new MfiFileProcessor());
    }
}
