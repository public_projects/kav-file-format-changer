package org.green.kav.camel.route.ftp;

import org.apache.camel.builder.RouteBuilder;
import org.green.kav.camel.processor.impl.FtpFileConsumerProcessor;
import org.green.kav.manager.config.container.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component
public class FtpRouteToBean extends RouteBuilder {
    @Autowired
    private MyConfig myConfig;

    @Override
    public void configure() throws Exception {
//        [?options]
//        from(ftp://[username@]hostname[:port]/directoryname)
//        from("ftp://admin@192.168.1.104:2221")
//        from("ftp://foo@myserver?password=secret&ftpClient.dataTimeout=30000&ftpClientConfig.serverLanguageCode=fr").to("bean:foo");
//        from("ftp://admin@192.168.1.104:2221?password=admin&move=.done").streamCaching()
        from("sftp://root@192.168.40.102:22/mnt/data/volumes/ipfbackend/proximityanalysis/Input?password=spanner&move=.done&useUserKnownHostsFile=false")
//        from("ftp://192.168.1.104:2221")
                .process(new FtpFileConsumerProcessor());
    }
}
