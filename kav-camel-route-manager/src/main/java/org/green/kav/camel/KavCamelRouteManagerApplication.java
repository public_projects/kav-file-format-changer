package org.green.kav.camel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"org.green.kav"})
public class KavCamelRouteManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KavCamelRouteManagerApplication.class, args);
    }
}
