package org.green.kav.camel.route.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.camel.processor.impl.CreateFirstFile;
import org.green.kav.manager.config.container.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JsonMergerRouteBuilder extends RouteBuilder {
    @Autowired
    private MyConfig myConfig;

    @Override
    public void configure() throws Exception {
        from("file:" + myConfig.getJsonMergeInputDir() + "?include=.*json&preMove=staging&move=.completed&moveFailed=.errorFiles")
                .process(new CreateFirstFile(myConfig.getJsonMergeOutputDir()));
    }
}
