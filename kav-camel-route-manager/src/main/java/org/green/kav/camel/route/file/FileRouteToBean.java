package org.green.kav.camel.route.file;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.kav.camel.processor.factory.PreprocessorFactory;
import org.green.kav.camel.processor.impl.*;
import org.green.kav.common.bean.impl.CdrModel;
import org.green.kav.common.bean.impl.CellModel;
import org.green.kav.manager.config.container.MyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class FileRouteToBean extends RouteBuilder {
    public static final String ORIGINAL_FILE_NAME = "OriginalFileName";
    @Autowired
    private MyConfig myConfig;

    @Override
    public void configure() throws Exception {
        CsvFileConsumerProcessor csvFileConsumerProcessor = new CsvFileConsumerProcessor(myConfig.getCsvFileDelimeter());
        CsvFileConsumerProcessor3 csvFileConsumerProcessor3 = new CsvFileConsumerProcessor3(myConfig.getCsvFileDelimeter());
        CsvFileSortingConsumerProcessor csvFileSortingConsumerProcessor =
                new CsvFileSortingConsumerProcessor(myConfig.getCsvFileDelimeter(), false);
        CsvFileSortingConsumerProcessor csvFileSortingRenewableDatesConsumerProcessor =
                new CsvFileSortingConsumerProcessor(myConfig.getCsvFileDelimeter(), true);

        List<Class> contentTypes = new ArrayList<>();
        contentTypes.add(CdrModel.class);
        contentTypes.add(CellModel.class);

//        log.info("From: {}", myConfig.getFilesFrom());
//        from("file:" + myConfig.getFilesFrom() + "?include=.*csv&preMove=staging&move=.completed")
//                .process(csvFileConsumerProcessor)
//                .process(new CsvToPojoProcessor(contentTypes))
//                .process(new WriteContentTypeToFileProcessor(myConfig.getFilesOutput()));
        from("file:" + myConfig.getFilesFrom() + "?include=.*csv&preMove=staging&move=.completed").process(csvFileSortingConsumerProcessor)
                                                                                                  .process(new WriteCsvFileContainerToFileProcessor(
                                                                                                          myConfig.getFilesOutput(),
                                                                                                          ".csv"))
                                                                                                  .process(csvFileConsumerProcessor3)
                                                                                                  .process(new WriteStringToFileProcessor2(
                                                                                                          myConfig.getFilesOutput()));

        from("file:" + myConfig.getCsvSortedRenewedDateJsonFilesFrom() + "?include=.*csv&preMove=staging&move=.completed")
                .process(csvFileSortingRenewableDatesConsumerProcessor)
                .process(new WriteCsvFileContainerToFileProcessor(myConfig.getCsvSortedRenewedDateJsonFilesOutput(),".csv"))
                .process(csvFileConsumerProcessor3)
                .process(new WriteStringToFileProcessor2(myConfig.getCsvSortedRenewedDateJsonFilesOutput()));


//        from("file:" + myConfig.getFilesFrom() + "?include=.*csv&preMove=staging&move=.completed")
//                .process(csvFileConsumerProcessor3)
//                .process(new WriteStringToFileProcessor2(myConfig.getFilesOutput()));

        from("file:" + myConfig.getFilesFrom() + "?include=.*json&preMove=staging&move=.completed").process(new JsonFileConsumerProcessor(
                myConfig.getFilesOutput()));

//        from("file:" + myConfig.getFilesFrom() + "?include=.*json&preMove=staging&move=.completed")
//                .process(new CsvFileConsumerProcessor(myConfig.getFilesOutput()));
    }
}
