package org.green.kav.manager.config.container;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Configuration
@ConfigurationProperties(prefix = "route.config")
public class MyConfig {
    private String splitFilesFrom;
    private String profileBuilderDir;
    private String splitFilesOutput;
    private String csvSortedRenewedDateJsonFilesFrom;
    private String csvSortedRenewedDateJsonFilesOutput;
    private String renewFlatFileDir;
    private String renewMfiFileDir;
    private String renewFlatFileDirOutput;
    private LocalDateTime renewFlatFileDateDiffFrmNow; // yyyy-MM-dd HH:mm:ss
    private String csvToJavaEnumFrom;
    private String filesFrom;
    private String filesOutput;
    private String jsonMergeInputDir;
    private String jsonMergeOutputDir;
    private String csvFileDelimeter = ",";
    private List<String> convertorBeans;

    public void setRenewFlatFileDateDiffFrmNow(String renewFlatFileDateDiffFrmNow) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        this.renewFlatFileDateDiffFrmNow = LocalDateTime.of(LocalDate.parse(renewFlatFileDateDiffFrmNow, dateTimeFormatter), LocalTime.of(0,0,0));
    }

    public String getSplitFilesFrom() {
        return splitFilesFrom;
    }

    public void setSplitFilesFrom(String splitFilesFrom) {
        this.splitFilesFrom = splitFilesFrom;
    }

    public String getProfileBuilderDir() {
        return profileBuilderDir;
    }

    public void setProfileBuilderDir(String profileBuilderDir) {
        this.profileBuilderDir = profileBuilderDir;
    }

    public String getSplitFilesOutput() {
        return splitFilesOutput;
    }

    public void setSplitFilesOutput(String splitFilesOutput) {
        this.splitFilesOutput = splitFilesOutput;
    }

    public String getCsvSortedRenewedDateJsonFilesFrom() {
        return csvSortedRenewedDateJsonFilesFrom;
    }

    public void setCsvSortedRenewedDateJsonFilesFrom(String csvSortedRenewedDateJsonFilesFrom) {
        this.csvSortedRenewedDateJsonFilesFrom = csvSortedRenewedDateJsonFilesFrom;
    }

    public String getCsvSortedRenewedDateJsonFilesOutput() {
        return csvSortedRenewedDateJsonFilesOutput;
    }

    public void setCsvSortedRenewedDateJsonFilesOutput(String csvSortedRenewedDateJsonFilesOutput) {
        this.csvSortedRenewedDateJsonFilesOutput = csvSortedRenewedDateJsonFilesOutput;
    }

    public String getRenewFlatFileDir() {
        return renewFlatFileDir;
    }

    public void setRenewFlatFileDir(String renewFlatFileDir) {
        this.renewFlatFileDir = renewFlatFileDir;
    }

    public String getRenewMfiFileDir() {
        return renewMfiFileDir;
    }

    public void setRenewMfiFileDir(String renewMfiFileDir) {
        this.renewMfiFileDir = renewMfiFileDir;
    }

    public String getRenewFlatFileDirOutput() {
        return renewFlatFileDirOutput;
    }

    public void setRenewFlatFileDirOutput(String renewFlatFileDirOutput) {
        this.renewFlatFileDirOutput = renewFlatFileDirOutput;
    }

    public LocalDateTime getRenewFlatFileDateDiffFrmNow() {
        return renewFlatFileDateDiffFrmNow;
    }

    public String getCsvToJavaEnumFrom() {
        return csvToJavaEnumFrom;
    }

    public void setCsvToJavaEnumFrom(String csvToJavaEnumFrom) {
        this.csvToJavaEnumFrom = csvToJavaEnumFrom;
    }

    public String getFilesFrom() {
        return filesFrom;
    }

    public void setFilesFrom(String filesFrom) {
        this.filesFrom = filesFrom;
    }

    public String getFilesOutput() {
        return filesOutput;
    }

    public void setFilesOutput(String filesOutput) {
        this.filesOutput = filesOutput;
    }

    public String getJsonMergeInputDir() {
        return jsonMergeInputDir;
    }

    public void setJsonMergeInputDir(String jsonMergeInputDir) {
        this.jsonMergeInputDir = jsonMergeInputDir;
    }

    public String getJsonMergeOutputDir() {
        return jsonMergeOutputDir;
    }

    public void setJsonMergeOutputDir(String jsonMergeOutputDir) {
        this.jsonMergeOutputDir = jsonMergeOutputDir;
    }

    public String getCsvFileDelimeter() {
        return csvFileDelimeter;
    }

    public void setCsvFileDelimeter(String csvFileDelimeter) {
        this.csvFileDelimeter = csvFileDelimeter;
    }

    public List<String> getConvertorBeans() {
        return convertorBeans;
    }

    public void setConvertorBeans(List<String> convertorBeans) {
        this.convertorBeans = convertorBeans;
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
        objectMapper.writerWithDefaultPrettyPrinter();
        return objectMapper;
    }
}
