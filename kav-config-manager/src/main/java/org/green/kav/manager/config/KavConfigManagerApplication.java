package org.green.kav.manager.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KavConfigManagerApplication {
	public static void main(String[] args) {
		SpringApplication.run(KavConfigManagerApplication.class, args);
	}
}
