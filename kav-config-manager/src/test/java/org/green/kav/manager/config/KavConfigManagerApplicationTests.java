package org.green.kav.manager.config;

import org.green.kav.manager.config.container.MyConfig;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class KavConfigManagerApplicationTests {
    private Logger log = LoggerFactory.getLogger(KavConfigManagerApplicationTests.class);

    @Autowired
    private MyConfig myConfig;

    @Test
    void contextLoads() {
        log.info("Files from : {}", myConfig.getFilesFrom());
        log.info("Convertor beans  : {}", myConfig.getConvertorBeans());

        String a = "something";
        String b = "";

        if(!a.equals(b)){
            log.info("false");
        } else {
            log.info("true");
        }
    }
}
